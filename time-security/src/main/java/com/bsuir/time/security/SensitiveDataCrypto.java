package com.bsuir.time.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Component
public class SensitiveDataCrypto {
    @Value("${time-security.sensitive-data-crypto.algorithm:#{null}}")
    private String algorithm;
    @Value("${time-security.sensitive-data-crypto.transformation:#{null}}")
    private String transformation;
    @Value("${time-security.sensitive-data-crypto.key:#{null}}")
    private String key;
    @Value("${time-security.sensitive-data-crypto.vector:#{null}}")
    private String vector;
    @Value("${time-security.sensitive-data-crypto.charset:#{null}}")
    private String charsetName;

    private void verifyRequiredPropertiesSet() {
        if (algorithm == null ||
                transformation == null ||
                key == null ||
                vector == null ||
                charsetName == null) {
            throw new RuntimeException("Not all required properties for SensitiveDataCrypto are set");
        }
    }

    public String decrypt(String encryptedText) {
        verifyRequiredPropertiesSet();

        if (encryptedText == null || encryptedText.length() == 0) {
            return "";
        }

        byte[] encryptedTextBytes = Base64.getDecoder().decode(encryptedText);
        byte[] textBytes;
        try {
            textBytes = executeCryptoAction(encryptedTextBytes, Cipher.DECRYPT_MODE);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        String result = new String(textBytes, Charset.forName(charsetName));
        return result;
    }

    public String encrypt(String plainText) {
        verifyRequiredPropertiesSet();

        byte[] textBytes = plainText.getBytes(Charset.forName(charsetName));
        byte[] encryptedTextBytes;
        try {
            encryptedTextBytes = executeCryptoAction(textBytes, Cipher.ENCRYPT_MODE);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        byte[] base64 = Base64.getEncoder().encode(encryptedTextBytes);
        String result = new String(base64, Charset.forName(charsetName));
        return result;
    }

    private byte[] executeCryptoAction(byte[] input, int cipherMode) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException {
        byte[] keyBytes = key.getBytes(Charset.forName(charsetName));
        byte[] vectorBytes = vector.getBytes(Charset.forName(charsetName));

        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(cipherMode, new SecretKeySpec(keyBytes, algorithm), new IvParameterSpec(vectorBytes));

        byte[] result = cipher.doFinal(input);
        return result;
    }

}
