package com.bsuir.time.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TimeUserDetalisService implements UserDetailsService {

    @Autowired
    private JdbcTemplate timeJdbcTemplate;

    @Override
    public TimeUserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        try {
            return timeJdbcTemplate.queryForObject("select e.uid, e.full_name, e.email, ec.password from employee e " +
                            "join employee_cred ec on ec.uid=e.uid where e.email = ?",
                    (resultSet, i) -> {
                        Long uid = resultSet.getLong("uid");
                        String name = resultSet.getString("full_name");
                        String email = resultSet.getString("email");
                        String password = resultSet.getString("password");
                        return new TimeUserDetails(uid, name, email, password, List.of());
                    }, s);
        }catch (EmptyResultDataAccessException e){
            throw new UsernameNotFoundException("User not found");
        }
    }
}
