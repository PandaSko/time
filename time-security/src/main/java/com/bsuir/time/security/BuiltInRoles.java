package com.bsuir.time.security;

public enum BuiltInRoles {

    ACTUATOR("ACTUATOR");

    private String roleName;

    BuiltInRoles(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }
}
