package com.bsuir.time.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.session.ChangeSessionIdAuthenticationStrategy;

import javax.servlet.Filter;

import static com.bsuir.time.security.BuiltInRoles.ACTUATOR;

@EnableWebSecurity
@ConfigurationProperties(prefix = "time-security")
public class TimeSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String LOGIN_URL = "/api/login";
    private static final String API_URL = "/api";
    private static final String JWT_AUTH_URL_PATTERN = "/api/**";
    public static final String DEFAULT_FAILURE_URL = "/something-went-wrong?message=";
    public static final String LOGOUT_URL = "/api/logout";

    @Autowired
    private TimeUserDetalisService timeUserDetalisService;

    @Value("${time-security.enable-make-me-feature:true}")
    private boolean enableMakeMeFeature;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //todo https://time.epam.com/api/login?error=server_error&error_description=MSIS9604%3a+An+error+occurred.+The+authorization+server+was+not+able+to+fulfill+the+request.&state=Egie6R
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry = http
                .csrf().disable()
                .authorizeRequests();
        expressionInterceptUrlRegistry.antMatchers(LOGIN_URL).permitAll()
                .requestMatchers(EndpointRequest.to("health", "info")).permitAll()
                .requestMatchers(EndpointRequest.toAnyEndpoint()).hasAuthority(ACTUATOR.getRoleName())
                .antMatchers("/info").permitAll()
                .antMatchers("/redirect/**").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/icons/**").permitAll()
                .antMatchers("/error").permitAll()
                .antMatchers("/something-went-wrong").permitAll()
                .antMatchers("/**").permitAll();//.authenticated();

        http.getConfigurer(SessionManagementConfigurer.class).init(http);
        //SessionAuthenticationStrategy sessionAuthenticationStrategy = http.getSharedObject(SessionAuthenticationStrategy.class); //this effectively is always ChangeSessionIdAuthenticationStrategy

        Filter[] filters = {
                new TimeCustomMakeMeFilter(
                        LOGIN_URL,
                        enableMakeMeFeature,
                        new ChangeSessionIdAuthenticationStrategy()
                ),
                new TimeCustomFilter(
                        LOGIN_URL,
                        new ChangeSessionIdAuthenticationStrategy(),
                        timeUserDetalisService
                )
//                new JwtTokenFilter(
//                        JWT_AUTH_URL_PATTERN,
//                        //beanFactory.getBean(ResourceServerTokenServices.class),
//                        tokenServices,
//                        new ChangeSessionIdAuthenticationStrategy()
//                ),
        };

        Class<? extends Filter> after = AbstractPreAuthenticatedProcessingFilter.class;
        for (Filter filter : filters) {
            http.addFilterAfter(filter, after);
            after = filter.getClass();
        }

        http.logout().logoutUrl(LOGOUT_URL);
        http.exceptionHandling().accessDeniedHandler(new AccessDeniedHandlerImpl());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(timeUserDetalisService);
    }
}
