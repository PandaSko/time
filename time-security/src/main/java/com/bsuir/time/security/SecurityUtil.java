package com.bsuir.time.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public final class SecurityUtil {

    private SecurityUtil() {
    }

    public static TimeUserDetails getTimeUserDetails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return null;
        }
        return (TimeUserDetails) auth.getPrincipal();
    }

    public static Long getUserId() {
        TimeUserDetails timeUserDetails = getTimeUserDetails();
        if (timeUserDetails == null) {
            return null;
        }

        return timeUserDetails.getId();
    }

    public static String getUserName() {
        TimeUserDetails timeUserDetails = getTimeUserDetails();
        if (timeUserDetails == null) {
            return null;
        }

        return timeUserDetails.getFullName();
    }

}
