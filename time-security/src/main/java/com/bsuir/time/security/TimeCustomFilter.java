package com.bsuir.time.security;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TimeCustomFilter extends AbstractAuthenticationProcessingFilter {


    private TimeUserDetalisService timeUserDetalisService;
    private boolean postOnly = false;

    protected TimeCustomFilter(String defaultFilterProcessesUrl, SessionAuthenticationStrategy sessionAuthenticationStrategy, TimeUserDetalisService timeUserDetalisService) {
        super(defaultFilterProcessesUrl);
        this.setSessionAuthenticationStrategy(sessionAuthenticationStrategy);
        this.timeUserDetalisService = timeUserDetalisService;
    }

    @Override
    protected boolean requiresAuthentication(HttpServletRequest request,
                                             HttpServletResponse response) {
        if (super.requiresAuthentication(request, response)) {
            return request.getParameter("password") != null && request.getParameter("email") != null;
        }

        return false;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        if (this.postOnly && !request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        } else {
            String email = this.obtainUsername(request);
            String password = this.obtainPassword(request);
            if (email == null) {
                email = "";
            }

            if (password == null) {
                password = "";
            }

            email = email.trim();

            TimeUserDetails timeUserDetails = timeUserDetalisService.loadUserByUsername(email);
            if (!timeUserDetails.getPassword().equals(password)) {
                throw new AuthenticationServiceException("Password or email wrong");
            }

            TestingAuthenticationToken t = new TestingAuthenticationToken(timeUserDetails, "");
            t.setAuthenticated(true);
            return t;
        }
    }

    protected String obtainPassword(HttpServletRequest request) {
        return request.getParameter("email");
    }

    protected String obtainUsername(HttpServletRequest request) {
        return request.getParameter("password");
    }

}
