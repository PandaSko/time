package com.bsuir.time.security;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class TimeCustomMakeMeFilter extends AbstractAuthenticationProcessingFilter {
    private static final String MAKE_ME_PARAMETER = "make-me";

    private final boolean enableMakeMeFeature;

    public TimeCustomMakeMeFilter(String defaultFilterProcessesUrl, boolean enableMakeMeFeature, SessionAuthenticationStrategy sessionAuthenticationStrategy) {
        super(defaultFilterProcessesUrl);
        this.enableMakeMeFeature = enableMakeMeFeature;
        this.setSessionAuthenticationStrategy(sessionAuthenticationStrategy);
    }

    @Override
    protected boolean requiresAuthentication(HttpServletRequest request,
                                             HttpServletResponse response) {
        if (super.requiresAuthentication(request, response) && enableMakeMeFeature) {
            return request.getParameter(MAKE_ME_PARAMETER) != null;
        }

        return false;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        String result = request.getParameter(MAKE_ME_PARAMETER);

        if (result == null) {
            throw new RuntimeException("expected having make-me parameter");
        }

        if (result.isEmpty()) {
            throw new AuthenticationServiceException("make-me parameter is empty");
        }

        if (result.contains(" ")) {
            throw new AuthenticationServiceException("make-me shouldn't contain space symbol");
        }
        if (!result.matches("^\\d+$")) {//pmc id
            throw new AuthenticationServiceException("only numbers");
        }


        TimeUserDetails timeUserDetails;
        timeUserDetails = new TimeUserDetails(Long.decode(result), result, result, result, List.of());

        TestingAuthenticationToken t = new TestingAuthenticationToken(timeUserDetails, "");
        t.setAuthenticated(true);
        return t;
    }

}
