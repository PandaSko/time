package com.bsuir.time.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.bsuir.time.security.BuiltInRoles.ACTUATOR;

public class TimeUserDetails extends User {

    private final Long id;
    private final String fullName;

    public TimeUserDetails(Long id, String userFullName, String email, String password, Collection<? extends GrantedAuthority> authorities) {
        super(email, password, authorities);
        this.id = id;
        this.fullName = userFullName;
    }

    public String getFullName() {
        return fullName;
    }

    public Long getId() {
        return id;
    }

}
