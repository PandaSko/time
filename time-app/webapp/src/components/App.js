import React from 'react';
import './App.css';
import NavBar from './header/NavBar'
import { Route, Switch, Router } from 'react-router-dom';
import TimeJournalRouter from './timeJournal/TimeJournalRouter';
import ProjectDashboardRouter from "./projectDashboard/ProjectDashboardRouter";
import PackageDashboard from "./packageDashboard/PackageDashboard";
import EmployeeDashboard from "./employeeDashboard/EmployeeDashboard";
import Second from './dashboards/Second';
import Login from './auth/Login';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state={id:1};
  }

  render(){
    var id = this.state.id;
    if(id){
      return (
        <div className="App">
          <NavBar currentEmployeeId={id}/>
          <Switch>
            <Route exact path="/" render={(props) => (<TimeJournalRouter currentUserId={id} oldProps ={props}/>)}/>
            <Route path="/journal" render={(props) => (<TimeJournalRouter currentUserId={id} oldProps ={props}/>)}/>
            <Route path="/projects" render={(props) => (<ProjectDashboardRouter currentUserId={id} oldProps ={props}/>)}/>
            <Route path="/employees" render={(props) => (<EmployeeDashboard currentUserId={id} oldProps ={props}/>)}/>
            <Route path="/packages/:id" component={PackageDashboard}/>
          </Switch>
        </div>
      );
    }else{
      return(
        <div className="App">
          <NavBar/>
          <Login/>
        </div>
      )
    }
  }
}

export default App;
