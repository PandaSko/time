import React from 'react';
import NavBarItem from "./NavBarItem";
import { NavLink } from 'react-router-dom';
import "./NavBar.css";
import logo from "../../static/logo.png"

class NavBar extends React.Component {
  render(){

    if(this.props.currentEmployeeId){
      const navItems = [["/journal","Time Journal", false], ["/projects", "Projects", false], ["/employees", "Employees", false]].map((pair) => <NavBarItem link={pair[0]} name={pair[1]} isExact={pair[2]}/>);
      return (
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <NavLink className="navbar-brand h3"  to="/journal"><img src={logo} width="30" id="logo"/> Time</NavLink>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse justify-content-between" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              {navItems}
            </div>
            <div class="navbar-nav nav-item dropdown">
              <a class=" nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Ivan Ivanov
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Log Out</a>
              </div>
            </div>
          </div>
        </nav>
      );
    }else{
      return(
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="navbar-brand h3" ><img src={logo} width="30" id="logo"/> Time</div>
        </nav>
      );
    }
  }
}

export default NavBar;
