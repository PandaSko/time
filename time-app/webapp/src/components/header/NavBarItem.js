import React from 'react';
import { NavLink } from 'react-router-dom';

class NavBarItem extends React.Component {
  render(){
    return (
      <NavLink className="nav-item nav-link" exact={this.props.isExact} to={this.props.link}>{this.props.name}</NavLink>
    );
  }
}

export default NavBarItem;
