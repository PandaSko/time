import React from 'react';
import "./PackageDashboardBody.css";
import logo from "../../static/logo.png"
import { Link } from 'react-router-dom';

class PackageDashboardBody extends React.Component {
  render(){
    var packages = this.props.data;
    if(!packages){
      return <div></div>;
    }
    console.log(packages);
    var items = packages.packages.map(packageData => <PackageLine key={packageData.uid} packageData={packageData}/>);
    return(
      <div className="container">
        <div className="row">
          <div className="list-group list">
            <HeaderLine/>
            {items}
          </div>
        </div>
      </div>
    );
  }
}

function PackageLine(props){
  var packageData = props.packageData;
  return (
    <Link className="list-group-item list-group-item-action" to={"/package/"+packageData.uid}>
      <div className="container">
        <div className="row">
          <div className="col header">
            {packageData.name}
          </div>
          <div className="col">
            {packageData.startDate}
          </div>
          <div className="col">
            {packageData.endDate?packageData.endDate:"-"}
          </div>
        </div>
      </div>
    </Link>
  );
}

function HeaderLine(props){
  return (
    <div className="list-group-item list-group-item-dark header">
      <div className="container">
        <div className="row">
          <div className="col">
            Name
          </div>
          <div className="col">
            Start date
          </div>
          <div className="col">
            End date
          </div>
        </div>
      </div>
    </div>
  );
}

export default PackageDashboardBody;
