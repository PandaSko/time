import React from 'react';
import PackageDashboardBody from "./PackageDashboardBody";
import PackageDashboardHeader from "./PackageDashboardHeader";

const API = "http://localhost:8080/api/";

class PackageDashboard extends React.Component {
  constructor(props){
    super(props);
    this.state = {id:parseInt(props.match.params.id, 10)};
  }

  componentDidMount(){
    const id = this.state.id;
    this.setState({isLoading: true })
    fetch(API + 'package/by-project-uid?projectUid=' + id)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Something went wrong ...');
        }
      })
      .then(data => {
        this.setState({
          data: data,
          isLoading: false
        });
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render(){
    const id = this.state.id;

    const error = this.state.error;
    if(this.state.isLoading){
      return <div>
      <h1>Loading....</h1>
      </div>;
    }
    if(error){
      return <p>{error.message}</p>
    }
    return (
      <div>
        <PackageDashboardHeader/>
        <PackageDashboardBody data={this.state.data}/>
      </div>
  );
  }
}

export default PackageDashboard;
