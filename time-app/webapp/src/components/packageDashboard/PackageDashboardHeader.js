import React from 'react';
import { Link } from 'react-router-dom';

class PackageDashboardHeader extends React.Component {
  render(){
    return(
      <div className="container">
        <div className="row">
          <div className="col-9">
          </div>
          <div className="col-3">
            <Link className="btn btn-success btn-block" >Add new package</Link>
          </div>
        </div>
      </div>
    );
  }
}
export default PackageDashboardHeader;
