import React from 'react';
import ProjectDashboard from './ProjectDashboard';
import { Route, Switch, Router, Redirect } from 'react-router-dom';

class ProjectDashboardRouter extends React.Component {
    render(){
      var id = this.props.currentUserId;
      var redirectUri = "/projects/"+id;
    return (
      <Switch>
        <Route path="/projects/:id" component={ProjectDashboard}/>
        <Route path="/projects" render={(props) =>(<Redirect to={redirectUri}/>)}/>
      </Switch>
  );
  }
}

export default ProjectDashboardRouter;
