import React from 'react';
import "./ProjectDashboardBody.css";
import logo from "../../static/logo.png"
import { Link } from 'react-router-dom';
import right from "../../static/chevron-right.png"
import down from "../../static/chevron-down.png"

class ProjectDashboardBody extends React.Component {
  render(){
    var projectData = this.props.data;
    if(!projectData){
      return <div></div>;
    }
    console.log(projectData);
    var items = projectData.projects.map(project => <ProjectLine key={project.uid} project={project}/>);
    return(
      <div className="container">
        <div className="row">
          <div className="list-group list">
            <HeaderLine/>
            {items}
          </div>
        </div>
      </div>
    );
  }
}

class ProjectLine extends React.Component{
  constructor(props){
    super(props);
    this.state = {collapsed:true};

    this.collapse = this.collapse.bind(this);
  }

  collapse(){
    this.setState({collapsed:!this.state.collapsed});
  }

  render(){
    var project = this.props.project;
    console.log(project);
    if(project.positionsResponse){
      var positions = project.positionsResponse.map(position => <PositionLine position={position}/>);
    }
    return (
      <Link className="list-group-item list-group-item-action" to={"/project/"+project.uid}>
        <div className="container">
          <div className="row">
            <div className="col header">
              {project.name}
            </div>
            <div className="col">
              {project.startDate}
            </div>
            <div className="col">
              {project.endDate?project.endDate:"-"}
            </div>
            <div className="col">
              {project.lockDate?project.lockDate:"-"}
            </div>
            <div className="col-3">
              {project.tmsAvailable?(<Link className="btn btn-success"  to={"/mapping/"+project.uid}>
                Tms
              </Link>):<div></div>}
              <Link className="btn btn-success"  to={"/packages/"+project.uid}>
                Packages
              </Link>
              <Link>
                <button className="btn" onClick={this.collapse}>
                  <img src={this.state.collapsed?right:down} width="20" height="20" class="chevron"/>
                </button>
              </Link>
            </div>
          </div>
          {this.state.collapsed?<div></div>:
            <div className="row position">
              <div className="list-group list-group-flush list">
                <PositionHeaderLine/>
                {positions}
              </div>
            </div>
          }
        </div>
      </Link>
    );
  }
}

function PositionLine(props){
  var position = props.position;
  return (
    <Link className=" list-group-item-action list-group-item-info" to={"/journal/"+position.uid}>
      <div className="container">
        <div className="row">
          <div className="col header">
            {position.name}
          </div>
          <div className="col">
            {position.roleName}
          </div>
          <div className="col">
            {position.startDate}
          </div>
          <div className="col">
            {position.endDate?position.endDate:"-"}
          </div>
        </div>
      </div>
    </Link>
  );
}

function PositionHeaderLine(props){
  var position = props.position;
  return (
    <div className="list-group-item-dark header ">
      <div className="container">
        <div className="row">
          <div className="col">
            Name
          </div>
          <div className="col">
            Role name
          </div>
          <div className="col">
            Start date
          </div>
          <div className="col">
            End date
          </div>
        </div>
      </div>
    </div>
  );
}

function HeaderLine(props){
  return (
    <div className="list-group-item list-group-item-dark header">
      <div className="container">
        <div className="row">
          <div className="col">
            Name
          </div>
          <div className="col">
            Start date
          </div>
          <div className="col">
            End date
          </div>
          <div className="col">
            Lock date
          </div>
          <div className="col-3">
            Actions
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProjectDashboardBody;
