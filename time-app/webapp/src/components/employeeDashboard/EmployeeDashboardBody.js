import React from 'react';
import "./EmployeeDashboardBody.css";
import { Link } from 'react-router-dom';

class EmployeeDashboardBody extends React.Component {
  render(){
    var employeeData = this.props.data;
    if(!employeeData){
      return <div></div>;
    }
    console.log(employeeData);
    var items = employeeData.employees.map(employee => <EmployeeLine key={employee.uid} employee={employee}/>);
    return(
      <div className="container">
        <div className="row">
          <div className="list-group list">
            <HeaderLine/>
            {items}
          </div>
        </div>
      </div>
    );
  }
}

function EmployeeLine(props){
  var employee = props.employee;
  return (
    <Link className="list-group-item list-group-item-action" to={"/journal/"+employee.uid}>
      <div className="container">
        <div className="row">
          <div className="col header">
            {employee.fullName}
          </div>
          <div className="col">
            {employee.email}
          </div>
          <div className="col">
            {employee.title}
          </div>
          <div className="col">
            {employee.hired}
          </div>
          <div className="col">
            {employee.fired?employee.fired:"-"}
          </div>
          <div className="col">
            <Link >
              {employee.managerName}
            </Link>
          </div>
        </div>
      </div>
    </Link>
  );
}

function HeaderLine(props){
  return (
    <div className="list-group-item list-group-item-dark header">
      <div className="container">
        <div className="row">
          <div className="col">
            Name
          </div>
          <div className="col">
            Email
          </div>
          <div className="col">
            Title
          </div>
          <div className="col">
            Hired
          </div>
          <div className="col">
            Fired
          </div>
          <div className="col">
            Manager Name
          </div>
        </div>
      </div>
    </div>
  );
}

export default EmployeeDashboardBody;
