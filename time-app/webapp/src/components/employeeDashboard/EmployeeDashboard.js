import React from 'react';
import EmployeeDashboardBody from "./EmployeeDashboardBody";
import EmployeeDashboardHeader from "./EmployeeDashboardHeader";

const API = "http://localhost:8080/api/";

class EmployeeDashboard extends React.Component {
  constructor(props){
    super(props);
    this.state = {id:this.props.currentUserId};
  }

  componentDidMount(){
    const id = this.state.id;
    this.setState({isLoading: true })
    fetch(API + 'employee/by-manager-uid?managerUid=' + id)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Something went wrong ...');
        }
      })
      .then(data => {
        this.setState({
          data: data,
          isLoading: false
        });
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render(){
    const id = this.state.id;

    const error = this.state.error;
    if(this.state.isLoading){
      return <div>
      <h1>Loading....</h1>
      </div>;
    }
    if(error){
      return <p>{error.message}</p>
    }
    return (
      <div>
        <EmployeeDashboardHeader/>
        <EmployeeDashboardBody data={this.state.data}/>
      </div>
  );
  }
}

export default EmployeeDashboard;
