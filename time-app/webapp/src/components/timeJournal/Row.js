import React from 'react';
import "./Row.css";
import * as dateutil from "../util/DateUtil";
import * as strutil from "../util/StringUtil";
import ActivityCell from "./cells/ActivityCell";
import Cell from "./cells/Cell";
import {ProjectNameCell, AssignmentCell, EmptyCell,HeaderCell} from "./cells/CommonCells";

class ActivityRow extends React.Component{
  constructor(props){
    super(props);

    this.changeDuration = this.changeDuration.bind(this);
    this.changeActivity = this.changeActivity.bind(this);
  };

  changeActivity(data){
      data.activityId = this.props.activity.id;
      this.props.changeActivity(data);
  }

  changeDuration(data){
      data.activityId = this.props.activity.id;
      this.props.changeDuration(data);
  }

  render(){
    return (
      <tr>
          <ActivityCell names = {this.props.activity} defName={this.props.defName} changeActivity={this.changeActivity}/>
          {this.props.dates.map(date => <Cell date={date} worklog={this.props.worklogs.get(dateutil.toString(date))}
            lock={this.props.locks[dateutil.toString(date)]} changeDuration={this.changeDuration}/>)}
      </tr>
      );
  }
}

export function ProjectRow(props){
  return(
    <tr className="project">
        <ProjectNameCell name = {props.name} isCollapsed={props.isCollapsed} collapseDetails = {props.collapseDetails}/>
        {props.dates.map(date => <HeaderCell/>)}
    </tr>
  );
}

export function AssignmentRow(props){
  return(
    <tr className="assignment">
        <AssignmentCell name = {props.name} addActivity={props.addActivity}/>
        {props.dates.map(date => <EmptyCell/>)}
    </tr>
  );
}

export default ActivityRow;
