import React from 'react';
import "./Cell.css";
import * as strutil from "../../util/StringUtil";

class Cell extends React.Component{
  constructor(props){
    super(props);
    var duration = "";
    // console.log(props);
    if(this.props.worklog){
      duration = this.props.worklog.curDuration;
      if(duration==null || duration ===0){
        duration = this.props.worklog.duration;
      }
    }
    console.log(this.props.worklog);
    this.state = {duration:duration};
    this.changeDuration = this.changeDuration.bind(this);
  };

  changeDuration(event){
      this.setState({duration:event.target.value});
      var data = new Object();
      data.duration = parseFloat(event.target.value);
      data.date = this.props.date;
      this.props.changeDuration(data);
  };

  render(){
    var editable = true;
    if(this.props.lock){
      editable = false;
    }

    var cellBody;
    if(editable){
      return <td className = "cell editable-cell">
        <input className="input-sm" step="0.01" type="number" min="0" max="24"
          value={this.state.duration} onChange={this.changeDuration}/>
      </td>;
    }else{
      return <td className = "cell non-editable">{this.state.duration}</td>;
    }
    // console.log(props.date);
    return( <td className = "cell ">{cellBody}</td>);
  }
}

export default Cell;
