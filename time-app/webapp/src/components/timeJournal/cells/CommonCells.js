import React from 'react';
import "./Cell.css";
import * as strutil from "../../util/StringUtil";
import right from "../../../static/chevron-right.png"
import down from "../../../static/chevron-down.png"

export function HeaderDateCell(props){
  var formatter = new Intl.DateTimeFormat("en", {
    day: "2-digit",
    month: "short"
  });
  var date = new Date(props.date);
  return( <th className= "cell header">{formatter.format(date)}</th>);
}

export function HeaderCell(props){
  return( <th className = "cell">{props.duration}</th>);
}

export function HeaderNameCell(props){
  return( <th className = "name-cell">{props.name}</th>);
}

export function EmptyCell(props){
  return( <td className = "cell" ></td>);
}

export function NameCell(props){
  return( <td className = "name-cell">{props.name}</td>);
}

export function AssignmentCell(props){
  return(
    <td className = "name-cell assignment">
      <div className="name">{props.name}</div>
      <div className="name"><button className="btn btn-sm btn-outline-success btn-block" onClick={props.addActivity}>Add activity</button></div>

    </td>);
}

export function ProjectNameCell(props){
  return(
    <th className = "name-cell project">
      <div className="project-name">
        {props.name}
        <button className="collapse-button" onClick={props.collapseDetails}>
          <img src={props.isCollapsed?right:down} width="20" height="20" class="chevron"/>
        </button>
      </div>
    </th>
);
}
