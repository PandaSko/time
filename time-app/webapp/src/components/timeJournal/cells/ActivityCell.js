import React from 'react';
import "./Cell.css";
import * as strutil from "../../util/StringUtil";

class ActivityCell extends React.Component{
  constructor(props){
    super(props);

    this.state = {activityName:"", isDefault:false};

    this.changeName = this.changeName.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  componentDidMount(){

    var activityName;
    var isDefault = false;
    if(!strutil.isBlank(this.props.names.curName)){
      activityName = this.props.names.curName;
    }else{
      activityName = this.props.defName;
    }
    if(activityName === this.props.defName){
      isDefault = true;
    }

    this.setState({activityName:activityName, isDefault:isDefault});
  }

  changeName(event){
    var data = new Object();
    if(strutil.isBlank(event.target.value)){
      data.name = this.props.defName;
    }else{
      data.name = event.target.value;
    }
    this.props.changeActivity(data);
    this.setState({activityName:event.target.value});
  }

  onBlur(event){
    if(strutil.isBlank(this.state.activityName)|| this.state.activityName===this.props.defName){
      this.setState({activityName:this.props.defName, isDefault:true});
    }
  }

  onFocus(event){
    if(this.state.isDefault){
      this.setState({activityName:"", isDefault:false});
    }
  }

  render(){
    var name;
    if(this.state.isDefault){
      name = <input type="text" className="input-l default" value={this.state.activityName} onFocus={this.onFocus} onBlur={this.onBlur} onChange={this.changeName}/>;
    }else{
      name = <input type="text" className="input-l" value={this.state.activityName} onFocus={this.onFocus} onBlur={this.onBlur} onChange={this.changeName}/>
    }

    return(
      <td className = "name-cell">
        {name}
      </td>);
  }
};


export default ActivityCell;
