import React from 'react';
import TimeJournal from './TimeJournal';
import { Route, Switch, Router, Redirect } from 'react-router-dom';
import * as dateutil from "../util/DateUtil";
import * as qs from 'query-string';

class TimeJournalRouter extends React.Component {
    render(){
      var id = this.props.currentUserId;
      console.log(this.props.oldProps.location.pathname);
      const values = qs.parse(this.props.oldProps.location.search)
      var fromTo = dateutil.week();
      var from = fromTo[0];
      var to = fromTo[1];
      if(values.from){
        fromTo = dateutil.weekByDate(values.from);
        from = fromTo[0];
        to = fromTo[1];
      }
      var redirectUri = "/journal/"+id+"?from="+from;
      console.log();
    return (
      <Switch>
        <Route path="/journal/:id" render={(props) =>(<TimeJournal currentUserId={id} journalOwner={parseInt(props.match.params.id, 10)} from={from} to={to} location={props.location.pathname}/>)}/>
        <Route path="/journal" render={(props) =>(<Redirect to={redirectUri}/>)}/>
        <Route path="/" render={(props) =>(<Redirect to={redirectUri}/>)}/>
      </Switch>
  );
  }
}

export default TimeJournalRouter;
