import React from 'react';
import TimeJournalTable from './TimeJournalTable';
import TimeJournalHeader from './TimeJournalHeader';
import * as tjHelper from "../util/TJUtil";

const API = "http://localhost:8080/api/";

class TimeJournal extends React.Component {
  constructor(props){
    super(props);
    this.state = {isChanged: false};

    this.addActivity = this.addActivity.bind(this);
    this.changeDuration = this.changeDuration.bind(this);
    this.changeActivity = this.changeActivity.bind(this);
  }

  changeActivity(data){
      this.setState({isChanged: true});
      tjHelper.changeActivity(this.state.data, data);
  }

  changeDuration(data){
      this.setState({isChanged: true});
      tjHelper.changeDuration(this.state.data, data);
  }

  addActivity(data){
    tjHelper.addActivity(this.state.data, data);
    this.setState({data: this.state.data});
  }

  componentDidMount(){
    const id = this.props.journalOwner;
    const from = this.props.from;
    const to = this.props.to;
    this.setState({isLoading: true })
    fetch(API + 'time-journal/?employeeUid=' + id + '&from=' + from + '&to=' + to)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Something went wrong ...');
        }
      })
      .then(data => {
        this.setState({
          data: tjHelper.formatData(data),
          info: tjHelper.formatProjectInfo(data),
          isLoading: false,
          workload: data.workload});
      })
      .catch(error => this.setState({ error, isLoading: false }));
  }

  render(){
    const id = this.props.journalOwner;
    const from = this.props.from;
    const to = this.props.to;
    const error = this.state.error;
    if(this.state.isLoading){
      return <div>
      <h1>Loading....</h1>
      </div>;
    }
    if(error){
      return <p>{error.message}</p>
    }
    return (
      <div>
        <TimeJournalHeader from={from} to={to} changed={this.state.isChanged} location={this.props.location}/>
        <TimeJournalTable data = {this.state.data} info={this.state.info}
          workload={this.state.workload} from={from} to={to}
          addActivity={this.addActivity} changeActivity={this.changeActivity}
          changeDuration={this.changeDuration}/>
      </div>
  );
  }
}

export default TimeJournal;
