import React from 'react';
import ActivityRow, {AssignmentRow} from "./Row";

class Assignment extends React.Component{
  constructor(props){
    super(props);

    this.addActivity = this.addActivity.bind(this);
    this.changeWorklogDuration = this.changeWorklogDuration.bind(this);
    this.changeActivity = this.changeActivity.bind(this);
  };

  changeActivity(data){
    var info = this.props.info;

    data.assignmentUid = info.uid;
    this.props.changeActivity(data);
  }

  changeWorklogDuration(data){
    var info = this.props.info;

    data.assignmentUid = info.uid;
    this.props.changeDuration(data);
  }

  addActivity(){
    var info = this.props.info;
    var data = this.props.data;

    var hasDefault = false;
    Array.from(data.keys()).forEach(key=>{
      if(key.curName === info.name){
        hasDefault = true;
      }
    });
    //check if with standart name non exists
    if(!hasDefault){
      var data = new Object();
      data.assignmentUid = info.uid;
      data.name = info.name;
      this.props.addActivity(data);
    }
  }

  render(){
    var info = this.props.info;
    var data = this.props.data;

    var rows = [];

    Array.from(data.keys()).forEach(key=>
      rows.push(<ActivityRow key={key.id} activity={key} defName={info.name} dates={this.props.dates}
         worklogs={data.get(key)} locks={info.locks} changeActivity={this.changeActivity} changeDuration={this.changeWorklogDuration}/>)
    );

    return(
      <tbody>
        <AssignmentRow name={info.name} dates={this.props.dates} addActivity={this.addActivity}/>
        {rows.map(row => row)}
      </tbody>
    );
  }
}

export default Assignment;
