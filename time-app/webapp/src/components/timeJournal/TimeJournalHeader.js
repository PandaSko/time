import React from 'react';
import "./TimeJournalHeader.css";
import right from "../../static/chevron-right.png"
import left from "../../static/chevron-left.png"
import minus from "../../static/minus.png"
import * as dateutil from "../util/DateUtil";
import { NavLink } from 'react-router-dom';

class TimeJournalHeader extends React.Component {
  constructor(props){
    super(props);

  }

  render(){
    const changed = this.props.changed;
    const from = this.props.from;
    const to = this.props.to;
    var prevFrom = dateutil.addDays(from, -7);
    var nextFrom = dateutil.addDays(from, 7);

    var saveCancel;
    if(changed){
        saveCancel = <div className="row">
      <button className="col btn btn-success" onClick="">
        Save
      </button>
      <a className="col btn btn-secondary" href={this.props.location+"?from="+from}>
        Cancel
      </a>
      </div>
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <div className="row">
              <HeaderItem img={left} location={this.props.location+"?from="+prevFrom} changed={changed}/>
              <input class="form-control input" type="text" placeholder={from} readOnly/>
              <img src={minus} width="12" height="12" id="minus"/>
              <input className="form-control input" type="text" placeholder={to} readOnly/>
              <HeaderItem img={right} location={this.props.location+"?from="+nextFrom} changed={changed}/>
            </div>
          </div>
          <div className="col"></div>
          <div className="col">
            {saveCancel}
          </div>
        </div>
      </div>
  );
  }
}

function HeaderItem(props){
  var btn = <button className="collapse-button">
    <img src={props.img} width="20" height="20" className="chevron"/>
  </button>;
  var body;
  if(props.changed){
    body = btn;
  }else{
    body = <a className="" href={props.location}>
      {btn}
    </a>;
  }

  return(
    <div>
      {body}
    </div>
  )
}

export default TimeJournalHeader;
