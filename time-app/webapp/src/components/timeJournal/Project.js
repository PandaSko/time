import React from 'react';
import {ProjectRow} from "./Row";
import Assignment from "./Assignment";

class Project extends React.Component{
  constructor(props){
    super(props);
    this.state = {isCollapsed:false};

    this.collapseDetails = this.collapseDetails.bind(this);
  }

  collapseDetails(){
    this.setState({isCollapsed: !this.state.isCollapsed});
  }

  render(){
    var data = this.props.data;
    var info = this.props.info;

    if(!this.state.isCollapsed){
      var details = Array.from(data.keys()).map((uid) =>
        <Assignment key={uid} data={data.get(uid)} info={info.assignments.get(uid)} dates={this.props.dates}
          addActivity={(data) => callWithData(data, info.uid, this.props.addActivity)} changeActivity={(data) => callWithData(data, info.uid, this.props.changeActivity)}
          changeDuration={(data) => callWithData(data, info.uid, this.props.changeDuration)}/>);
    }
    return(
      <table className = "table table-bordered">
        <ProjectRow name={info.name}
          dates={this.props.dates} isCollapsed={this.state.isCollapsed} collapseDetails={this.collapseDetails}/>
        {details}
      </table>
    );
  }
}

function callWithData(data, uid, func){
  data.projectUid = uid;
  func(data);
};

export default Project;
