import React from 'react';
import * as dateutil from "../util/DateUtil";
import {ProjectNameCell, EmptyCell, NameCell, HeaderDateCell, HeaderCell, HeaderNameCell} from "./cells/CommonCells";
import {WorklogRow, ProjectRow, AssignmentRow} from "./Row";
import Project from "./Project";

class TimeJournalTable extends React.Component {
  render(){
    if(!this.props.data){
      return <div></div>
    }
    var workloads = this.props.workload.workloads;
    return (
      <div class="container">
          <TimeJournalTableHeader data={workloads}/>
          <TimeJournalTableBody data={this.props.data} info={this.props.info} from={this.props.from} to={this.props.to}
            addActivity={this.props.addActivity} changeActivity={this.props.changeActivity}
            changeDuration={this.props.changeDuration}/>
      </div>
  );
  }
}

class TimeJournalTableHeader extends React.Component {
  render(){
    return (
      <div class="row">
        <table className ="table table-bordered">
          <thead className="thead-dark">
            <tr>
              <HeaderNameCell name=""/>
              {
                this.props.data.map(workload =><HeaderDateCell date={workload.date}/>)
              }
            </tr>
            <tr>
              <HeaderNameCell name="Norm per day"/>
              {
                this.props.data.map(
                  workload =><HeaderCell duration = {workload.duration}/>
                )
              }
            </tr>
          </thead>
        </table>
      </div>
  );
  }
}

function TimeJournalTableBody(props){
        // const id = this.props.journalOwner;
    const from = props.from;
    const to = props.to;
    var dates = dateutil.period(from, to);
    const projects = props.data;
    const info = props.info;
    return (
      <div className="row">
        {
            Array.from(projects.keys()).map(uid =><Project data={projects.get(uid)} info={info.get(uid)} dates = {dates}
            addActivity={props.addActivity} changeActivity={props.changeActivity}
             changeDuration={props.changeDuration}/>)
        }
      </div>
  );
}

export default TimeJournalTable;
