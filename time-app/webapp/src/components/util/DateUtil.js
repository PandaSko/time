
export function period(start, end){
  var startDate = new Date(start);
  var endDate = new Date(end);
  var dates = [];
  for(var curr = startDate;!(curr.getDate() > endDate.getDate() && curr.getMonth() == endDate.getMonth());
    curr.setDate(curr.getDate()+1)){
    dates.push(new Date(curr.getTime()));
  }
  return dates;
}

export function toString(date){
  return date.toISOString().split("T")[0];
}

export function week(){
  var curr = new Date; // get current date
  var first = curr.getDate() - curr.getDay() + 2; // First day is the day of the month - the day of the week
  var last = first + 6; // last day is the first day + 6

  var firstday = new Date(curr.setDate(first));
  var lastday = new Date(curr.setDate(last));
  return [toString(firstday), toString(lastday)];
}

export function weekByDate(date){
  var curr = new Date(date); // get current date
  var first = curr.getDate(); // First day is the day of the month - the day of the week
  var last = first + 6; // last day is the first day + 6

  var firstday = new Date(curr.setDate(first));
  var lastday = new Date(curr.setDate(last));
  return [toString(firstday), toString(lastday)];
}

export function addDays(date, num){
  var curr = new Date(date);
  var day = curr.getDate() + num;
  var result = new Date(curr.setDate(day));
  return toString(result);
}
