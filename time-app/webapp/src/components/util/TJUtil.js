import * as dateutil from "./DateUtil";

export function formatData(data){
  var formatedData = new Map();
  data.projects.forEach(project=>{
    var assignmentMap = new Map();
    project.assignments.forEach(assignment=>{
      var locks = assignment.locks;
      var activityMap = new Map;
      var i = 1;
      Object.keys(assignment.taskToWorklogs).forEach(activityName=>{
        var task = assignment.taskToWorklogs[activityName];
        var worklogMap = new Map();
        Object.keys(task).forEach(date =>{
          var dataWorklog = task[date];
          var locked = false;
          if(locks[date]){
              locked = true;
          }
          var worklog = new Worklog(dataWorklog.uid, dataWorklog.duration, dataWorklog.duration, dataWorklog.date, locked);
          worklogMap.set(date, worklog);
        });

        var activity = new Activity(i++, activityName, activityName);
        activityMap.set(activity, worklogMap);
      });

      assignmentMap.set(assignment.assignmentUid, activityMap);
    });
    formatedData.set(project.projectUid, assignmentMap);
  });
  return formatedData;
};

export function formatProjectInfo(data){
  var formatedData = new Map();
  data.projects.forEach(project=>{
    var assignmentMap = new Map();
    project.assignments.forEach(assignment=>{
      assignmentMap.set(assignment.assignmentUid, new Assignment(assignment.assignmentUid, assignment.name, assignment.locks))
    });
    formatedData.set(project.projectUid, new Project(project.projectUid, project.projectName, assignmentMap));
  });
  return formatedData;
};

export function changeActivity(data, newActivity){
  var activities = data.get(newActivity.projectUid).get(newActivity.assignmentUid);
  var curActivity;
  var value;
  Array.from(activities.keys()).forEach(activity =>{
    if(activity.id === newActivity.activityId){
      curActivity = activity;
      value = activities.get(activity);
      activities.delete(activity);
    }
  });
  curActivity.curName = newActivity.name;
  activities.set(curActivity, value);
}

export function addActivity(data, newActivity){
   var activities = data.get(newActivity.projectUid).get(newActivity.assignmentUid);
   var id = 0;
   Array.from(activities.keys()).forEach(activity =>{
     if(activity.id > id){
       id = activity.id;
     }
   });
   activities.set(new Activity(++id, null, newActivity.name), new Map())
}

export function changeDuration(data, worklog){
  var activities = data.get(worklog.projectUid).get(worklog.assignmentUid);
  var curActivity;
  var value;
  Array.from(activities.keys()).forEach(activity =>{
    if(activity.id === worklog.activityId){
      value = activities.get(activity);
    }
  });
  var worklogRec = value.get(dateutil.toString(worklog.date));
  if(worklogRec){
    worklogRec.curDuration = worklog.duration;
  }else{
    value.set(dateutil.toString(worklog.date), new Worklog(null, null, worklog.duration, dateutil.toString(worklog.date), false));
  }
}

function Assignment(uid, name, locks){
  this.uid = uid;
  this.name = name;
  this.locks = locks;
}

function Project(uid, name, assignments){
  this.uid = uid;
  this.name = name;
  this.assignments = assignments;
}

function Activity(id, activityName, curName){
  this.id = id;
  this.name = activityName;
  this.curName = curName;
};

function Worklog(uid, duration, curDuration, date, locked){
  this.uid = uid;
  this.duration = duration;
  this.curDuration = curDuration;
  this.date = date;
  this.locked = locked;
};
