package com.bsuir.time.position.dto;

import com.bsuir.time.domain.Position;

import java.util.ArrayList;
import java.util.List;

public class PositionListResponse {

    private List<Position> positions = new ArrayList<>();

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }
}
