package com.bsuir.time.position.dto;

import com.bsuir.time.domain.enums.PositionStatus;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class PositionRequest {

    @NotNull
    private Long employeeUid;
    private String primaryRoleName;
    @NotNull
    private LocalDate startDate;
    private LocalDate endDate;
    @NotNull
    private PositionStatus status;
    @NotNull
    private Long projectUid;

    public Long getEmployeeUid() {
        return employeeUid;
    }

    public void setEmployeeUid(Long employeeUid) {
        this.employeeUid = employeeUid;
    }

    public String getPrimaryRoleName() {
        return primaryRoleName;
    }

    public void setPrimaryRoleName(String primaryRoleName) {
        this.primaryRoleName = primaryRoleName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public PositionStatus getStatus() {
        return status;
    }

    public void setStatus(PositionStatus status) {
        this.status = status;
    }

    public Long getProjectUid() {
        return projectUid;
    }

    public void setProjectUid(Long projectUid) {
        this.projectUid = projectUid;
    }
}
