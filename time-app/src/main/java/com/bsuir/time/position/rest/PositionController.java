package com.bsuir.time.position.rest;

import com.bsuir.time.dao.EmployeeDao;
import com.bsuir.time.dao.PositionDao;
import com.bsuir.time.dao.ProjectDao;
import com.bsuir.time.domain.Position;
import com.bsuir.time.domain.Project;
import com.bsuir.time.position.dto.PositionListResponse;
import com.bsuir.time.position.dto.PositionRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PositionController.API_ADDRESS)
@Api(tags = "position", description = "position api")
public class PositionController {

    public static final String API_ADDRESS = "/api/position";

    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private PositionDao positionDao;

    @GetMapping
    @ApiOperation(value = "Get position")
    public PositionListResponse getPositions() {
        PositionListResponse response = new PositionListResponse();
        response.setPositions(positionDao.findAll());
        return response;
    }

    @PutMapping
    @ApiOperation(value = "Change position")
    public void changeProject(@RequestParam Long uid,
                              @RequestBody @Validated PositionRequest request) {
        validateProjectRequest(request);
        positionDao.save(convertToPosition(uid, request));
    }

    @PostMapping
    @ApiOperation(value = "Create position")
    public void createProject(@RequestBody @Validated PositionRequest request) {
        validateProjectRequest(request);
        positionDao.save(convertToPosition(request));
    }

    @DeleteMapping
    @ApiOperation(value = "Delete position")
    public void deleteProject(@RequestParam Long uid) {
        positionDao.deleteById(uid);
    }

    private void validateProjectRequest(PositionRequest positionRequest) {
        if (positionRequest.getProjectUid() == 0) {
            throw new RuntimeException("Restrict to create position for npa project");
        }
        if (positionRequest.getEmployeeUid() == 0) {
            throw new RuntimeException("Restrict to create position for global admin");
        }

        Project project = projectDao.findById(positionRequest.getProjectUid())
                .orElseThrow(() -> new RuntimeException("Project not found"));
        employeeDao.findById(positionRequest.getEmployeeUid())
                .orElseThrow(() -> new RuntimeException("Employee not found"));

        if (positionRequest.getStartDate().isBefore(project.getStartDate())) {
            throw new RuntimeException("Position start date should be after project start date");
        }
        if (positionRequest.getEndDate() != null && positionRequest.getEndDate().isBefore(positionRequest.getStartDate())) {
            throw new RuntimeException("Position end date should be after start date");
        }
    }

    private Position convertToPosition(Long uid, PositionRequest request) {
        Position position = convertToPosition(request);
        position.setUid(uid);
        return position;
    }

    private Position convertToPosition(PositionRequest request) {
        Position position = new Position();
        position.setEndDate(request.getEndDate());
        position.setEmployeeUid(request.getEmployeeUid());
        position.setPrimaryRoleName(request.getPrimaryRoleName());
        position.setProjectUid(request.getProjectUid());
        position.setStatus(request.getStatus());
        position.setStartDate(request.getStartDate());
        return position;
    }
}
