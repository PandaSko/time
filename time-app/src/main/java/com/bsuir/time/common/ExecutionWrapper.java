package com.bsuir.time.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.time.OffsetDateTime;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ExecutionWrapper {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private volatile static int counter = 0;

    private static final int SHUTDOWN_WAIT_TIME_MILLISECONDS = 900_000;

    private final ExecutorService executorService = Executors.newFixedThreadPool(4);

    @Autowired
    @Qualifier("timeDataSource")
    private DataSource timeDataSource;

    @Autowired
    private ExecutionWrapper.TransactionWrapper transactionWrapper;

    @PreDestroy
    protected void predestroy() throws InterruptedException {
        executorService.shutdown();
        long started = System.currentTimeMillis();
        while (counter > 0) {
            Thread.sleep(1000);

            if (counter <= 0) {
                break;
            }

            if (System.currentTimeMillis() - started > SHUTDOWN_WAIT_TIME_MILLISECONDS) {
                logger.warn("Unable to wait till all executes ends. Shutting down anyway.");
                break;
            }
        }
    }

    private <T> T callExclusive(String lockName, boolean removeLockEntryFromDbInCaseOfSuccess, Callable<T> callable) {
        boolean exists = new JdbcTemplate(timeDataSource).queryForObject("select exists(select 1 from lock where id = ?)", Boolean.class, lockName);
        if (!exists) {
            new JdbcTemplate(timeDataSource).update("insert into lock (id, value, change, changed) values (?, false, 'init entry', ?)", lockName, OffsetDateTime.now());
        }

        boolean updated = new JdbcTemplate(timeDataSource).update("update lock set value = true, change = 'in progress', changed = ? where id = ? and value is distinct from true",
                OffsetDateTime.now(), lockName) != 0;

        if (!updated) {
            throw new LockedException();
        }

        try {
            T result = callable.call();
            new JdbcTemplate(timeDataSource).update("update lock set value = false, change = 'success', changed = ? where id = ?",
                    OffsetDateTime.now(), lockName);

            if (removeLockEntryFromDbInCaseOfSuccess) {
                new JdbcTemplate(timeDataSource).update("delete from lock where id = ?", lockName);
            }

            return result;
        } catch (Exception e) {
            new JdbcTemplate(timeDataSource).update("update lock set value = false, change = 'failed', changed = ? where id = ?",
                    OffsetDateTime.now(), lockName);
            throw new RuntimeException(e);
        }
    }

    private <T> T call(Execution<T> execution) {
        Callable<T> callable = execution.getCallable();
        Deque<Callable<T>> callsHolder = new LinkedList<>();
        callsHolder.add(callable);

        if (execution.getTransaction() != null) {
            switch (execution.getTransaction()) {
                case REQUIRED:
                    callable = () -> transactionWrapper.runInTransaction(callsHolder.pollLast());
                    callsHolder.add(callable);
                    break;
                case NESTED:
                    callable = () -> transactionWrapper.runInNestedTransaction(callsHolder.pollLast());
                    callsHolder.add(callable);
                    break;
                default:
                    throw new RuntimeException("transaction type is not supported");
            }
        }

        if (execution.getExclusiveLockName() != null) {
            callable = () -> callExclusive(execution.getExclusiveLockName(), execution.isRemoveLockEntryFromDbInCaseOfSuccess(), callsHolder.pollLast());
            callsHolder.add(callable);
        }

        try {
            synchronized (this) {//doing not atomic operation
                counter++;
            }

            return callsHolder.pollLast().call();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            synchronized (this) {//doing not atomic operation
                counter--;
            }
        }
    }

    protected ExecutorService getExecutorService() {
        return executorService;
    }

    @Component
    public static class TransactionWrapper {
        @Transactional
        public <T> T runInTransaction(Callable<T> callable) {
            try {
                return callable.call();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }

        @Transactional(propagation = Propagation.NESTED)
        public <T> T runInNestedTransaction(Callable<T> callable) {
            try {
                return callable.call();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }

    }

    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "Already is running")
    public class LockedException extends RuntimeException {
    }

    public <T> Execution<T> build(Callable<T> callable) {
        return new Execution<>(this, callable);
    }

    public Execution<Void> build(Runnable runnable) {
        return new Execution<>(this, () -> {
            runnable.run();
            return null;
        });
    }

    public static class Execution<T> {
        private final Logger logger = LoggerFactory.getLogger(getClass());

        private String exclusiveLockName;
        private boolean removeLockEntryFromDbInCaseOfSuccess;
        private Propagation transaction;
        private String description;
        private Callable<T> callable;
        private ExecutionWrapper executionWrapper;
        private boolean silently;

        private Execution(ExecutionWrapper executionWrapper, Callable<T> callable) {
            this.executionWrapper = executionWrapper;
            this.callable = callable;
        }

        public T getResult() {
            //todo throw exception if silently or asynchronous is on or execution didn't happen
            throw new UnsupportedOperationException("not implemented");
        }

        public CompletableFuture<Void> executeSilentlyAndAsync() {

            return CompletableFuture.runAsync(() -> executionWrapper.call(this), executionWrapper.getExecutorService())
                    .exceptionally((e) -> {
                        if (e != null) {
                            if (LockedException.class.isAssignableFrom(e.getClass())) {
                                logger.info("execution is locked. description: " + description + ", lockName: " + exclusiveLockName);
                            } else {
                                logger.error("description: " + description + ", lockName: " + exclusiveLockName, e);
                            }
                        }
                        return null;
                    });

        }

        public void execute() {
            try {
                executionWrapper.call(this);
            } catch (Exception e) {
                if (silently) {
                    if (LockedException.class.isAssignableFrom(e.getClass())) {
                        logger.info("execution is locked. description: " + description + ", lockName: " + exclusiveLockName);
                    } else {
                        logger.error("description: " + description + ", lockName: " + exclusiveLockName, e);
                    }
                } else {
                    throw e;
                }
            }
        }

        public String getExclusiveLockName() {
            return exclusiveLockName;
        }

        public Execution lock(String exclusiveLockName) {
            this.exclusiveLockName = exclusiveLockName;
            return this;
        }

        public Propagation getTransaction() {
            return transaction;
        }

        public Execution transaction() {
            this.transaction = Propagation.REQUIRED;
            return this;
        }

        public Execution nestedTransaction() {
            this.transaction = Propagation.NESTED;
            return this;
        }

        public String getDescription() {
            return description;
        }

        public Execution logIfFailedInSilentlyMode(String description) {
            this.description = description;
            return this;
        }

        public Callable<T> getCallable() {
            return callable;
        }

        public boolean isSilently() {
            return silently;
        }

        public Execution silently() {
            this.silently = true;
            return this;
        }

        public boolean isRemoveLockEntryFromDbInCaseOfSuccess() {
            return removeLockEntryFromDbInCaseOfSuccess;
        }

        public Execution removeLockEntryFromDbInCaseOfSuccess() {
            this.removeLockEntryFromDbInCaseOfSuccess = true;
            return this;
        }
    }
}