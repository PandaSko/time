package com.bsuir.time.common;

import com.google.common.collect.Lists;
import org.postgresql.util.PGInterval;
import org.springframework.util.CollectionUtils;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class JdbcUtil {

    public static final int FETCH_SIZE_500 = 500;
    public static final int FETCH_SIZE_1000 = 1000;
    public static final int FETCH_SIZE_10_000 = 10_000;

    public static Set<String> getNullableSetOfStringsFor(String fieldName, ResultSet rs) throws SQLException {
        Array array = rs.getArray(fieldName);
        if (rs.wasNull()) {
            return null;
        }
        String[] arr = new String[]{};
        if (array != null) {
            arr = (String[]) array.getArray();
        }
        return Arrays.stream(arr).collect(Collectors.toSet());
    }

    public static Set<String> getNullableSetOfNonNullableStringsFor(String fieldName, ResultSet rs) throws SQLException {
        Array array = rs.getArray(fieldName);
        if (rs.wasNull()) {
            return null;
        }
        String[] arr = new String[]{};
        if (array != null) {
            arr = (String[]) array.getArray();
        }
        return Arrays.stream(arr).filter(Objects::nonNull).collect(Collectors.toSet());
    }

    private static Set<Long> getNullableSetOfLongsFor(String fieldName, ResultSet rs) throws SQLException {
        Array array = rs.getArray(fieldName);
        if (rs.wasNull()) {
            return null;
        }
        Long[] arr = new Long[]{};
        if (array != null) {
            arr = (Long[]) array.getArray();
        }
        return Arrays.stream(arr).collect(Collectors.toSet());
    }

    public static Set<String> getNotNullableSetOfStringsFor(String fieldName, ResultSet rs) throws SQLException {
        Set<String> result = getNullableSetOfStringsFor(fieldName, rs);
        if (result == null) {
            result = new HashSet<>();
        }

        return result;
    }

    public static Set<Long> getNotNullableSetOfLongsFor(String fieldName, ResultSet rs) throws SQLException {
        Set<Long> result = getNullableSetOfLongsFor(fieldName, rs);
        if (result == null) {
            result = new HashSet<>();
        }

        return result;
    }

    public static List<LocalDatePeriod> getNotNullableSetOfLocalDatesFor(String fieldName, ResultSet rs) throws SQLException {
        List<LocalDatePeriod> result = new ArrayList<>();
        LocalDatePeriod defaultLocalDatePeriod = new LocalDatePeriod();
        defaultLocalDatePeriod.setStartDate(null);
        defaultLocalDatePeriod.setEndDate(null);

        Array array = rs.getArray(fieldName);
        if (rs.wasNull()) {
            result.add(defaultLocalDatePeriod);
            return result;
        }

        java.sql.Date[] arr = new java.sql.Date[]{};
        if (array != null) {
            arr = (java.sql.Date[]) array.getArray();
        }

        if (arr.length % 2 != 0) {
            throw new RuntimeException("Periods should have even number of dates");
        }


        if (arr.length == 0) {
            result.add(defaultLocalDatePeriod);
            return result;
        }

        for (int i = 0; i < arr.length; i += 2) {
            LocalDatePeriod localDatePeriod = new LocalDatePeriod();
            localDatePeriod.setStartDate(arr[i] != null ? arr[i].toLocalDate() : null);
            localDatePeriod.setEndDate(arr[i + 1] != null ? arr[i + 1].toLocalDate() : null);
            if (localDatePeriod.getStartDate() != null && localDatePeriod.getEndDate() != null && localDatePeriod.getStartDate().isAfter(localDatePeriod.getEndDate())) {
                throw new RuntimeException("Start date is after end date");
            }
            result.add(localDatePeriod);
        }

        return result;
    }

    public static Duration getDuration(String fieldName, ResultSet rs) throws SQLException {
        PGInterval source = (PGInterval) rs.getObject(fieldName);
        final long NANO_SCALE = 1_000_000_000;
        if (source == null) {
            return null;
        }

        int seconds = (int) source.getSeconds();
        long nanos = (long) (source.getSeconds() * NANO_SCALE - (double) seconds * NANO_SCALE);
        Duration result = Duration.ofHours(source.getHours()).plusMinutes(source.getMinutes())
                .plusSeconds(seconds).plusNanos(nanos);
        return result;
    }

    public static int getFetchSizeForTime() {
        return FETCH_SIZE_10_000;
    }

    public static Array getArrayForLocalDate(PreparedStatement pStmt, List<LocalDate> elements) throws SQLException {
        return pStmt.getConnection().createArrayOf("date", elements.toArray());
    }

    public static Array getArrayForString(PreparedStatement pStmt, Collection<String> elements) throws SQLException {
        return pStmt.getConnection().createArrayOf("text", elements.toArray());
    }

    public static Array getArrayForLong(PreparedStatement pStmt, Set<Long> elements) throws SQLException {
        return pStmt.getConnection().createArrayOf("bigint", elements.toArray());
    }

    public static <T extends Enum<T>> T getEnum(String fieldName, ResultSet rs, Class<T> enumType) throws SQLException {
        String rawValue = rs.getString(fieldName);
        if (rawValue == null) {
            return null;
        }
        T result = Enum.valueOf(enumType, rawValue);
        return result;
    }

    public static <T, E> List<E> fetchPartitioned(Collection<T> params, Function<Collection<T>, Collection<E>> action, int fetchSize) {
        if (CollectionUtils.isEmpty(params)) {
            return Collections.emptyList();
        }
        return Lists.partition(new ArrayList<>(params), fetchSize).stream().flatMap(partition -> action.apply(partition).stream()).collect(Collectors.toList());
    }

    public static <T> void executePartitioned(Collection<T> params, Consumer<Collection<T>> action, int fetchSize) {
        if (CollectionUtils.isEmpty(params)) {
            return;
        }
        Lists.partition(new ArrayList<>(params), fetchSize).forEach(action);
    }

    public static <T,R> R fetchByIds(Function<Collection<T>, R> fetchFunc, Collection<T> params, R def){
        if(params.isEmpty()){
            return def;
        }
        return fetchFunc.apply(params);
    }

    public static <T,R> List<R> fetchByIds(Function<Collection<T>, List<R>> fetchFunc, Collection<T> params){
        if(params.isEmpty()){
            return new ArrayList<>();
        }
        return fetchFunc.apply(params);
    }
}
