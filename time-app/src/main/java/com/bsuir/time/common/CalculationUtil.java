package com.bsuir.time.common;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class CalculationUtil {

    private static final int SCALE_2 = 2;

    public static double round(double value) {
        return round(new BigDecimal(Double.toString(value))).doubleValue();
    }

    public static BigDecimal round(BigDecimal value) {
        return value.setScale(SCALE_2, RoundingMode.HALF_UP);
    }
}
