package com.bsuir.time.common;

import java.time.LocalDate;

public class ConflictException extends RuntimeException {
    private final Code code;

    private ConflictException(Code code, String humanReadableMessage) {
        super(humanReadableMessage);
        this.code = code;
    }

    public static ConflictException throwNoProjectForAssignment(long assignmentUid) {
        return new ConflictException(null, "no project found for assignment " + assignmentUid);
    }

    public static ConflictException throwThereIsManagerLock(long employeeUid, long projectUid, LocalDate date) {
        return new ConflictException(Code.MANAGER_LOCK, "There's reporting lock for day " + date);
    }

    public static ConflictException throwNoWorklog(long worklogUid) {
        return new ConflictException(Code.WORKLOG, "Worklog with identifier " + worklogUid + " is not known");
    }

    public static ConflictException throwNoAssignment(long assignmentUid) {
        return new ConflictException(Code.ASSIGNMENT, "Assignment with identifier " + assignmentUid + " is not known");
    }

    public static ConflictException throwNoAssignmentOnDate(long assignmentUid, LocalDate date) {
        return new ConflictException(Code.ASSIGNMENT_ON_DATE, "Assignment with identifier " + assignmentUid + " is not appropriate for a date " + date);
    }

    public static ConflictException throwNoPosition(long employeeUid, long projectUid, LocalDate date) {
        return new ConflictException(Code.NO_POSITION, "No position for " + date);
    }

    public static ConflictException throwNoPackage(long packageId) {
        return new ConflictException(Code.NO_PACKAGE, "No package with id " + packageId);
    }

    public static ConflictException throwAfterExitDate() {
        return new ConflictException(Code.AFTER_EXIT_DATE, "Out of employee active dates");
    }

    public static ConflictException throwCanNotDeletePackage() {
        return new ConflictException(Code.REFERENCED_PACK_DELETE, "Cannot delete billing package");
    }

    public Code getCode() {
        return code;
    }

    public String getHumanReadableMessage() {
        return this.getMessage();
    }


    public enum Code {
        MANAGER_LOCK,
        WORKLOG,
        ASSIGNMENT,
        ASSIGNMENT_ON_DATE,
        NO_POSITION,
        NO_PACKAGE,
        AFTER_EXIT_DATE,
        REFERENCED_PACK_DELETE,
    }
}
