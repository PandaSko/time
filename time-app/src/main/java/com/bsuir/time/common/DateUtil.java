package com.bsuir.time.common;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;

public final class DateUtil {

    public static LocalDate minOfNotNull(LocalDate... values) {
        return Stream.of(values).filter(Objects::nonNull).min(LocalDate::compareTo)
                .orElseThrow(() -> new IllegalArgumentException("At least some date should be not null"));
    }

    public static LocalDate maxOfNotNull(LocalDate... values) {
        return Stream.of(values).filter(Objects::nonNull).max(LocalDate::compareTo)
                .orElseThrow(() -> new IllegalArgumentException("At least some date should be not null"));
    }

    public static Set<LocalDate> period(LocalDate from, LocalDate to) {
        if (from == null || to == null) {
            throw new IllegalArgumentException();
        }
        if (from.isAfter(to)) {
            return Collections.emptySet();
        }
        Set<LocalDate> result = new HashSet<>();
        for (LocalDate date = from; !date.isAfter(to); date = date.plusDays(1)) {
            result.add(date);
        }
        return result;
    }

    public static Set<LocalDate> period(LocalDatePeriod localDatePeriod) {
        return period(localDatePeriod.getStartDate(), localDatePeriod.getEndDate());
    }

    public static Set<LocalDate> period(Collection<LocalDatePeriod> values) {
        Set<LocalDate> result = new HashSet<>();
        values.forEach(period -> result.addAll(DateUtil.period(period.getStartDate(), period.getEndDate())));
        return result;
    }

    public static Set<LocalDate> periodRestrictedAtDateAndAfter(LocalDate restrictionDate, Collection<LocalDatePeriod> values) {
        Set<LocalDate> result = period(values);
        if (restrictionDate == null) {
            return result;
        }
        result.removeIf(date -> !date.isBefore(restrictionDate));
        return result;
    }

    public static Set<LocalDate> periodRestrictedBefore(LocalDate restrictionDate, Collection<LocalDatePeriod> values) {
        Set<LocalDate> result = period(values);
        if (restrictionDate == null) {
            return result;
        }
        result.removeIf(date -> date.isBefore(restrictionDate));
        return result;
    }

    public static LocalDate minStartDatePreferNull(Collection<LocalDatePeriod> values) {
        for (LocalDatePeriod i : values) {
            if (i.getStartDate() == null) {
                return null;
            }
        }

        return values.stream().map(LocalDatePeriod::getStartDate).filter(Objects::nonNull).min(LocalDate::compareTo)
                .orElseThrow(() -> new RuntimeException("At least some date should be not null"));
    }

    public static LocalDate maxEndDatePreferNull(Collection<LocalDatePeriod> values) {
        for (LocalDatePeriod i : values) {
            if (i.getEndDate() == null) {
                return null;
            }
        }

        return values.stream().map(LocalDatePeriod::getEndDate).filter(Objects::nonNull).max(LocalDate::compareTo)
                .orElseThrow(() -> new RuntimeException("At least some date should be not null"));
    }

    public static boolean dayAtLeastInOneOfPeriods(LocalDate day, List<LocalDatePeriod> periods) {
        for (LocalDatePeriod period : periods) {
            if (dayInPeriod(day, period)) {
                return true;
            }
        }
        return false;
    }

    private static boolean dayInPeriod(LocalDate day, LocalDatePeriod period) {
        if (period.getStartDate() == null && period.getEndDate() == null) {
            return true;
        }
        if (period.getStartDate() != null && day.isBefore(period.getStartDate())) {
            return false;
        }
        if (period.getEndDate() != null && day.isAfter(period.getEndDate())) {
            return false;
        }
        return true;
    }

    public static boolean dayInPeriod(LocalDate day, LocalDateTime startDate, LocalDateTime endDate) {
        return !startDate.toLocalDate().isAfter(day) && (endDate == null || !endDate.toLocalDate().isBefore(day));
    }

    public static LocalDate quarterStart(LocalDate date) {
        LocalDate result = date.withMonth(1 + (date.getMonthValue() - 1) / 3 * 3).withDayOfMonth(1);
        return result;
    }
}
