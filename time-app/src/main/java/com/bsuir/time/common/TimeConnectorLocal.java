package com.bsuir.time.common;

import com.bsuir.time.core.service.other.TimeFacade;
import com.bsuir.time.worklog.dto.request.WorklogDeleteRequest;
import com.bsuir.time.worklog.dto.request.WorklogPostRequest;
import com.bsuir.time.worklog.dto.response.WorklogPostResponse;
import com.bsuir.time.worklog.rest.WorklogController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;

@Component
public class TimeConnectorLocal implements TimeFacade {

    @Autowired
    private WorklogController worklogController;

    @Override
    public Long addWorklog(Long taskId, Long pmcUserId, String activityName, double duration, LocalDate date, String accountUid,
                           String sourceId) {
        WorklogPostRequest request = new WorklogPostRequest();
        request.setAssignmentUid(taskId);
        request.setEmployeeUid(pmcUserId);
        request.setComment(activityName);
        request.setHours(new BigDecimal(duration));
        request.setDate(date);
        request.setSourceSystemId(accountUid);
        request.setSourceId(sourceId);

        WorklogPostResponse response = worklogController.post(request);

        return response.getUid();
    }

    @Override
    public Long updateWorklogForcibly(Long pmcUserId, Long pmcTaskId, String activityName, double duration, LocalDate date,
                                      long oldActivityId, String accountUid, String sourceId) {
        WorklogDeleteRequest deleteRequest = new WorklogDeleteRequest();
        deleteRequest.setUid(oldActivityId);

        WorklogPostRequest createRequest = new WorklogPostRequest();
        createRequest.setEmployeeUid(pmcUserId);
        createRequest.setAssignmentUid(pmcTaskId);
        createRequest.setComment(activityName);
        createRequest.setDate(date);
        createRequest.setHours(new BigDecimal(duration));
        createRequest.setSourceSystemId(accountUid);
        createRequest.setSourceId(sourceId);

        worklogController.delete(deleteRequest);
        WorklogPostResponse createResponse = worklogController.post(createRequest);

        return createResponse.getUid();
    }

    @Override
    public void deleteWorklog(long id) {
        WorklogDeleteRequest request = new WorklogDeleteRequest();
        request.setUid(id);
        worklogController.delete(request);
    }
}
