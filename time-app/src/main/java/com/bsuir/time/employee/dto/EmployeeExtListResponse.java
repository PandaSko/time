package com.bsuir.time.employee.dto;

import java.util.ArrayList;
import java.util.List;

public class EmployeeExtListResponse {
    private List<EmployeeExtResponse> employees = new ArrayList<>();

    public List<EmployeeExtResponse> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeExtResponse> employees) {
        this.employees = employees;
    }
}
