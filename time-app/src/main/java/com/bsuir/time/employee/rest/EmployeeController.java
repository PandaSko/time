package com.bsuir.time.employee.rest;

import com.bsuir.time.dao.EmployeeDao;
import com.bsuir.time.domain.Employee;
import com.bsuir.time.employee.dto.EmployeeExtListResponse;
import com.bsuir.time.employee.dto.EmployeeExtResponse;
import com.bsuir.time.employee.dto.EmployeeListResponse;
import com.bsuir.time.employee.dto.EmployeeRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping(EmployeeController.API_ADDRESS)
@Api(tags = "employee", description = "employee api")
public class EmployeeController {

    public static final String API_ADDRESS = "/api/employee";

    @Autowired
    private EmployeeDao employeeDao;

    @GetMapping("/by-manager-uid")
    @ApiOperation(value = "Get employee by manager")
    public EmployeeExtListResponse getEmployeesByManagerUid(@RequestParam Long managerUid) {
        EmployeeExtListResponse response = new EmployeeExtListResponse();
        Employee employee = employeeDao.findById(managerUid).orElseThrow(RuntimeException::new);
        response.setEmployees(fetchRecEmployees(managerUid, employee.getFullName()));
        return response;
    }

    private List<EmployeeExtResponse> fetchRecEmployees(Long managerUid, String name) {
        List<EmployeeExtResponse> response = new ArrayList<>();
        List<Employee> employees = employeeDao.findByManagerUid(managerUid);
        Map<Long, Employee> uidToEmployee = employees.stream()
                .collect(Collectors.toMap(Employee::getUid, Function.identity()));
        response.addAll(employees.stream().map(employee -> {
            EmployeeExtResponse result = new EmployeeExtResponse();
            result.setEmail(employee.getEmail());
            result.setFired(employee.getFired());
            result.setFullName(employee.getFullName());
            result.setManagerName(name);
            result.setHired(employee.getHired());
            result.setManagerUid(managerUid);
            result.setTitle(employee.getTitle());
            result.setUid(employee.getUid());
            return result;
        }).collect(Collectors.toList()));
        for (Long uid : uidToEmployee.keySet()) {
            response.addAll(fetchRecEmployees(uid, uidToEmployee.get(uid).getFullName()));
        }
        return response;
    }

    @GetMapping
    @ApiOperation(value = "Get employee")
    public EmployeeListResponse getEmployees() {
        EmployeeListResponse response = new EmployeeListResponse();
        response.setEmployees(employeeDao.findAll());
        return response;
    }

    @PutMapping
    @ApiOperation(value = "Change employee")
    public void changeEmployee(@RequestParam Long uid,
                               @RequestBody @Validated EmployeeRequest request) {
        validateEmployeeRequest(request);
        Employee employee = convertToEmployee(uid, request);
        employeeDao.save(employee);
    }

    @PostMapping
    @ApiOperation(value = "Create employee")
    public void createEmployee(@RequestBody @Validated EmployeeRequest request) {
        validateEmployeeRequest(request);
        Employee employee = convertToEmployee(request);
        employeeDao.save(employee);
    }

    @DeleteMapping
    @ApiOperation(value = "Delete employee")
    public void deleteEmployee(@RequestParam Long uid) {
        employeeDao.deleteById(uid);
    }

    private void validateEmployeeRequest(EmployeeRequest request) {
        if (request.getManagerUid() != null) {
            if (request.getManagerUid() == 0) {
                throw new RuntimeException("Restrict to use global admin");
            }
            employeeDao.findById(request.getManagerUid()).orElseThrow(() -> new RuntimeException("Manager not found"));
        }
        if (request.getFired() != null && request.getFired().isBefore(request.getHired())) {
            throw new RuntimeException("Employee can't be fired before hire");
        }
    }

//    private void fillManagerUidsProjection(Employee employee) {
//        if (employee.getProjectUid() != null) {
//            Employee manager = employeeDao.findById(employee.getProjectUid())
//                    .orElseThrow(() -> new RuntimeException("Manager not found"));
//
//            List<Long> managerUidsProjection = manager.getManagerUidsProjection();
//            managerUidsProjection.add(manager.getUid());
//            employee.setManagerUidsProjection(managerUidsProjection);
//
//        } else {
//            employee.setManagerUidsProjection(List.of());
//        }
//    }

    private Employee convertToEmployee(Long uid, EmployeeRequest request) {
        Employee employee = convertToEmployee(request);
        employee.setUid(uid);
        return employee;
    }

    private Employee convertToEmployee(EmployeeRequest request) {
        Employee employee = new Employee();
        employee.setEmail(request.getEmail());
        employee.setFired(request.getFired());
        employee.setFullName(request.getFullName());
        employee.setHired(request.getHired());
        employee.setManagerUid(request.getManagerUid());
        employee.setTimeReportingRequired(request.getTimeReportingRequired());
        employee.setTitle(request.getTitle());
        return employee;
    }
}
