package com.bsuir.time.employee.dto;

import com.bsuir.time.domain.Employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeListResponse {

    private List<Employee> employees = new ArrayList<>();

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
