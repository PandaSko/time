package com.bsuir.time.employee.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class EmployeeRequest {
    @NotBlank
    private String fullName;
    @NotBlank
    private String title;
    private Long managerUid;
    @Email
    @NotNull
    private String email;
    private LocalDate fired;
    @NotNull
    private LocalDate hired;
    @NotNull
    private Boolean timeReportingRequired;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getManagerUid() {
        return managerUid;
    }

    public void setManagerUid(Long managerUid) {
        this.managerUid = managerUid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getFired() {
        return fired;
    }

    public void setFired(LocalDate fired) {
        this.fired = fired;
    }

    public LocalDate getHired() {
        return hired;
    }

    public void setHired(LocalDate hired) {
        this.hired = hired;
    }

    public Boolean getTimeReportingRequired() {
        return timeReportingRequired;
    }

    public void setTimeReportingRequired(Boolean timeReportingRequired) {
        this.timeReportingRequired = timeReportingRequired;
    }
}
