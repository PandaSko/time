package com.bsuir.time.tms_mapping;

import com.bsuir.time.core.dao.*;
import com.bsuir.time.dao.EmployeeDao;
import com.bsuir.time.dao.PackageDao;
import com.bsuir.time.dao.ProjectDao;
import com.bsuir.time.tms_mapping.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(TmsMappingController.API_ADDRESS)
@Api(tags = "tms-mapping", description = "tms mapping api")
public class TmsMappingController {

    public static final String API_ADDRESS = "/api/tms-mapping";

    @Autowired
    private TmsReportingItemMappingDao tmsReportingItemMappingDao;
    @Autowired
    private TmsProjectMappingDao tmsProjectMappingDao;
    @Autowired
    private TmsUserMappingDao tmsUserMappingDao;
    @Autowired
    private TmsAccountDao tmsAccountDao;
    @Autowired
    private TmsUserDao tmsUserDao;
    @Autowired
    private TmsReportingItemDao tmsReportingItemDao;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private PackageDao packageDao;
    @Autowired
    private ProjectDao projectDao;

    @GetMapping("/reporting-item")
    @ApiOperation(value = "Get reporting item mapping by accountId")
    public ReportingItemMappingListResponse getReportingItemMappingsByAccountId(@RequestParam Long accountId){
        ReportingItemMappingListResponse response = new ReportingItemMappingListResponse();
        response.setReportingItemMappings(tmsReportingItemMappingDao.findByAccountId(accountId));
        return response;
    }

    @PostMapping("/reporting-item")
    @ApiOperation(value = "Create reporting item mapping")
    public void createReportingItemMapping(@RequestBody @Validated TmsReportingItemMappingRequest request) {
        validateTmsReportingItemMappingRequest(request);
        tmsReportingItemMappingDao.insertMapping(request.getAccountId(), request.getReportingItemId(),
                request.getTimePackageId(), request.getStartDate(), request.getEndDate());
    }

    @DeleteMapping("/reporting-item")
    @ApiOperation(value = "Delete reporting item mapping")
    public void deleteReportingItemMapping(@RequestParam Long accountId,
                                           @RequestParam String reportingItemId) {
        tmsReportingItemMappingDao.deleteMapping(accountId, reportingItemId);
    }

    @GetMapping("/project")
    @ApiOperation(value = "Get project mapping by accountId")
    public ProjectMappingListResponse getProjectMappingsByAccountId(@RequestParam Long accountId) {
        ProjectMappingListResponse response = new ProjectMappingListResponse();
        response.setTmsProjectMappings(tmsProjectMappingDao.findByAccountId(accountId));
        return response;
    }

    @PostMapping("/project")
    @ApiOperation(value = "Create project mapping")
    public void createProjectMapping(@RequestBody @Validated TmsProjectMappingRequest request) {
        validateTmsProjectMappingRequest(request);
        tmsProjectMappingDao.insertMapping(request.getAccountId(), request.getReportingItemId(),
                request.getProjectUid());
    }

    @DeleteMapping("/project")
    @ApiOperation(value = "Delete project mapping")
    public void deleteProjectMapping(@RequestParam Long accountId,
                                     @RequestParam String reportingItemId) {
        tmsProjectMappingDao.deleteMapping(accountId, reportingItemId);
    }

    @GetMapping("/user")
    @ApiOperation(value = "Get user mapping by accountId")
    public UserMappingListResponse getUserMappingsByAccountId(@RequestParam Long accountId) {
        UserMappingListResponse response = new UserMappingListResponse();
        response.setUserMappings(tmsUserMappingDao.findByAccountId(accountId));
        return response;
    }

    @PostMapping("/user")
    @ApiOperation(value = "Create user mapping")
    public void createUserMapping(@RequestBody @Validated TmsUserMappingRequest request) {
        validateTmsUserMappingRequest(request);
        tmsUserMappingDao.createMapping(request.getAccountId(), request.getSourceId(), request.getEmployeeUid());
    }

    @DeleteMapping("/user")
    @ApiOperation(value = "Delete user mapping")
    public void deleteUserMapping(@RequestParam Long accountId,
                                  @RequestParam String sourceId) {
        tmsUserMappingDao.deleteMapping(accountId, sourceId);
    }

    private void validateTmsReportingItemMappingRequest(TmsReportingItemMappingRequest request) {
        tmsAccountDao.findById(request.getAccountId()).orElseThrow(() -> new RuntimeException("Account not found"));
        packageDao.findById(request.getTimePackageId()).orElseThrow(() -> new RuntimeException("Package not found"));
        if (!tmsReportingItemDao.existsReportingItem(request.getAccountId(), request.getReportingItemId())) {
            throw new RuntimeException("Reporting item not found");
        }
    }

    private void validateTmsProjectMappingRequest(TmsProjectMappingRequest request) {
        tmsAccountDao.findById(request.getAccountId()).orElseThrow(() -> new RuntimeException("Account not found"));
        projectDao.findById(request.getProjectUid()).orElseThrow(() -> new RuntimeException("Project not found"));
        if (!tmsReportingItemDao.existsReportingItem(request.getAccountId(), request.getReportingItemId())) {
            throw new RuntimeException("Reporting item not found");
        }
    }

    private void validateTmsUserMappingRequest(TmsUserMappingRequest request) {
        tmsAccountDao.findById(request.getAccountId()).orElseThrow(() -> new RuntimeException("Account not found"));
        employeeDao.findById(request.getEmployeeUid()).orElseThrow(() -> new RuntimeException("Employee not found"));
        if (!tmsUserDao.existsUser(request.getAccountId(), request.getSourceId())) {
            throw new RuntimeException("User not found");
        }
    }
}
