package com.bsuir.time.tms_mapping.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class TmsProjectMappingRequest {
    @NotNull
    private Long accountId;
    @NotBlank
    private String reportingItemId;
    @NotNull
    private Long projectUid;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getReportingItemId() {
        return reportingItemId;
    }

    public void setReportingItemId(String reportingItemId) {
        this.reportingItemId = reportingItemId;
    }

    public Long getProjectUid() {
        return projectUid;
    }

    public void setProjectUid(Long projectUid) {
        this.projectUid = projectUid;
    }
}
