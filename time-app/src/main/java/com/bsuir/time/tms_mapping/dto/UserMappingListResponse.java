package com.bsuir.time.tms_mapping.dto;

import com.bsuir.time.core.dto.TmsUserMapping;

import java.util.ArrayList;
import java.util.List;

public class UserMappingListResponse {

    private List<TmsUserMapping> userMappings = new ArrayList<>();

    public List<TmsUserMapping> getUserMappings() {
        return userMappings;
    }

    public void setUserMappings(List<TmsUserMapping> userMappings) {
        this.userMappings = userMappings;
    }
}
