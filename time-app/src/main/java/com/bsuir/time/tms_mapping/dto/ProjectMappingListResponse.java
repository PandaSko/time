package com.bsuir.time.tms_mapping.dto;

import com.bsuir.time.core.dto.TmsProjectMapping;

import java.util.ArrayList;
import java.util.List;

public class ProjectMappingListResponse {

    private List<TmsProjectMapping> tmsProjectMappings = new ArrayList<>();

    public List<TmsProjectMapping> getTmsProjectMappings() {
        return tmsProjectMappings;
    }

    public void setTmsProjectMappings(List<TmsProjectMapping> tmsProjectMappings) {
        this.tmsProjectMappings = tmsProjectMappings;
    }
}
