package com.bsuir.time.tms_mapping.dto;

import com.bsuir.time.core.dto.TmsReportingItemMapping;

import java.util.ArrayList;
import java.util.List;

public class ReportingItemMappingListResponse {
    private List<TmsReportingItemMapping> reportingItemMappings = new ArrayList<>();

    public List<TmsReportingItemMapping> getReportingItemMappings() {
        return reportingItemMappings;
    }

    public void setReportingItemMappings(List<TmsReportingItemMapping> reportingItemMappings) {
        this.reportingItemMappings = reportingItemMappings;
    }
}
