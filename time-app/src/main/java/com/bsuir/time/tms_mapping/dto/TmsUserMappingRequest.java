package com.bsuir.time.tms_mapping.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class TmsUserMappingRequest {
    @NotNull
    private Long accountId;
    @NotBlank
    private String sourceId;
    @NotNull
    private Long employeeUid;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public Long getEmployeeUid() {
        return employeeUid;
    }

    public void setEmployeeUid(Long employeeUid) {
        this.employeeUid = employeeUid;
    }
}
