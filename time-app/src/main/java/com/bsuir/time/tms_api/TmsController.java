package com.bsuir.time.tms_api;

import com.bsuir.time.common.ExecutionWrapper;
import com.bsuir.time.core.dao.TmsReportingItemDao;
import com.bsuir.time.core.dao.TmsUserDao;
import com.bsuir.time.core.dao.TmsUserMappingDao;
import com.bsuir.time.core.dto.TmsReportingItemDto;
import com.bsuir.time.core.dto.TmsUserDto;
import com.bsuir.time.core.dto.TmsWorklog;
import com.bsuir.time.core.provider.artifact.TmsReportingItemProviderApi;
import com.bsuir.time.core.provider.artifact.TmsUserProviderApi;
import com.bsuir.time.core.provider.other.TmsWorklogProvaiderApi;
import com.bsuir.time.core.service.other.TimeFacade;
import com.bsuir.time.core.service.unified.UnifiedReportingItemSyncService;
import com.bsuir.time.core.service.unified.UnifiedUserSyncService;
import com.bsuir.time.core.service.unified.UnifiedWorklogSyncService;
import com.bsuir.time.tms_api.dto.TmsReportingItem;
import com.bsuir.time.tms_api.dto.TmsReportingItemsResponse;
import com.bsuir.time.tms_api.dto.TmsUser;
import com.bsuir.time.tms_api.dto.TmsUsersResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(TmsController.API_ADDRESS)
@Api(TmsController.SWAGGER_API_NAME)
public class TmsController {
    public static final String API_ADDRESS = "/api/tms/tms-api";
    public static final String SWAGGER_API_NAME = "tms api";
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ExecutionWrapper executionWrapper;
    @Autowired
    @Qualifier("timeJdbcTemplate")
    private JdbcTemplate timeJdbcTemplate;
    @Autowired
    private TmsUserDao tmsUserDao;
    @Autowired
    private TmsReportingItemDao tmsReportingItemDao;
    @Autowired
    private TmsUserMappingDao tmsUserMappingDao;
    @Autowired
    private TimeFacade timeFacade;

    @PutMapping("/worklogs")
    @ApiOperation("Populate worklogs")
    public void populateWorklogs(@RequestParam long accountId,
                                 @RequestBody List<TmsWorklog> worklogs,
                                 @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam("yyyy-MM-dd") LocalDate from,
                                 @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam("yyyy-MM-dd") LocalDate to,
                                 boolean resynchronize) {
        try {
            timeJdbcTemplate.queryForObject("select id from tms_account where id = ? and type = 'Global'",
                    Long.class, accountId);


            executionWrapper.build(() -> {
                TmsWorklogProvaiderApi provaiderApi = new TmsWorklogProvaiderApi(accountId, from, to, worklogs);
                UnifiedWorklogSyncService.process(provaiderApi, resynchronize, timeJdbcTemplate,
                        tmsUserMappingDao, timeFacade);
            })
                    .transaction()
                    .lock("sync.worklogs.global:" + accountId)
                    .removeLockEntryFromDbInCaseOfSuccess()
                    .logIfFailedInSilentlyMode("[sync.worklogs.global:" + accountId + "] import triggered manually")
                    .execute();
        } catch (Throwable th) {
            logger.info("populate worklogs by api", th);
            throw th;
        }
    }

    @PutMapping("/reporting-items")
    @ApiOperation("Populate reporting items")
    public void populateReportingItems(@RequestParam long accountId, @RequestBody List<TmsReportingItem> reportingItems) {
        try {
            timeJdbcTemplate.queryForObject("select id from tms_account where id = ? and type = 'Global'",
                    Long.class, accountId);
            List<TmsReportingItemDto> reportingItemDtos = reportingItems.stream().map(reportingItem -> {
                TmsReportingItemDto reportingItemDto = new TmsReportingItemDto();
                reportingItemDto.setAccountId(accountId);
                reportingItemDto.setName(reportingItem.getName());
                reportingItemDto.setParentSourceId(reportingItem.getParentSourceId());
                reportingItemDto.setSourceId(reportingItem.getSourceId());
                return reportingItemDto;
            }).collect(Collectors.toList());

            executionWrapper.build(() -> {
                List<String> ids = tmsReportingItemDao.fetchSourceIdsByAccountId(accountId);
                UnifiedReportingItemSyncService syncService = new UnifiedReportingItemSyncService(timeJdbcTemplate);
                syncService.syncWith(new TmsReportingItemProviderApi(accountId, reportingItemDtos, ids));
            })
                    .transaction()
                    .lock("sync.reporting-items.global:" + accountId)
                    .removeLockEntryFromDbInCaseOfSuccess()
                    .logIfFailedInSilentlyMode("[sync.reporting-items.global:" + accountId + "] import triggered manually")
                    .execute();
        } catch (Throwable th) {
            logger.info("populate reporting items by api", th);
            throw th;
        }
    }

    @GetMapping("/reporting-items")
    @ApiOperation("Get reporting items")
    public TmsReportingItemsResponse getReportingItems(@RequestParam long accountId) {//todo probably need to add partition
        timeJdbcTemplate.queryForObject("select id from tms_account where id = ? and type = 'Global'",
                Long.class, accountId);

        TmsReportingItemsResponse response = new TmsReportingItemsResponse();

        List<TmsReportingItemDto> reportingItemDtos = tmsReportingItemDao.fetchByAccountId(accountId);
        response.setReportingItems(convertTmsReportingItemDtoToTmsReportingItem(reportingItemDtos));

        return response;
    }

    @PutMapping("/users")
    @ApiOperation("Populate users")
    public void populateUsers(@RequestParam long accountId, @RequestBody List<TmsUser> users) {
        try {
            timeJdbcTemplate.queryForObject("select id from tms_account where id = ? and type = 'Global'",
                    Long.class, accountId);
            List<TmsUserDto> tmsUserDtos = users.stream().map(user -> {
                TmsUserDto userDto = new TmsUserDto();
                userDto.setAccountId(accountId);
                userDto.setName(user.getName());
                userDto.setSourceId(user.getSourceId());
                return userDto;
            }).collect(Collectors.toList());

            executionWrapper.build(() -> {
                List<String> userKeys = tmsUserDao.fetchSourceIdsByAccountId(accountId);
                UnifiedUserSyncService syncService = new UnifiedUserSyncService(timeJdbcTemplate);
                syncService.syncWith(new TmsUserProviderApi(accountId, tmsUserDtos, userKeys));
            })
                    .transaction()
                    .lock("sync.users.global:" + accountId)
                    .removeLockEntryFromDbInCaseOfSuccess()
                    .logIfFailedInSilentlyMode("[sync.users.global:" + accountId + "] import triggered manually")
                    .execute();
        } catch (Throwable th) {
            logger.info("populate users by api", th);
            throw th;
        }
    }

    @GetMapping("/users")
    @ApiOperation("Get users")
    public TmsUsersResponse getUsers(@RequestParam long accountId) {
        timeJdbcTemplate.queryForObject("select id from tms_account where id = ? and type = 'Global'",
                Long.class, accountId);

        List<TmsUserDto> tmsUserDtos = tmsUserDao.fetchByAccountId(accountId);
        TmsUsersResponse response = new TmsUsersResponse();
        response.setUsers(convertTmsUserDtoToTmsUser(tmsUserDtos));

        return response;
    }

    private List<TmsUser> convertTmsUserDtoToTmsUser(List<TmsUserDto> tmsUsers) {
        return tmsUsers.stream().map(tmsUserDto -> {
            TmsUser tmsUser = new TmsUser();
            tmsUser.setName(tmsUserDto.getName());
            tmsUser.setSourceId(tmsUserDto.getSourceId());
            return tmsUser;
        }).collect(Collectors.toList());
    }

    private List<TmsReportingItem> convertTmsReportingItemDtoToTmsReportingItem(List<TmsReportingItemDto> reportingItemDtos) {
        return reportingItemDtos.stream().map(tmsReportingItemDto -> {
            TmsReportingItem tmsReportingItem = new TmsReportingItem();
            tmsReportingItem.setParentSourceId(tmsReportingItemDto.getParentSourceId());
            tmsReportingItem.setSourceId(tmsReportingItemDto.getSourceId());
            tmsReportingItem.setName(tmsReportingItemDto.getName());
            return tmsReportingItem;
        }).collect(Collectors.toList());
    }
}
