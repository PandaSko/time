package com.bsuir.time.tms_api.dto;

import java.util.List;

public class TmsUsersResponse {
    private List<TmsUser> users;

    public List<TmsUser> getUsers() {
        return users;
    }

    public void setUsers(List<TmsUser> users) {
        this.users = users;
    }
}
