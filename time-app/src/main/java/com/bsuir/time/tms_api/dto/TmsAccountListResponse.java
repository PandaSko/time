package com.bsuir.time.tms_api.dto;

import com.bsuir.time.core.dto.TmsAccount;

import java.util.ArrayList;
import java.util.List;

public class TmsAccountListResponse {

    private List<TmsAccount> tmsAccounts = new ArrayList<>();

    public List<TmsAccount> getTmsAccounts() {
        return tmsAccounts;
    }

    public void setTmsAccounts(List<TmsAccount> tmsAccounts) {
        this.tmsAccounts = tmsAccounts;
    }
}
