package com.bsuir.time.tms_api.dto;

import java.util.List;

public class TmsReportingItemsResponse {
    private List<TmsReportingItem> reportingItems;

    public List<TmsReportingItem> getReportingItems() {
        return reportingItems;
    }

    public void setReportingItems(List<TmsReportingItem> reportingItems) {
        this.reportingItems = reportingItems;
    }
}
