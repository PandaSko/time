package com.bsuir.time.tms_api;

import com.bsuir.time.core.dao.TmsAccountDao;
import com.bsuir.time.core.dto.TmsAccount;
import com.bsuir.time.tms_api.dto.TmsAccountListResponse;
import com.bsuir.time.tms_api.dto.TmsAccountRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(TmsAccountController.API_ADDRESS)
@Api(tags = "tms-account", description = "tms account api")
public class TmsAccountController {

    public static final String API_ADDRESS = "/api/tms/tms-account";

    @Autowired
    private TmsAccountDao tmsAccountDao;

    @GetMapping
    @ApiOperation(value = "Get tms accounts")
    public TmsAccountListResponse getTmsAccounts() {
        TmsAccountListResponse response = new TmsAccountListResponse();
        response.setTmsAccounts(tmsAccountDao.findAll());
        return response;
    }

    @PutMapping
    @ApiOperation(value = "Change tms account")
    public void changeTmsAccount(@RequestParam Long id,
                                 @RequestBody @Validated TmsAccountRequest request) {
        tmsAccountDao.save(convertToTmsAccount(id, request));
    }

    @PostMapping
    @ApiOperation(value = "Create tms account")
    public void createTmsAccount(@RequestBody @Validated TmsAccountRequest request) {
        tmsAccountDao.save(convertToTmsAccount(request));
    }

    @DeleteMapping
    @ApiOperation(value = "Delete tms account")
    public void deleteTmsAccount(@RequestParam Long id) {
        tmsAccountDao.deleteById(id);
    }

    private TmsAccount convertToTmsAccount(Long id, TmsAccountRequest request) {
        TmsAccount tmsAccount = convertToTmsAccount(request);
        tmsAccount.setId(id);
        return tmsAccount;
    }

    private TmsAccount convertToTmsAccount(TmsAccountRequest request) {
        TmsAccount tmsAccount = new TmsAccount();
        tmsAccount.setName(request.getName());
        tmsAccount.setPreferResynchronization(request.getPreferResynchronization());
        tmsAccount.setUid(request.getUid());
        tmsAccount.setType(request.getType());
        return tmsAccount;
    }
}
