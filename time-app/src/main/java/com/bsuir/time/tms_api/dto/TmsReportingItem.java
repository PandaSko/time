package com.bsuir.time.tms_api.dto;

public class TmsReportingItem {
    private String sourceId;
    private String name;
    private String parentSourceId;

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentSourceId() {
        return parentSourceId;
    }

    public void setParentSourceId(String parentSourceId) {
        this.parentSourceId = parentSourceId;
    }
}
