package com.bsuir.time.tms_api.dto;

import javax.validation.constraints.NotBlank;

public class TmsAccountRequest {

    @NotBlank
    private String uid;
    @NotBlank
    private String type;
    @NotBlank
    private String name;
    private Boolean preferResynchronization;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPreferResynchronization() {
        return preferResynchronization;
    }

    public void setPreferResynchronization(Boolean preferResynchronization) {
        this.preferResynchronization = preferResynchronization;
    }
}
