package com.bsuir.time.tms_api.dto;

public class TmsUser {
    private String sourceId;
    private String name;

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
