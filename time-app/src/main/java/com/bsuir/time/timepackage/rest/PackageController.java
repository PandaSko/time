package com.bsuir.time.timepackage.rest;

import com.bsuir.time.dao.PackageDao;
import com.bsuir.time.dao.ProjectDao;
import com.bsuir.time.domain.Package;
import com.bsuir.time.domain.Project;
import com.bsuir.time.timepackage.dto.PackageListResponse;
import com.bsuir.time.timepackage.dto.PackageRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PackageController.API_ADDRESS)
@Api(tags = "package", description = "package api")
public class PackageController {

    public static final String API_ADDRESS = "/api/package";

    @Autowired
    private PackageDao packageDao;
    @Autowired
    private ProjectDao projectDao;

    @GetMapping
    @ApiOperation(value = "Get packages")
    public PackageListResponse getPackages() {
        PackageListResponse response = new PackageListResponse();
        response.setPackages(packageDao.findAll());
        return response;
    }

    @GetMapping("/by-project-uid")
    @ApiOperation(value = "Get packages by projectUid")
    public PackageListResponse getPackagesByProjectUid(@RequestParam Long projectUid) {
        PackageListResponse response = new PackageListResponse();
        response.setPackages(packageDao.findByProjectUid(projectUid));
        return response;
    }

    @PutMapping
    @ApiOperation(value = "Change package")
    public void changePackage(@RequestParam Long uid,
                              @RequestBody @Validated PackageRequest request) {
        validateUid(uid);
        validatePackageRequest(request);
        packageDao.save(convertToPackage(uid, request));
    }

    @PostMapping
    @ApiOperation(value = "Create package")
    public void createPackage(@RequestBody @Validated PackageRequest request) {
        validatePackageRequest(request);
        packageDao.save(convertToPackage(request));
    }

    @DeleteMapping
    @ApiOperation(value = "Delete package")
    public void deletePackage(@RequestParam Long uid) {
        validateUid(uid);
        packageDao.deleteById(uid);
    }

    private void validateUid(Long uid) {
        if (uid == 0) {
            throw new RuntimeException("Actions with npa package is restricted");
        }
    }

    private void validatePackageRequest(PackageRequest packageRequest) {
        if (packageRequest.getProjectUid() == 0) {
            throw new RuntimeException("Restrict to create position for npa project");
        }
        Project project = projectDao.findById(packageRequest.getProjectUid())
                .orElseThrow(() -> new RuntimeException("Project not found"));

        if (packageRequest.getStartDate().isBefore(project.getStartDate())) {
            throw new RuntimeException("Package start date should be after project start date");
        }
        if (packageRequest.getEndDate() != null && packageRequest.getEndDate().isBefore(packageRequest.getStartDate())) {
            throw new RuntimeException("Package end date should be after start date");
        }
    }

    private Package convertToPackage(Long uid, PackageRequest request) {
        Package timePackage = convertToPackage(request);
        timePackage.setUid(uid);
        return timePackage;
    }

    private Package convertToPackage(PackageRequest request) {
        Package timePackage = new Package();
        timePackage.setEndDate(request.getEndDate());
        timePackage.setProjectUid(request.getProjectUid());
        timePackage.setName(request.getName());
        timePackage.setStartDate(request.getStartDate());
        return timePackage;
    }

}
