package com.bsuir.time.timepackage.dto;

import com.bsuir.time.domain.Package;

import java.util.ArrayList;
import java.util.List;

public class PackageListResponse {

    private List<Package> packages = new ArrayList<>();

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }
}
