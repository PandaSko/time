package com.bsuir.time;

import org.springframework.boot.SpringApplication;

public class TimeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimeConfiguration.class, args);
    }

}
