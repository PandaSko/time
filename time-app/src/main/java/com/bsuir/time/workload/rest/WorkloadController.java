package com.bsuir.time.workload.rest;

import com.bsuir.time.dao.EmployeeDao;
import com.bsuir.time.dao.WorkloadDao;
import com.bsuir.time.domain.Workload;
import com.bsuir.time.workload.dto.WorkloadListResponse;
import com.bsuir.time.workload.dto.WorkloadRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(WorkloadController.API_ADDRESS)
@Api(tags = "workload", description = "workload api")
public class WorkloadController {

    public static final String API_ADDRESS = "/api/workload";

    @Autowired
    private WorkloadDao workloadDao;
    @Autowired
    private EmployeeDao employeeDao;

    @GetMapping
    @ApiOperation(value = "Get workloads")
    public WorkloadListResponse getWorkloads() {
        WorkloadListResponse response = new WorkloadListResponse();
        response.setWorkloads(workloadDao.findAll());
        return response;
    }

    @PutMapping
    @ApiOperation(value = "Change workload")
    public void changeWorkload(@RequestParam Long uid,
                               @RequestBody @Validated WorkloadRequest request) {
        validateWorkloadRequest(request);
        Workload workload = convertToWorkload(uid, request);
        workloadDao.save(workload);
    }

    @PostMapping
    @ApiOperation(value = "Create workload")
    public void createWorkload(@RequestBody @Validated WorkloadRequest request) {
        validateWorkloadRequest(request);
        Workload workload = convertToWorkload(request);
        workloadDao.save(workload);
    }

    @DeleteMapping
    @ApiOperation(value = "Delete workload")
    public void deleteWorkload(@RequestParam Long uid) {
        workloadDao.deleteById(uid);
    }


    private void validateWorkloadRequest(WorkloadRequest workloadRequest) {
        employeeDao.findById(workloadRequest.getEmployeeUid())
                .orElseThrow(() -> new RuntimeException("Employee not found"));
    }

    private Workload convertToWorkload(Long uid, WorkloadRequest request) {
        Workload workload = convertToWorkload(request);
        workload.setUid(uid);
        return workload;
    }

    private Workload convertToWorkload(WorkloadRequest request) {
        Workload workload = new Workload();
        workload.setDuration(request.getDuration());
        workload.setEmployeeUid(request.getEmployeeUid());
        workload.setFromDate(request.getFromDate());
        return workload;
    }
}
