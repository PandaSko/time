package com.bsuir.time.workload.dto;

import com.bsuir.time.domain.Workload;

import java.util.ArrayList;
import java.util.List;

public class WorkloadListResponse {

    private List<Workload> workloads = new ArrayList<>();

    public List<Workload> getWorkloads() {
        return workloads;
    }

    public void setWorkloads(List<Workload> workloads) {
        this.workloads = workloads;
    }
}
