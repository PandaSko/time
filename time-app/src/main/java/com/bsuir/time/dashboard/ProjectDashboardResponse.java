package com.bsuir.time.dashboard;

import java.util.ArrayList;
import java.util.List;

public class ProjectDashboardResponse {
    private List<ProjectResponse> projects = new ArrayList<>();

    public List<ProjectResponse> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectResponse> projects) {
        this.projects = projects;
    }
}
