package com.bsuir.time.dashboard;

import com.bsuir.time.core.dao.TmsProjectMappingDao;
import com.bsuir.time.core.dto.TmsProjectMapping;
import com.bsuir.time.dao.EmployeeDao;
import com.bsuir.time.dao.LockDao;
import com.bsuir.time.dao.PositionDao;
import com.bsuir.time.dao.ProjectDao;
import com.bsuir.time.domain.Employee;
import com.bsuir.time.domain.Lock;
import com.bsuir.time.domain.Position;
import com.bsuir.time.domain.Project;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.bsuir.time.common.JdbcUtil.fetchByIds;
import static com.bsuir.time.time_journal.TimeJournalController.MIN_DATE;

@RestController
@RequestMapping(ProjectDashboardController.API_ADDRESS)
@Api(tags = "projectDashboard", description = "project dashboard api")
public class ProjectDashboardController {

    public static final String API_ADDRESS = "/api/project-dashboard";

    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private PositionDao positionDao;
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private LockDao lockDao;
    @Autowired
    private TmsProjectMappingDao tmsProjectMappingDao;

    @GetMapping
    @ApiOperation(value = "Get project dashboard info")
    public ProjectDashboardResponse getProjects(@RequestParam Long managerUid) {
        ProjectDashboardResponse response = new ProjectDashboardResponse();
        List<Project> projects = projectDao.findByManagerUid(managerUid);
        List<Long> projectUids = projects.stream().map(Project::getUid).collect(Collectors.toList());
        Map<Long, Lock> locksPerProject = fetchByIds(lockDao::findByProjectUids, projectUids).stream()
                .collect(Collectors.toMap(Lock::getProjectUid, Function.identity()));

        for(Project project : projects){
            ProjectResponse projectResponse = new ProjectResponse();
            projectResponse.setUid(project.getUid());
            projectResponse.setName(project.getName());
            projectResponse.setEndDate(project.getEndDate());
            LocalDate lockDate = Optional.ofNullable(locksPerProject.get(project.getUid()))
                    .map(Lock::getLockDate)
                    .orElse(null);
            projectResponse.setLockDate(lockDate);
            projectResponse.setStartDate(project.getStartDate());

            List<TmsProjectMapping> projectMappings = tmsProjectMappingDao.findByProjectUid(project.getUid());
            projectResponse.setTmsAvailable(!CollectionUtils.isEmpty(projectMappings));

            List<Position> positions = positionDao.findActualByProjectUid(project.getUid());
            for (Position position : positions) {
                Employee employee = employeeDao.findById(position.getEmployeeUid()).get();

                ProjectPositionResponse projectPositionResponse = new ProjectPositionResponse();
                projectPositionResponse.setEndDate(position.getEndDate());
                projectPositionResponse.setStartDate(position.getStartDate());
                projectPositionResponse.setRoleName(position.getPrimaryRoleName());
                projectPositionResponse.setUid(employee.getUid());
                projectPositionResponse.setName(employee.getFullName());
                projectResponse.getPositionsResponse().add(projectPositionResponse);
            }
            response.getProjects().add(projectResponse);
        }

        return response;
    }

}
