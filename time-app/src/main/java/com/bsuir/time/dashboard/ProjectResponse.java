package com.bsuir.time.dashboard;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ProjectResponse {
    private Long uid;
    private String name;
    private LocalDate startDate;
    private LocalDate endDate;
    private boolean isTmsAvailable;
    private LocalDate lockDate;
    private List<ProjectPositionResponse> positionsResponse = new ArrayList<>();

    public List<ProjectPositionResponse> getPositionsResponse() {
        return positionsResponse;
    }

    public void setPositionsResponse(List<ProjectPositionResponse> positionsResponse) {
        this.positionsResponse = positionsResponse;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public LocalDate getLockDate() {
        return lockDate;
    }

    public void setLockDate(LocalDate lockDate) {
        this.lockDate = lockDate;
    }

    public boolean isTmsAvailable() {
        return isTmsAvailable;
    }

    public void setTmsAvailable(boolean tmsAvailable) {
        isTmsAvailable = tmsAvailable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
