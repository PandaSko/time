package com.bsuir.time.lock;

import com.bsuir.time.dao.ProjectDao;
import com.bsuir.time.domain.Lock;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(LockController.API_ADDRESS)
@Api(tags = "lock", description = "lock api")
public class LockController {

    public static final String API_ADDRESS = "/api/lock";

    @Autowired
    private LockService lockService;
    @Autowired
    private ProjectDao projectDao;

    @GetMapping
    @ApiOperation(value = "Get lock for project")
    public LockResponse getLockByProject(@RequestParam Long projectUid) {
        LockResponse response = new LockResponse();
        LocalDate fromDate = LocalDate.MIN;
        Lock lock = lockService.findLock(projectUid).orElse(null);
        if (lock != null) {
            fromDate = lock.getLockDate();
        }
        response.setLockDate(fromDate);
        return response;
    }


    @PostMapping
    @ApiOperation(value = "Set lock")
    public void createProject(@RequestBody @Validated LockRequest request) {
        validateLockRequest(request);
        lockService.setLockDate(request.getProjectUid(), request.getLockDate());
    }

    private void validateLockRequest(LockRequest lockRequest) {
        if (lockRequest.getProjectUid() == 0) {
            throw new RuntimeException("Locks for npa project is restricted");
        }
        projectDao.findById(lockRequest.getProjectUid())
                .orElseThrow(() -> new RuntimeException("Project not found"));
    }

}
