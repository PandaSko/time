package com.bsuir.time.lock;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class LockRequest {
    @NotNull
    private Long projectUid;
    @NotNull
    private LocalDate lockDate;

    public Long getProjectUid() {
        return projectUid;
    }

    public void setProjectUid(Long projectUid) {
        this.projectUid = projectUid;
    }

    public LocalDate getLockDate() {
        return lockDate;
    }

    public void setLockDate(LocalDate lockDate) {
        this.lockDate = lockDate;
    }
}
