package com.bsuir.time.lock;

import com.bsuir.time.dao.LockDao;
import com.bsuir.time.domain.Lock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class LockService {

    @Autowired
    private LockDao lockDao;

    public void setLockDate(Long projectUid, LocalDate lockDate) {
        lockDao.findByProjectUid(projectUid).ifPresentOrElse(lock -> lockDao.updateLockDate(lockDate, projectUid),
                () -> lockDao.insertLockDate(lockDate, projectUid));
    }

    public Optional<Lock> findLock(Long projectUid){
        return lockDao.findByProjectUid(projectUid);
    }
}
