package com.bsuir.time.lock;

import java.time.LocalDate;

public class LockResponse {
    private LocalDate lockDate;

    public LocalDate getLockDate() {
        return lockDate;
    }

    public void setLockDate(LocalDate lockDate) {
        this.lockDate = lockDate;
    }
}
