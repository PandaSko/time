package com.bsuir.time.dao;

import com.bsuir.time.domain.Package;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PackageDao extends CustomCrudRepository<Package, Long> {

    @Query("select * from time_package where project_uid = :projectUid")
    List<Package> findByProjectUid(@Param("projectUid") Long projectUid);
}
