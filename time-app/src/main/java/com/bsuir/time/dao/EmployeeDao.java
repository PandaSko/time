package com.bsuir.time.dao;

import com.bsuir.time.domain.Employee;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeDao extends CustomCrudRepository<Employee, Long> {

    @Query("select * from employee where manager_uid = :managerUid")
    List<Employee> findByManagerUid(@Param("managerUid") Long managerUid);


}
