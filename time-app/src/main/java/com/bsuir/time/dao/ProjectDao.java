package com.bsuir.time.dao;

import com.bsuir.time.domain.Project;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProjectDao extends CustomCrudRepository<Project, Long> {

    @Query("select * from project where manager_uid = :managerUid")
    List<Project> findByManagerUid(@Param("managerUid")Long managerUid);

}
