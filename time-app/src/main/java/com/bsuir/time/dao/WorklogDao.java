package com.bsuir.time.dao;

import com.bsuir.time.domain.Worklog;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface WorklogDao extends CustomCrudRepository<Worklog, Long> {

    @Query("update worklog set activity_name = :comment, duration = :duration where uid = :uid")
    @Modifying
    void updateByUid(@Param("comment") String comment,
                            @Param("duration") double duration, @Param("uid") long uid);

    @Query("select * from worklog where employee_uid = :employeeUid and timesheet_date <= :to " +
            "and timesheet_date >= :from")
    List<Worklog> findAllByEmployeeUid(@Param("employeeUid") Long employeeUid,
                                          @Param("from") LocalDate from,
                                          @Param("to") LocalDate to);
}
