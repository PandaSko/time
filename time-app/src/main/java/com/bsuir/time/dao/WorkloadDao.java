package com.bsuir.time.dao;

import com.bsuir.time.domain.Workload;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WorkloadDao extends CustomCrudRepository<Workload, Long> {

    @Query("select * from workload where employee_uid = :employeeUid")
    List<Workload> findAllByEmployeeUid(@Param("employeeUid") Long employeeUid);
}
