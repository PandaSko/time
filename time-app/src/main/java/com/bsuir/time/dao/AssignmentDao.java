package com.bsuir.time.dao;

import com.bsuir.time.domain.Assignment;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface AssignmentDao extends CustomCrudRepository<Assignment, Long> {

    @Query("select * from assignment where employee_uid = :employeeUid and start_date <= :to " +
            "and (end_date is null or end_date >= :from)")
    List<Assignment> findAllByEmployeeUid(@Param("employeeUid") Long employeeUid,
                                        @Param("from") LocalDate from,
                                        @Param("to") LocalDate to);
}
