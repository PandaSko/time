package com.bsuir.time.dao;

import com.bsuir.time.domain.Package;
import com.bsuir.time.domain.Position;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface PositionDao extends CustomCrudRepository<Position, Long> {

    @Query("select * from position where employee_uid = :employeeUid and start_date <= :to " +
            "and (end_date is null or end_date >= :from)")
    List<Position> findAllByEmployeeUid(@Param("employeeUid") Long employeeUid,
                                        @Param("from") LocalDate from,
                                        @Param("to") LocalDate to);
    @Query("select * from position where project_uid = :projectUid " +
            "and (end_date is null or end_date >= now()) ")
    List<Position> findActualByProjectUid(@Param("projectUid") Long projectUid);
}
