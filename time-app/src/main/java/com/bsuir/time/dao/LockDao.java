package com.bsuir.time.dao;

import com.bsuir.time.domain.Lock;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface LockDao extends Repository<Lock, Void> {

    @Query("select * from manager_lock where project_uid = :projectUid")
    Optional<Lock> findByProjectUid(@Param("projectUid")Long projectUid);

    @Query("select * from manager_lock where project_uid in (:projectUids)")
    List<Lock> findByProjectUids(@Param("projectUids") Collection<Long> projectUid);

    @Modifying
    @Query("update manager_lock set lock_date = :lockDate where project_uid = :projectUid")
    void updateLockDate(@Param("lockDate")LocalDate lockDate, @Param("projectUid") Long projectUid);

    @Modifying
    @Query("insert into manager_lock values (:projectUid, :lockDate)")
    void insertLockDate(@Param("lockDate")LocalDate lockDate, @Param("projectUid") Long projectUid);
}
