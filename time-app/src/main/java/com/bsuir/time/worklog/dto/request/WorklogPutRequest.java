package com.bsuir.time.worklog.dto.request;


import java.math.BigDecimal;

public class WorklogPutRequest {
    private Long uid;
    private String comment;
    private BigDecimal hours;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }
}
