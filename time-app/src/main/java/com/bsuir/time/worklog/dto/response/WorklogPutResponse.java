package com.bsuir.time.worklog.dto.response;

public class WorklogPutResponse {
    private String stub;

    public String getStub() {
        return stub;
    }

    public void setStub(String stub) {
        this.stub = stub;
    }
}
