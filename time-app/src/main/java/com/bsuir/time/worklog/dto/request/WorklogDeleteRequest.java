package com.bsuir.time.worklog.dto.request;

public class WorklogDeleteRequest {
    private Long uid;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
