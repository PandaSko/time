package com.bsuir.time.worklog.service;

import com.bsuir.time.common.ConflictException;
import com.bsuir.time.dao.AssignmentDao;
import com.bsuir.time.domain.Assignment;
import com.bsuir.time.domain.Lock;
import com.bsuir.time.lock.LockService;
import com.bsuir.time.worklog.dto.request.WorklogDeleteRequest;
import com.bsuir.time.worklog.dto.request.WorklogPostRequest;
import com.bsuir.time.worklog.dto.request.WorklogPutRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class WorklogValidator {

    @Autowired
    private AssignmentDao assignmentDao;
    @Autowired
    private LockService lockService;

    public void validatePost(WorklogPostRequest request) {
        Long assignmentUid = request.getAssignmentUid();
        Long employeeUid = request.getEmployeeUid();
        Assert.notNull(employeeUid, "employeeUid should not be null");
        Assert.isTrue(employeeUid != 0, "Global admin can't report worklog");
        Assert.notNull(assignmentUid, "assignmentId should not be null");
        Assert.notNull(request.getDate(), "date should not be null");
        Assert.notNull(request.getComment(), "comment should not be null");
        Assert.notNull(request.getHours(), "duration should not be null");
        Assert.isTrue(request.getHours().doubleValue() > 0, "Duration should be greater than 0");
        Assert.isTrue(request.getHours().doubleValue() <= 24, "Duration should not be greater than 24");

        if (request.getSourceId() != null) {
            Assert.isTrue(request.getSourceSystemId() != null, "You should specify source system id if specify source id or source code for mapped artifact");
        }


        Assignment assignment = assignmentDao.findById(assignmentUid).orElseThrow(() -> ConflictException.throwNoAssignment(assignmentUid));
        if (assignment.getUid() != 0) { // not npa assignment
            Lock lock = lockService.findLock(assignment.getProjectUid()).orElse(null);
            if (lock != null) {
                if (request.getDate().isBefore(lock.getLockDate())) {
                    throw ConflictException.throwThereIsManagerLock(employeeUid, assignment.getProjectUid(), request.getDate());
                }
            }
        }
    }

    public void validatePut(WorklogPutRequest request) {
        Assert.notNull(request.getComment(), "comment should not be null");
        Assert.notNull(request.getHours(), "duration should not be null");
        Assert.notNull(request.getUid(), "uid should not be null");
        Assert.isTrue(request.getHours().doubleValue() > 0, "Duration should be greater than 0");
        Assert.isTrue(request.getHours().doubleValue() <= 24, "Duration should not be greater than 24");

    }

    public void validateDelete(WorklogDeleteRequest request) {
        Assert.notNull(request.getUid(), "uid should not be null");
    }
}
