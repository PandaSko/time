package com.bsuir.time.worklog.dto.response;

public class WorklogDeleteResponse {
    public enum Status {DELETED}

    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
