package com.bsuir.time.worklog.dto.response;

import java.util.ArrayList;
import java.util.List;

public class WorklogBatchResponse {
    private List<WorklogDeleteResponse> toDelete = new ArrayList<>();
    private List<WorklogPutResponse> toChange = new ArrayList<>();
    private List<WorklogPostResponse> toCreate = new ArrayList<>();

    public List<WorklogDeleteResponse> getToDelete() {
        return toDelete;
    }

    public void setToDelete(List<WorklogDeleteResponse> toDelete) {
        this.toDelete = toDelete;
    }

    public List<WorklogPutResponse> getToChange() {
        return toChange;
    }

    public void setToChange(List<WorklogPutResponse> toChange) {
        this.toChange = toChange;
    }

    public List<WorklogPostResponse> getToCreate() {
        return toCreate;
    }

    public void setToCreate(List<WorklogPostResponse> toCreate) {
        this.toCreate = toCreate;
    }
}
