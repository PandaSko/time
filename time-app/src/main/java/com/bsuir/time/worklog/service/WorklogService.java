package com.bsuir.time.worklog.service;

import com.bsuir.time.dao.AssignmentDao;
import com.bsuir.time.dao.WorklogDao;
import com.bsuir.time.domain.Assignment;
import com.bsuir.time.domain.Worklog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class WorklogService {

    @Autowired
    private WorklogDao worklogDao;
    @Autowired
    private AssignmentDao assignmentDao;

    public long createWorklog(Long employeeUid, Long assignmentUid, String comment, LocalDate date,
                              double hours, String sourceId, String sourceSystemId) {

        Assignment assignment = assignmentDao.findById(assignmentUid).get();

        Worklog worklog = new Worklog();
        worklog.setEmployeeUid(employeeUid);
        worklog.setAssignmentUid(assignmentUid);
        worklog.setActivityName(comment);
        worklog.setDuration(hours);
        worklog.setPositionUid(assignment.getPositionUid());
        worklog.setProjectUidProjection(assignment.getProjectUid());
        worklog.setSourceId(sourceId);
        worklog.setSourceSystemId(sourceSystemId);
        worklog.setTimePackageUid(assignment.getTimePackageUid());
        worklog.setTimesheetDate(date);

        Worklog saved = worklogDao.save(worklog);
        return saved.getUid();
    }

    public void updateWorklog(Long uid, String comment, double duration) {
        worklogDao.updateByUid(comment, duration, uid);
    }

    public void deleteWorklog(Long uid) {
        worklogDao.deleteById(uid);
    }

    public Worklog get(Long uid) {
        return worklogDao.findById(uid).orElse(null);
    }

}
