package com.bsuir.time.worklog.rest;

import com.bsuir.time.common.CalculationUtil;
import com.bsuir.time.common.ConflictException;
import com.bsuir.time.dao.WorklogDao;
import com.bsuir.time.domain.Worklog;
import com.bsuir.time.worklog.dto.request.WorklogBatchRequest;
import com.bsuir.time.worklog.dto.request.WorklogDeleteRequest;
import com.bsuir.time.worklog.dto.request.WorklogPostRequest;
import com.bsuir.time.worklog.dto.request.WorklogPutRequest;
import com.bsuir.time.worklog.dto.response.WorklogBatchResponse;
import com.bsuir.time.worklog.dto.response.WorklogDeleteResponse;
import com.bsuir.time.worklog.dto.response.WorklogListResponse;
import com.bsuir.time.worklog.dto.response.WorklogPostResponse;
import com.bsuir.time.worklog.dto.response.WorklogPutResponse;
import com.bsuir.time.worklog.service.WorklogService;
import com.bsuir.time.worklog.service.WorklogValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Comparator;

@RestController
@RequestMapping(WorklogController.API_ADDRESS)
@Api(tags = "worklog", description = "worklog api")
public class WorklogController {

    public static final String API_ADDRESS = "/api/worklog";

    @Autowired
    private WorklogDao worklogDao;
    @Autowired
    private WorklogValidator worklogValidator;
    @Autowired
    private WorklogService worklogService;

    @GetMapping
    @ApiOperation(value = "Get worklogs")
    public WorklogListResponse getWorklogs() {
        WorklogListResponse response = new WorklogListResponse();
        response.setWorklogs(worklogDao.findAll());
        return response;
    }

    @PostMapping
    @ApiOperation(value = "Batch worklog api")
    public WorklogBatchResponse batch(@RequestBody @Validated WorklogBatchRequest batchRequest) {
        WorklogBatchResponse response = new WorklogBatchResponse();

        if (batchRequest.getToDelete() != null) {
            batchRequest.getToDelete().sort(Comparator.comparing(WorklogDeleteRequest::getUid));//to reduce number of dead locks
            for (WorklogDeleteRequest i : batchRequest.getToDelete()) {
                response.getToDelete().add(delete(i));
            }
        }

        if (batchRequest.getToChange() != null) {
            batchRequest.getToChange().sort(
                    Comparator.comparing(WorklogPutRequest::getUid));
            for (WorklogPutRequest i : batchRequest.getToChange()) {
                response.getToChange().add(put(i));
            }
        }

        if (batchRequest.getToCreate() != null) {
            for (WorklogPostRequest i : batchRequest.getToCreate()) {
                response.getToCreate().add(post(i));
            }
        }

        return response;
    }

    public WorklogPostResponse post(WorklogPostRequest request) {
        Long assignmentUid = request.getAssignmentUid();

        worklogValidator.validatePost(request);

        BigDecimal roundedHours = CalculationUtil.round(request.getHours());
        long uid = worklogService.createWorklog(
                request.getEmployeeUid(), assignmentUid, request.getComment(), request.getDate(),
                roundedHours.doubleValue(), request.getSourceId(), request.getSourceSystemId()
        );

        WorklogPostResponse response = new WorklogPostResponse();
        response.setUid(uid);
        return response;
    }

    private WorklogPutResponse put(WorklogPutRequest request) {
        worklogValidator.validatePut(request);

        Worklog worklogExistingVersion = worklogService.get(request.getUid());
        if (worklogExistingVersion == null) {
            throw ConflictException.throwNoWorklog(request.getUid());
        }

        WorklogPutResponse result = new WorklogPutResponse();
        BigDecimal roundedHours = CalculationUtil.round(request.getHours());
        worklogService.updateWorklog(request.getUid(), request.getComment(), roundedHours.doubleValue());

        return result;
    }

    public WorklogDeleteResponse delete(WorklogDeleteRequest request) {
        WorklogDeleteResponse response = new WorklogDeleteResponse();

        worklogValidator.validateDelete(request);

        Worklog worklogExistingVersion = worklogService.get(request.getUid());
        if (worklogExistingVersion == null) {
            response.setStatus(WorklogDeleteResponse.Status.DELETED);
            return response;
        }

        worklogService.deleteWorklog(request.getUid());
        response.setStatus(WorklogDeleteResponse.Status.DELETED);

        return response;
    }
}
