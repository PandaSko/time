package com.bsuir.time.worklog.dto.request;

import java.util.ArrayList;
import java.util.List;

public class WorklogBatchRequest {
    private List<WorklogDeleteRequest> toDelete = new ArrayList<>();
    private List<WorklogPutRequest> toChange = new ArrayList<>();
    private List<WorklogPostRequest> toCreate = new ArrayList<>();

    public List<WorklogDeleteRequest> getToDelete() {
        return toDelete;
    }

    public void setToDelete(List<WorklogDeleteRequest> toDelete) {
        this.toDelete = toDelete;
    }

    public List<WorklogPutRequest> getToChange() {
        return toChange;
    }

    public void setToChange(List<WorklogPutRequest> toChange) {
        this.toChange = toChange;
    }

    public List<WorklogPostRequest> getToCreate() {
        return toCreate;
    }

    public void setToCreate(List<WorklogPostRequest> toCreate) {
        this.toCreate = toCreate;
    }
}
