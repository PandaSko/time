package com.bsuir.time.worklog.dto.request;


import java.math.BigDecimal;
import java.time.LocalDate;

public class WorklogPostRequest {
    private Long employeeUid;
    private Long assignmentUid;
    private String comment;
    private LocalDate date;
    private BigDecimal hours;
    private String sourceSystemId;
    private String sourceId;

    public String getSourceSystemId() {
        return sourceSystemId;
    }

    public void setSourceSystemId(String sourceSystemId) {
        this.sourceSystemId = sourceSystemId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public Long getEmployeeUid() {
        return employeeUid;
    }

    public void setEmployeeUid(Long employeeUid) {
        this.employeeUid = employeeUid;
    }

    public Long getAssignmentUid() {
        return assignmentUid;
    }

    public void setAssignmentUid(Long assignmentUid) {
        this.assignmentUid = assignmentUid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }
}
