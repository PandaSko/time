package com.bsuir.time.worklog.dto.response;

import com.bsuir.time.domain.Worklog;

import java.util.ArrayList;
import java.util.List;

public class WorklogListResponse {

    private List<Worklog> worklogs = new ArrayList<>();

    public List<Worklog> getWorklogs() {
        return worklogs;
    }

    public void setWorklogs(List<Worklog> projects) {
        this.worklogs = projects;
    }
}
