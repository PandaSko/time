package com.bsuir.time;

import com.bsuir.time.common.JdbcUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jdbc.repository.config.EnableJdbcAuditing;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.util.List;

@SpringBootApplication
@EnableJdbcRepositories
@EnableJdbcAuditing
public class TimeConfiguration {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "datasource.time")
    public DataSource timeDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @Primary
    public JdbcTemplate timeJdbcTemplate(@Qualifier("timeDataSource") DataSource dataSource) {
        JdbcTemplate result = new JdbcTemplate(dataSource);
        result.setFetchSize(JdbcUtil.getFetchSizeForTime());
        return result;
    }

    @Bean
    @Primary
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Qualifier("timeDataSource") DataSource dataSource) {
        NamedParameterJdbcTemplate result = new NamedParameterJdbcTemplate(dataSource);
        result.setCacheLimit(JdbcUtil.getFetchSizeForTime());
        return result;
    }


    @Bean
    @Profile("!test")
    @Primary
    public DataSourceTransactionManager timeTransactionManager(@Qualifier("timeDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    /*
     * We enable CORS for all our REST API as:
     *  - requests come through other server which consume our REST API
     *  - user's browser is behind proxies (Chrome is sensitive to things like that)
     *  - we implement permissions model and all endpoints should be managed by it
     *
     */
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedMethods(
                        RequestMethod.GET.toString(),
                        RequestMethod.HEAD.toString(),
                        RequestMethod.POST.toString(),
                        RequestMethod.PUT.toString(),
                        RequestMethod.PATCH.toString(),
                        RequestMethod.OPTIONS.toString(),
                        RequestMethod.TRACE.toString(),
                        RequestMethod.DELETE.toString()
                );
            }

            @Override
            public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
            }
        };
    }

    /*
     * By design our REST API is behind /api path
     * However springfox-swagger and Spring Boot expose predefined urls which can not be overridden
     * (swagger|webjars|v2|configuration - is reserved by springfox-swagger)
     * Additionally UI stores resources in its specific folders
     * All the rest is processed by single page UI application: index.html
     */
    @Controller
    @RequestMapping("/{welcome-page-alias:^(?!(?:api|tms|something-went-wrong|swagger|webjars|v2|configuration|index\\.html|asset-manifest\\.json|static|favicon\\.ico|bundle\\.js|style\\.css|fonts|icons|images)).*$}/**")
    public static class IndexHtmlUrlRewriteController {
        @GetMapping
        public String get(HttpServletResponse response) {
            return "forward:/index.html";
        }
    }

    @Controller
    @RequestMapping("/{error-page-alias:something-went-wrong$}")
    public static class ErrorHtmlUrlRewriteController {
        @GetMapping
        public String get(HttpServletResponse response) {
            return "forward:/something-went-wrong.html";
        }
    }
//
//    @Bean
//    public static ErrorPageRegistrar securityErrorPageRegistrar() {
//        return registry -> registry.addErrorPages(new ErrorPage(RequestRejectedException.class, "/something-went-wrong.html"));
//    }
}
