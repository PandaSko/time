package com.bsuir.time.project.rest;

import com.bsuir.time.dao.EmployeeDao;
import com.bsuir.time.dao.ProjectDao;
import com.bsuir.time.domain.Project;
import com.bsuir.time.project.dto.ProjectListResponse;
import com.bsuir.time.project.dto.ProjectRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ProjectController.API_ADDRESS)
@Api(tags = "project", description = "project api")
public class ProjectController {

    public static final String API_ADDRESS = "/api/project";

    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private EmployeeDao employeeDao;

    @GetMapping
    @ApiOperation(value = "Get projects")
    public ProjectListResponse getProjects() {
        ProjectListResponse response = new ProjectListResponse();
        response.setProjects(projectDao.findAll());
        return response;
    }

    @PutMapping
    @ApiOperation(value = "Change project")
    public void changeProject(@RequestParam Long uid,
                              @RequestBody @Validated ProjectRequest request) {
        validateUid(uid);
        validateProjectRequest(request);
        projectDao.save(convertToProject(uid, request));
    }

    @PostMapping
    @ApiOperation(value = "Create project")
    public void createProject(@RequestBody @Validated ProjectRequest request) {
        validateProjectRequest(request);
        projectDao.save(convertToProject(request));
    }

    @DeleteMapping
    @ApiOperation(value = "Delete project")
    public void deleteProject(@RequestParam Long uid) {
        validateUid(uid);
        projectDao.deleteById(uid);
    }

    private void validateUid(Long uid) {
        if (uid == 0) {
            throw new RuntimeException("Actions with npa project is restricted");
        }
    }

    private void validateProjectRequest(ProjectRequest projectRequest) {
        if (projectRequest.getManagerUid() == 0) {
            throw new RuntimeException("Restrict to use global admin");
        }

        employeeDao.findById(projectRequest.getManagerUid())
                .orElseThrow(() -> new RuntimeException("Manager not found"));

        if (projectRequest.getEndDate() != null && projectRequest.getEndDate().isBefore(projectRequest.getStartDate())) {
            throw new RuntimeException("End date can't be before start date");
        }
    }

    private Project convertToProject(Long uid, ProjectRequest request) {
        Project project = convertToProject(request);
        project.setUid(uid);
        return project;
    }

    private Project convertToProject(ProjectRequest request) {
        Project project = new Project();
        project.setEndDate(request.getEndDate());
        project.setManagerUid(request.getManagerUid());
        project.setName(request.getName());
        project.setStartDate(request.getStartDate());
        return project;
    }
}
