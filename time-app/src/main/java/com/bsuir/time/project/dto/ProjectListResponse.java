package com.bsuir.time.project.dto;

import com.bsuir.time.domain.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectListResponse {

    private List<Project> projects = new ArrayList<>();

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
