package com.bsuir.time.domain;

import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;

@Table("manager_lock")
public class Lock {
    private Long projectUid;
    private LocalDate lockDate;

    public Long getProjectUid() {
        return projectUid;
    }

    public void setProjectUid(Long projectUid) {
        this.projectUid = projectUid;
    }

    public LocalDate getLockDate() {
        return lockDate;
    }

    public void setLockDate(LocalDate lockDate) {
        this.lockDate = lockDate;
    }
}
