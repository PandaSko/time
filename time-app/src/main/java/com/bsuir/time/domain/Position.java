package com.bsuir.time.domain;

import com.bsuir.time.domain.enums.PositionStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Position {
    @Id
    private Long uid;
    private Long employeeUid;
    private String primaryRoleName;
    private LocalDate startDate;
    private LocalDate endDate;
    private PositionStatus status;
    private Long projectUid;

    @LastModifiedDate
    private LocalDateTime changed;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getEmployeeUid() {
        return employeeUid;
    }

    public void setEmployeeUid(Long employeeUid) {
        this.employeeUid = employeeUid;
    }

    public String getPrimaryRoleName() {
        return primaryRoleName;
    }

    public void setPrimaryRoleName(String primaryRoleName) {
        this.primaryRoleName = primaryRoleName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public PositionStatus getStatus() {
        return status;
    }

    public void setStatus(PositionStatus status) {
        this.status = status;
    }

    public Long getProjectUid() {
        return projectUid;
    }

    public void setProjectUid(Long projectUid) {
        this.projectUid = projectUid;
    }
}
