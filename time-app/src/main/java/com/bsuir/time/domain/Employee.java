package com.bsuir.time.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Employee {
    @Id
    private Long uid;
    private String fullName;
    private String title;
    private Long managerUid;
    private String email;
    private LocalDate fired;
    private LocalDate hired;
    private Boolean timeReportingRequired;
    @LastModifiedDate
    private LocalDateTime changed;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getManagerUid() {
        return managerUid;
    }

    public void setManagerUid(Long managerUid) {
        this.managerUid = managerUid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getFired() {
        return fired;
    }

    public void setFired(LocalDate fired) {
        this.fired = fired;
    }

    public LocalDate getHired() {
        return hired;
    }

    public void setHired(LocalDate hired) {
        this.hired = hired;
    }

    public Boolean getTimeReportingRequired() {
        return timeReportingRequired;
    }

    public void setTimeReportingRequired(Boolean timeReportingRequired) {
        this.timeReportingRequired = timeReportingRequired;
    }
}
