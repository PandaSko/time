package com.bsuir.time.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Worklog {
    @Id
    private Long uid;
    private LocalDate timesheetDate;
    private Double duration;
    private String activityName;
    private Long employeeUid;
    private Long assignmentUid;
    private Long timePackageUid;
    private Long positionUid;
    private Long projectUidProjection;
    private String sourceSystemId;
    private String sourceId;
    @LastModifiedDate
    private LocalDateTime changed;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public LocalDate getTimesheetDate() {
        return timesheetDate;
    }

    public void setTimesheetDate(LocalDate timesheetDate) {
        this.timesheetDate = timesheetDate;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Long getEmployeeUid() {
        return employeeUid;
    }

    public void setEmployeeUid(Long employeeUid) {
        this.employeeUid = employeeUid;
    }

    public Long getAssignmentUid() {
        return assignmentUid;
    }

    public void setAssignmentUid(Long assignmentUid) {
        this.assignmentUid = assignmentUid;
    }

    public Long getTimePackageUid() {
        return timePackageUid;
    }

    public void setTimePackageUid(Long timePackageUid) {
        this.timePackageUid = timePackageUid;
    }

    public Long getPositionUid() {
        return positionUid;
    }

    public void setPositionUid(Long positionUid) {
        this.positionUid = positionUid;
    }

    public Long getProjectUidProjection() {
        return projectUidProjection;
    }

    public void setProjectUidProjection(Long projectUidProjection) {
        this.projectUidProjection = projectUidProjection;
    }

    public String getSourceSystemId() {
        return sourceSystemId;
    }

    public void setSourceSystemId(String sourceSystemId) {
        this.sourceSystemId = sourceSystemId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }
}
