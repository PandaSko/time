package com.bsuir.time.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Assignment {
    @Id
    private Long uid;
    private String name;
    private Long projectUid;
    private Long employeeUid;
    private Long positionUid;
    private Long timePackageUid;
    private LocalDate startDate;
    private LocalDate endDate;
    @LastModifiedDate
    private LocalDateTime changed;

    public Long getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getProjectUid() {
        return projectUid;
    }

    public void setProjectUid(Long projectUid) {
        this.projectUid = projectUid;
    }

    public Long getEmployeeUid() {
        return employeeUid;
    }

    public void setEmployeeUid(Long employeeUid) {
        this.employeeUid = employeeUid;
    }

    public Long getPositionUid() {
        return positionUid;
    }

    public void setPositionUid(Long positionUid) {
        this.positionUid = positionUid;
    }

    public Long getTimePackageUid() {
        return timePackageUid;
    }

    public void setTimePackageUid(Long timePackageUid) {
        this.timePackageUid = timePackageUid;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
