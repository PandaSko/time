package com.bsuir.time.assignment.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class AssignmentRequest {
    @NotBlank
    private String name;
    @NotNull
    private Long projectUid;
    @NotNull
    private Long employeeUid;
    @NotNull
    private Long positionUid;
    @NotNull
    private Long timePackageUid;
    @NotNull
    private LocalDate startDate;
    private LocalDate endDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getProjectUid() {
        return projectUid;
    }

    public void setProjectUid(Long projectUid) {
        this.projectUid = projectUid;
    }

    public Long getEmployeeUid() {
        return employeeUid;
    }

    public void setEmployeeUid(Long employeeUid) {
        this.employeeUid = employeeUid;
    }

    public Long getPositionUid() {
        return positionUid;
    }

    public void setPositionUid(Long positionUid) {
        this.positionUid = positionUid;
    }

    public Long getTimePackageUid() {
        return timePackageUid;
    }

    public void setTimePackageUid(Long timePackageUid) {
        this.timePackageUid = timePackageUid;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
