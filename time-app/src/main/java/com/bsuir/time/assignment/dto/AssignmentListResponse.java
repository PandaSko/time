package com.bsuir.time.assignment.dto;

import com.bsuir.time.domain.Assignment;

import java.util.ArrayList;
import java.util.List;

public class AssignmentListResponse {

    private List<Assignment> assignments = new ArrayList<>();

    public List<Assignment> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<Assignment> projects) {
        this.assignments = projects;
    }
}
