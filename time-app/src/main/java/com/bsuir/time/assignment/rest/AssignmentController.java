package com.bsuir.time.assignment.rest;

import com.bsuir.time.assignment.dto.AssignmentListResponse;
import com.bsuir.time.assignment.dto.AssignmentRequest;
import com.bsuir.time.dao.AssignmentDao;
import com.bsuir.time.dao.PackageDao;
import com.bsuir.time.dao.PositionDao;
import com.bsuir.time.domain.Assignment;
import com.bsuir.time.domain.Package;
import com.bsuir.time.domain.Position;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AssignmentController.API_ADDRESS)
@Api(tags = "assignment", description = "assignment api")
public class AssignmentController {

    public static final String API_ADDRESS = "/api/assignment";

    @Autowired
    private AssignmentDao assignmentDao;
    @Autowired
    private PositionDao positionDao;
    @Autowired
    private PackageDao packageDao;

    @GetMapping
    @ApiOperation(value = "Get assignments")
    public AssignmentListResponse getAssignments() {
        AssignmentListResponse response = new AssignmentListResponse();
        response.setAssignments(assignmentDao.findAll());
        return response;
    }

    @PutMapping
    @ApiOperation(value = "Change assignment")
    public void changeAssignment(@RequestParam Long uid,
                                 @RequestBody @Validated AssignmentRequest request) {
        validateUid(uid);
        validateAssignmentRequest(request);
        assignmentDao.save(convertToAssignment(uid, request));
    }

    @PostMapping
    @ApiOperation(value = "Create assignment")
    public void createAssignment(@RequestBody @Validated AssignmentRequest request) {
        validateAssignmentRequest(request);
        assignmentDao.save(convertToAssignment(request));
    }

    @DeleteMapping
    @ApiOperation(value = "Delete assignment")
    public void deleteAssignment(@RequestParam Long uid) {
        validateUid(uid);
        assignmentDao.deleteById(uid);
    }

    private void validateUid(Long uid) {
        if (uid == 0) {
            throw new RuntimeException("Actions with npa package is restricted");
        }
    }

    private void validateAssignmentRequest(AssignmentRequest assignmentRequest) {
        if (assignmentRequest.getEmployeeUid() == 0) {
            throw new RuntimeException("Restrict to use global admin");
        }
        if (assignmentRequest.getProjectUid() == 0) {
            throw new RuntimeException("Restrict to use npa project");
        }

        Package timePackage = packageDao.findById(assignmentRequest.getTimePackageUid())
                .orElseThrow(() -> new RuntimeException("Time package not found"));
        Position position = positionDao.findById(assignmentRequest.getPositionUid())
                .orElseThrow(() -> new RuntimeException("Position not found"));

        if (!timePackage.getProjectUid().equals(position.getProjectUid())
                || !timePackage.getProjectUid().equals(assignmentRequest.getProjectUid())) {
            throw new RuntimeException("Project uid is not same");
        }

        if(!position.getEmployeeUid().equals(assignmentRequest.getEmployeeUid())){
            throw new RuntimeException("Employee uid is not same");
        }

        if(assignmentRequest.getStartDate().isBefore(position.getStartDate())
                || assignmentRequest.getStartDate().isBefore(timePackage.getStartDate())){
            throw new RuntimeException("Assignment start date should be after position and time package start dates");
        }
        if(assignmentRequest.getEndDate()!=null
                && assignmentRequest.getEndDate().isBefore(assignmentRequest.getStartDate())){
            throw new RuntimeException("End date can't be before start date");
        }
    }

    private Assignment convertToAssignment(Long uid, AssignmentRequest request) {
        Assignment assignment = convertToAssignment(request);
        assignment.setUid(uid);
        return assignment;
    }

    private Assignment convertToAssignment(AssignmentRequest request) {
        Assignment assignment = new Assignment();
        assignment.setEndDate(request.getEndDate());
        assignment.setEmployeeUid(request.getEmployeeUid());
        assignment.setPositionUid(request.getPositionUid());
        assignment.setName(request.getName());
        assignment.setStartDate(request.getStartDate());
        assignment.setProjectUid(request.getProjectUid());
        assignment.setTimePackageUid(request.getTimePackageUid());
        return assignment;
    }

}
