package com.bsuir.time.time_journal.dto;

import java.util.List;

public class WorkloadTimeJournalResponse {
    private List<TimeJournalWorkload> workloads;

    public List<TimeJournalWorkload> getWorkloads() {
        return workloads;
    }

    public void setWorkloads(List<TimeJournalWorkload> workloads) {
        this.workloads = workloads;
    }
}
