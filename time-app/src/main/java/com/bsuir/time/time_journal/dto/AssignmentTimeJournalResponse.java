package com.bsuir.time.time_journal.dto;

import java.time.LocalDate;
import java.util.Map;

public class AssignmentTimeJournalResponse {
    private Long assignmentUid;
    private String name;
    private Map<String, Map<LocalDate, TimeJournalWorklog>> taskToWorklogs;
    private Map<LocalDate, LockTimeJournalResponse> locks;

    public Long getAssignmentUid() {
        return assignmentUid;
    }

    public void setAssignmentUid(Long assignmentUid) {
        this.assignmentUid = assignmentUid;
    }

    public Map<LocalDate, LockTimeJournalResponse> getLocks() {
        return locks;
    }

    public void setLocks(Map<LocalDate, LockTimeJournalResponse> locks) {
        this.locks = locks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Map<LocalDate, TimeJournalWorklog>> getTaskToWorklogs() {
        return taskToWorklogs;
    }

    public void setTaskToWorklogs(Map<String, Map<LocalDate, TimeJournalWorklog>> taskToWorklogs) {
        this.taskToWorklogs = taskToWorklogs;
    }
}
