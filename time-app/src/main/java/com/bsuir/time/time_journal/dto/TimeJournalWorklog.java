package com.bsuir.time.time_journal.dto;

import java.time.LocalDate;

public class TimeJournalWorklog {
    private Long uid;
    private Double duration;
    private LocalDate date;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
