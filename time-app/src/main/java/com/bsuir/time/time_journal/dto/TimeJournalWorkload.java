package com.bsuir.time.time_journal.dto;

import java.time.LocalDate;

public class TimeJournalWorkload {
    private Double duration;
    private LocalDate date;

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
