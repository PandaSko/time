package com.bsuir.time.time_journal.dto;

import java.time.LocalDate;
import java.util.List;

public class ProjectTimeJournalResponse {

    private LocalDate startDate;
    private LocalDate endDate;
    private LocalDate lockDate;
    private String projectName;
    private Long projectUid;
    private List<AssignmentTimeJournalResponse> assignments;

    public LocalDate getStartDate() {
        return startDate;
    }

    public Long getProjectUid() {
        return projectUid;
    }

    public void setProjectUid(Long projectUid) {
        this.projectUid = projectUid;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalDate getLockDate() {
        return lockDate;
    }

    public void setLockDate(LocalDate lockDate) {
        this.lockDate = lockDate;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public List<AssignmentTimeJournalResponse> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<AssignmentTimeJournalResponse> assignments) {
        this.assignments = assignments;
    }
}
