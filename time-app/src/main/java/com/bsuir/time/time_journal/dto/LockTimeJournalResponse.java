package com.bsuir.time.time_journal.dto;

import java.time.LocalDate;

public class LockTimeJournalResponse {
    private LocalDate date;
    private Boolean locked;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }
}
