package com.bsuir.time.time_journal;

import com.bsuir.time.common.DateUtil;
import com.bsuir.time.dao.AssignmentDao;
import com.bsuir.time.dao.EmployeeDao;
import com.bsuir.time.dao.LockDao;
import com.bsuir.time.dao.PackageDao;
import com.bsuir.time.dao.PositionDao;
import com.bsuir.time.dao.ProjectDao;
import com.bsuir.time.dao.WorkloadDao;
import com.bsuir.time.dao.WorklogDao;
import com.bsuir.time.domain.Assignment;
import com.bsuir.time.domain.Employee;
import com.bsuir.time.domain.Lock;
import com.bsuir.time.domain.Package;
import com.bsuir.time.domain.Position;
import com.bsuir.time.domain.Project;
import com.bsuir.time.domain.Workload;
import com.bsuir.time.domain.Worklog;
import com.bsuir.time.time_journal.dto.AssignmentTimeJournalResponse;
import com.bsuir.time.time_journal.dto.LockTimeJournalResponse;
import com.bsuir.time.time_journal.dto.ProjectTimeJournalResponse;
import com.bsuir.time.time_journal.dto.TimeJournalResponse;
import com.bsuir.time.time_journal.dto.TimeJournalWorkload;
import com.bsuir.time.time_journal.dto.TimeJournalWorklog;
import com.bsuir.time.time_journal.dto.WorkloadTimeJournalResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.bsuir.time.common.JdbcUtil.fetchByIds;

@RestController
@RequestMapping(TimeJournalController.API_ADDRESS)
@Api(tags = "time-journal", description = "time journal api")
public class TimeJournalController {

    public static final String API_ADDRESS = "/api/time-journal";
    public static final LocalDate MIN_DATE = LocalDate.parse("1970-01-01");

    @Autowired
    private WorkloadDao workloadDao;
    @Autowired
    private PositionDao positionDao;
    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private AssignmentDao assignmentDao;
    @Autowired
    private WorklogDao worklogDao;
    @Autowired
    private LockDao lockDao;
    @Autowired
    private PackageDao packageDao;
    @Autowired
    private EmployeeDao employeeDao;

    @GetMapping("/")
    public TimeJournalResponse get(@RequestParam Long employeeUid,
                                   @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam("yyyy-MM-dd") LocalDate from,
                                   @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam("yyyy-MM-dd") LocalDate to) {
        Optional<Employee> employeeRecord = employeeDao.findById(employeeUid);
        if(!employeeRecord.isPresent()){
            throw new RuntimeException("Employee not found");
        }
        Employee employee = employeeRecord.get();

        List<Workload> workloads = workloadDao.findAllByEmployeeUid(employeeUid);
        List<Position> positions = positionDao.findAllByEmployeeUid(employeeUid, from, to);
        List<Long> projectUids = positions.stream().map(Position::getProjectUid).collect(Collectors.toList());
        List<Project> projects = fetchByIds(projectDao::findAllById, projectUids);
        List<Assignment> assgnmentCache = assignmentDao.findAllByEmployeeUid(employeeUid, from, to);
        Map<Long, Position> uidToPosition = positions.stream()
                .collect(Collectors.toMap(Position::getUid, Function.identity()));
        Map<Long, Lock> locksPerProject = fetchByIds(lockDao::findByProjectUids, projectUids).stream()
                .collect(Collectors.toMap(Lock::getProjectUid, Function.identity()));
        Set<Long> packageUids = assgnmentCache.stream().map(Assignment::getTimePackageUid).collect(Collectors.toSet());
        Map<Long, Package> uidToPackage = fetchByIds(packageDao::findAllById, packageUids).stream()
                .collect(Collectors.toMap(Package::getUid, Function.identity()));
        Map<Long, List<Assignment>> assignmentsPerProject = assgnmentCache.stream()
                .collect(Collectors.groupingBy(Assignment::getProjectUid));
        Map<Long, List<Worklog>> worklogsPerAssignment = worklogDao.findAllByEmployeeUid(employeeUid, from, to).stream()
                .collect(Collectors.groupingBy(Worklog::getAssignmentUid));

        TimeJournalResponse timeJournalResponse = new TimeJournalResponse();
        timeJournalResponse.setFrom(from);
        timeJournalResponse.setTo(to);

        WorkloadTimeJournalResponse workloadTimeJournalResponse = calculateWorkloadPerDay(workloads, from, to);
        timeJournalResponse.setWorkload(workloadTimeJournalResponse);

        List<ProjectTimeJournalResponse> projectResponses = new ArrayList<>();
        for(Project project : projects){
            ProjectTimeJournalResponse projectTimeJournalResponse = new ProjectTimeJournalResponse();
            projectTimeJournalResponse.setProjectName(project.getName());
            projectTimeJournalResponse.setProjectUid(project.getUid());
            LocalDate lockDate = Optional.ofNullable(locksPerProject.get(project.getUid()))
                    .map(Lock::getLockDate)
                    .orElse(MIN_DATE);
            projectTimeJournalResponse.setLockDate(lockDate);
            projectTimeJournalResponse.setAssignments(new ArrayList<>());
            List<Assignment> assignments = assignmentsPerProject.getOrDefault(project.getUid(), new ArrayList<>());
            for(Assignment assignment : assignments){
                AssignmentTimeJournalResponse assignmentTimeJournalResponse = new AssignmentTimeJournalResponse();
                assignmentTimeJournalResponse.setName(assignment.getName());
                assignmentTimeJournalResponse.setAssignmentUid(assignment.getUid());
                assignmentTimeJournalResponse.setTaskToWorklogs(worklogsPerAssignment.getOrDefault(assignment.getUid(), new ArrayList<>()).stream()
                        .collect(Collectors.groupingBy(Worklog::getActivityName,
                                Collectors.toMap(Worklog::getTimesheetDate, this::convertToTjWorklog))));
                assignmentTimeJournalResponse.setLocks(calculateLocks(from, to, project, assignment, lockDate, uidToPackage.get(assignment.getTimePackageUid()),
                        uidToPosition.get(assignment.getPositionUid()), employee));
                projectTimeJournalResponse.getAssignments().add(assignmentTimeJournalResponse);
            }

            projectResponses.add(projectTimeJournalResponse);
        }

        timeJournalResponse.setProjects(projectResponses);
        return timeJournalResponse;
    }

    private Map<LocalDate, LockTimeJournalResponse> calculateLocks(LocalDate from, LocalDate to, Project project, Assignment assignment,
                                                                   LocalDate lockDate, Package timePackage, Position position, Employee employee) {
        Map<LocalDate, LockTimeJournalResponse> result = new HashMap<>();
        LocalDate firstUnlockedDay = DateUtil.maxOfNotNull(project.getStartDate(), assignment.getStartDate(), lockDate, timePackage.getStartDate(),
                position.getStartDate(), employee.getHired());
        LocalDate firstLockedDay = DateUtil.minOfNotNull(to.plusDays(1), project.getEndDate(), assignment.getEndDate(), timePackage.getEndDate(),
                position.getEndDate(), employee.getFired());
        if(firstUnlockedDay.isBefore(from) && firstLockedDay.isAfter(to)){
            return result;
        }
        for(LocalDate day : DateUtil.period(from, to)){
            if(!day.isAfter(firstUnlockedDay) || !day.isBefore(firstLockedDay)){
                LockTimeJournalResponse lock = new LockTimeJournalResponse();
                lock.setDate(day);
                lock.setLocked(true);
                result.put(day, lock);
            }
        }
        return result;
    }

    private TimeJournalWorklog convertToTjWorklog(Worklog worklog) {
        TimeJournalWorklog tjWorklog = new TimeJournalWorklog();
        tjWorklog.setDate(worklog.getTimesheetDate());
        tjWorklog.setDuration(worklog.getDuration());
        tjWorklog.setUid(worklog.getUid());
        return tjWorklog;
    }

    private WorkloadTimeJournalResponse calculateWorkloadPerDay(List<Workload> workloads, LocalDate from, LocalDate to) {
        WorkloadTimeJournalResponse result = new WorkloadTimeJournalResponse();
        List<TimeJournalWorkload> workloadList = new ArrayList<>();
        result.setWorkloads(workloadList);
        Map<LocalDate, BigDecimal> dateToDur = workloads.stream().collect(Collectors.toMap(Workload::getFromDate, Workload::getDuration));
        List<LocalDate> fromDates = new ArrayList<>(dateToDur.keySet());
        fromDates.sort(Comparator.naturalOrder());

        Set<LocalDate> period = DateUtil.period(from, to);
        period.forEach(day ->{
            TimeJournalWorkload workload = new TimeJournalWorkload();
            workload.setDate(day);
            workload.setDuration(calcDuration(day, fromDates, dateToDur));
            workloadList.add(workload);
        });
        workloadList.sort(Comparator.comparing(TimeJournalWorkload::getDate));
        return result;

    }

    private Double calcDuration(LocalDate day, List<LocalDate> fromDates, Map<LocalDate, BigDecimal> dateToDur) {
        if(day.getDayOfWeek() == DayOfWeek.SATURDAY || day.getDayOfWeek() == DayOfWeek.SUNDAY){
            return 0d;
        }

        if(CollectionUtils.isEmpty(fromDates)){
            return 0d;
        }

        LocalDate firstWorkload = fromDates.get(0);
        if(day.isBefore(firstWorkload)){
            return 0d;
        }
        for(LocalDate fromDate: fromDates){
            if (!day.isBefore(fromDate)) {
                firstWorkload = fromDate;
            }else{
                break;
            }
        }
        return dateToDur.get(firstWorkload).doubleValue();
    }
}
