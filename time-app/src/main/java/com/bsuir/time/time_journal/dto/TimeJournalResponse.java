package com.bsuir.time.time_journal.dto;

import java.time.LocalDate;
import java.util.List;

public class TimeJournalResponse {
    private LocalDate from;
    private LocalDate to;
    private WorkloadTimeJournalResponse workload;
    private List<ProjectTimeJournalResponse> projects;

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }

    public WorkloadTimeJournalResponse getWorkload() {
        return workload;
    }

    public void setWorkload(WorkloadTimeJournalResponse workload) {
        this.workload = workload;
    }

    public List<ProjectTimeJournalResponse> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectTimeJournalResponse> projects) {
        this.projects = projects;
    }
}
