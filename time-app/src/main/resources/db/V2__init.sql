create table worklog(
	uid bigserial primary key,
	timesheet_date date NOT NULL,
	duration numeric(6,4) NOT NULL,
	activity_name text,

	employee_uid bigint NOT null references employee(uid),
	assignment_uid bigint not null references assignment(uid),
	time_package_uid bigint references time_package(uid),
	position_uid bigint references "position"(uid),

	project_uid_projection bigint,

	changed timestamptz NOT null,

	source_system_id text,
	source_id text
);

create table manager_lock(
	project_uid bigint not null references project(uid),
	lock_date date not null
);

