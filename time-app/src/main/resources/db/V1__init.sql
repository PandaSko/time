create table employee(
	uid bigserial primary key,
	full_name text NOT NULL,
	title text not NULL,
	manager_uid bigint references employee(uid),
	email text not NULL,
	manager_uids_projection bigint[],
	fired date,
	time_reporting_required bool,
	hired date not null,
	changed timestamptz
);

insert into employee values(0, 'admin', 'admin', null, 'pandasut.vb@gmail.com', null, null, false, '1970-01-01', null);

create table project(
	uid bigserial primary key,
	name text not null,
	start_date date not null,
	end_date date,
	manager_uid bigint not null references employee(uid),
	changed timestamptz
);

insert into project values (0, 'npa', '1970-01-01', null, 0, null);

create table time_package(
	uid bigserial primary key,
	name text not null,
	start_date date not null,
	end_date date,
	project_uid bigint not null references project(uid),
	changed timestamptz
);

create table "position"(
	uid bigserial primary key,
	employee_uid bigint not null references employee(uid),
	primary_role_name text,
	start_date date NOT NULL,
	end_date date,
	status text not NULL,
	changed timestamptz,
	project_uid bigint not null references project(uid)
);

create table workload(
	uid bigserial primary key,
	from_date date NOT NULL,
	duration numeric(6,4) NOT NULL,
	employee_uid bigint not null references employee(uid),
	changed timestamptz
);

create table "assignment"(
	uid bigserial primary key,

    name text not null,
	project_uid bigint NOT null references project(uid),
	employee_uid bigint references employee(uid),
	position_uid bigint references "position"(uid),
	time_package_uid bigint null references time_package(uid),

	start_date date not NULL,
	end_date date,

	changed timestamptz
);

insert into "assignment" values (0, 'npa', 0, null, null, null, '1970-01-01', null, null);

