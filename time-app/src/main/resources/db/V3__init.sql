CREATE TABLE "lock" (
	id text primary key,
	value bool NOT NULL,
	"change" text,
	changed timestamptz
);

create table tms_account(
	id bigserial primary key,
	uid text NOT null unique,
	"type" text NOT NULL,
	"name" text NOT NULL,
	prefer_resynchronization bool,
	changed timestamptz
);

create table tms_reporting_item(
	account_id bigint not null references tms_account(id),
	source_id text NOT NULL,
	"name" text,
	parent_source_id text,
	parent_ids_projection text[]
);
CREATE UNIQUE INDEX source_artifact_unique ON public.tms_reporting_item USING btree (account_id, source_id) ;

create table tms_reporting_item_mapping(
	account_id bigint NOT null references tms_account(id),
	reporting_item_id text NOT NULL,
	time_package_id bigint NOT null,
	start_date date,
	end_date date
);

CREATE TABLE tms_user (
	account_id bigint NOT null REFERENCES tms_account(id),
	source_id text NOT NULL,
	"name" text,
	CONSTRAINT source_user_pkey PRIMARY KEY (account_id, source_id)
);

CREATE TABLE tms_user_mapping (
	account_id bigint NOT NULL,
	source_id text NOT NULL,
	employee_uid bigint NOT null references employee(uid)
);
CREATE UNIQUE INDEX tms_user_mapping_account_sync_id ON public.tms_user_mapping USING btree (account_id, source_id) ;


create table tms_project_mapping(
	account_id bigint NOT null references tms_account(id),
	reporting_item_id text NOT NULL,
	project_uid bigint NOT NULL
);