create table tms_worklog (
	account_id bigint not null,
	source_id text not null,
	timesheet_date date not null,
	hours numeric(6,4),
	"comment" text,
	reporting_item_id text,
	changed timestamptz not null,
	deleted bool not null,
	sync_user_id text not null,
	constraint tms_worklog_pkey primary key (account_id, source_id)
);

create table tms_worklog_message (
	account_id int8 not null,
	source_id text not null,
	success bool not null,
	last_success_hash text,
	message text,
	changed timestamptz not null,
	deleted bool not null,
	project_uid_projection text ,
	day_projection date,
	reporting_item_id text,
	assignment_changed bool,
	constraint worklog_external_tms_message_pkey primary key (account_id, source_id)
);

create index tms_worklog_message_by_project on tms_worklog_message using btree (project_uid_projection, day_projection) ;