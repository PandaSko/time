package com.bsuir.time.core.service.unified;

import com.bsuir.time.core.dao.TmsUserMappingDao;
import com.bsuir.time.core.dao.WorklogExternalTmsMessageDao;
import com.bsuir.time.core.dto.TmsWorklog;
import com.bsuir.time.core.provider.other.TmsWorklogProvider;
import com.bsuir.time.core.provider.other.TmsWorklogProviderDashboard;
import com.bsuir.time.core.service.other.SearchLogicOfAssignment;
import com.bsuir.time.core.service.other.SearchLogicOfEscapeProject;
import com.bsuir.time.core.service.other.SearchLogicOfTimePackage;
import com.bsuir.time.core.service.other.TimeFacade;
import com.bsuir.time.core.service.other.TmsWorklogSyncService;
import com.bsuir.time.core.util.DateUtil;
import com.bsuir.time.core.util.JdbcUtil;
import com.bsuir.time.core.util.SyncServiceAbstract3;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.Hashing;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestClientResponseException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class UnifiedWorklogSyncService extends SyncServiceAbstract3<UnifiedWorklogSyncService.Worklog, Long> {
    private static final Logger logger = LoggerFactory.getLogger(UnifiedWorklogSyncService.class);

    private Long accountId;
    private String accountUid;
    private TimeFacade time;
    private boolean resynchronize;
    private JdbcTemplate timeJdbcTemplate;
    private TmsUserMappingDao tmsUserMappingDao;
    private WorklogExternalTmsMessageDao worklogExternalTmsMessageDao;
    private SearchLogicOfAssignment searchLogicOfAssignment;
    private Map<Long, Worklog> source = new HashMap<>();
    private Map<Long, Worklog> target = new HashMap<>();
    private List<String> unsuccessfullySyncedSourceIds = new ArrayList<>();

    private Map<String, String> worklogSyncIdToReportingItemIdMap = new HashMap<>();
//    private Map<Long, String> packageIdToName = new HashMap<>();//for log saving


    public UnifiedWorklogSyncService(Long accountId, String accountUid, boolean resynchronize, JdbcTemplate timeJdbcTemplate,
                                     TmsUserMappingDao tmsUserMappingDao, TimeFacade time) {
        this.accountId = accountId;
        this.accountUid = accountUid;
        this.resynchronize = resynchronize;
        this.timeJdbcTemplate = timeJdbcTemplate;
        this.tmsUserMappingDao = tmsUserMappingDao;
        this.searchLogicOfAssignment = new SearchLogicOfAssignment(timeJdbcTemplate);
        this.worklogExternalTmsMessageDao = new WorklogExternalTmsMessageDao(timeJdbcTemplate);
        this.time = time;
    }

    public static void process(TmsWorklogProvider provider, boolean resync, JdbcTemplate timeJdbcTemplate, TmsUserMappingDao tmsUserMappingDao, TimeFacade time) {
        Set<String> userSourceIds = provider.getUserSourceIds();
        Long accountId = provider.getAccountId();
        String accountUid = timeJdbcTemplate.queryForObject("select uid from tms_account where id = ?", String.class, accountId);
        if (CollectionUtils.isEmpty(userSourceIds)) {
            logger.info("No users found to sync for account {} in {} provider", accountId, provider.getClass().getName());
        }

        for (String userSourceId : userSourceIds) {
            UnifiedWorklogSyncService unifiedWorklogSyncService = new UnifiedWorklogSyncService(accountId, accountUid, resync, timeJdbcTemplate,
                    tmsUserMappingDao, time);
            try {
                Map<String, TmsWorklog> externalWorklogsMap = prepareWorklogsMap(provider.getWorklogs(userSourceId));

                //cache
                if (!provider.getClass().isAssignableFrom(TmsWorklogProviderDashboard.class)) {
                    logger.info("caching data for user source id {}", userSourceId);
                    new TmsWorklogSyncService(timeJdbcTemplate, userSourceId, accountId).sync(externalWorklogsMap, provider.getFrom(), provider.getTo());
                }

                unifiedWorklogSyncService.sync(accountId, userSourceId, provider.getFrom(), provider.getTo(), externalWorklogsMap);
            } catch (Throwable th) {
                logger.error("error for user source id {}", userSourceId, th);
            }

        }
    }

    public void sync(Long accountId, String userSourceId, LocalDate from, LocalDate to, Map<String, TmsWorklog> externalWorklogsMap) {
        init();

        Long userPmcId = tmsUserMappingDao.getMappedUserUid(accountId, userSourceId);
        if (userPmcId == null) {
            return;
        }

        Map<String, Long> worklogSyncIdToTimeIdMap = new HashMap<>();//for performance
        if (from != null && to != null) {
            timeJdbcTemplate.query("select * from worklog where source_system_id = ? and employee_uid = ? and " +
                    "timesheet_date between ? and ? and source_id is not null", rs -> {
                Worklog entity = readWorklog(rs);
                worklogSyncIdToTimeIdMap.put(entity.getSourceId(), entity.getId());
                target.put(entity.getId(), entity);
            }, accountUid, userPmcId, from, to);
        }

        logger.info("processing data for user {}", userPmcId);
        //prepare source
        List<ReportingItemMappingDto> syncTasksCache = getTasksForSync(timeJdbcTemplate, accountId);
        long fakeIdCounter = -1;
        Map<String, ShortExternalMessage> sourceIdToShortMessage = worklogExternalTmsMessageDao
                .fetchShortExternalMessage(externalWorklogsMap.keySet(), accountId).stream()
                .collect(Collectors.toMap(ShortExternalMessage::getSourceId, Function.identity()));
        List<String> sourceIdWithNoneActualAssignment = new ArrayList<>();
        List<String> sourceIdWithActualAssignment = new ArrayList<>();
        List<String> shouldBeDeletedSourceIds = new ArrayList<>();

        for (Map.Entry<String, TmsWorklog> entry : externalWorklogsMap.entrySet()) {
            TmsWorklog tmsWorklog = entry.getValue();

            //step1 process deletions by ids
            if (tmsWorklog == null) {
                shouldBeDeletedSourceIds.add(entry.getKey());
                continue;
            }

            worklogSyncIdToReportingItemIdMap.put(tmsWorklog.getSourceId(), tmsWorklog.getReportingItemId());

            Long escapeProject = SearchLogicOfEscapeProject.find(accountId, new ArrayList<>(), new ArrayList<>(), timeJdbcTemplate);
            if (escapeProject == null) {
                continue;//tms sync just ignores not associated reporting items. no one KPS will be able to see errors about them
            }

            Worklog worklog = new Worklog();
            //step3: convert. try to convert as many properties as possible
            if (tmsWorklog.getHours() != null) {
                double hoursRounded = new BigDecimal(Double.toString(tmsWorklog.getHours())).setScale(2, RoundingMode.HALF_UP)
                        .doubleValue();//to avoid false positive updates
                worklog.setHours(hoursRounded);
            }

            worklog.setComment(tmsWorklog.getComment());
            worklog.setDay(tmsWorklog.getDay());
            worklog.setEmployeeId(userPmcId);
            worklog.setSourceId(tmsWorklog.getSourceId());

            Long timeId = worklogSyncIdToTimeIdMap.get(tmsWorklog.getSourceId());
            if (timeId == null) {
                timeJdbcTemplate.query("select * from worklog where source_system_id = ? and source_id = ?",
                        (rs) -> {
                            Worklog entity = readWorklog(rs);
                            worklogSyncIdToTimeIdMap.put(entity.getSourceId(), entity.getId());
                            target.put(entity.getId(), entity);
                        }, accountUid, tmsWorklog.getSourceId());

                timeId = worklogSyncIdToTimeIdMap.get(tmsWorklog.getSourceId());
            }

            if (timeId != null) {//copy from target
                worklog.setId(timeId);
                Worklog targetWorklog = target.get(worklog.getId());
            } else {//set default
                worklog.setId(fakeIdCounter);
                fakeIdCounter--;
            }

            Set<Long> timePackageCandidates = SearchLogicOfTimePackage.findMatchingTimePackages(accountId, tmsWorklog.getReportingItemId(),
                    worklog.getDay(), syncTasksCache, timeJdbcTemplate);

            List<Long> assignmentCandidates = searchLogicOfAssignment.findMatchingAssignments(timePackageCandidates, userPmcId, tmsWorklog.getDay());

            worklog.setEscapePmcProjectId(SearchLogicOfEscapeProject.find(accountId, timePackageCandidates,
                    assignmentCandidates, timeJdbcTemplate));

            if (assignmentCandidates.size() == 1) {
                worklog.setAssignmentUid(assignmentCandidates.get(0));
            }

            //step4: validate and log validation message if needed
            Set<Long> finalTimePackageCandidates = timePackageCandidates;
            Optional<String> exceptionalMessage = checkExternalWorklog(tmsWorklog)
                    .or(() -> checkAssignmentCandidate(tmsWorklog, finalTimePackageCandidates, assignmentCandidates));

            if (exceptionalMessage.isPresent()) {
                //validation has not passed. leave log and ignore this worklog while sync.
                worklogExternalTmsMessageDao.saveExternalMessage(worklog, accountId, false, exceptionalMessage.get(), null);
                target.remove(worklog.getId());
                continue;
            }

            //validation passed
            if (!resynchronize) {//for sync mode if worklog was not changed in jira do not try to re-store assignment, only check is actual
                String hashInSource = calcHash(tmsWorklog.getHours(), worklog.getComment()/*todo change to tmsWorklog.comment?*/,
                        tmsWorklog.getDay(), userPmcId/*todo change to tmsWorklog.syncUserId?*/, tmsWorklog.getReportingItemId());
                ShortExternalMessage shortExternalMessage = sourceIdToShortMessage.get(tmsWorklog.getSourceId());
                Long timeIdForRemove = worklogSyncIdToTimeIdMap.get(tmsWorklog.getSourceId());

                if (shortExternalMessage != null && hashInSource.equals(shortExternalMessage.getLastHash())) {
                    if (timeIdForRemove != null) {
                        Worklog existWorklog = target.get(timeIdForRemove);
                        if (existWorklog != null && shortExternalMessage.getAssignmentChanged() == existWorklog
                                .getAssignmentUid().equals(worklog.getAssignmentUid())) {
                            if (!shortExternalMessage.getAssignmentChanged()) {
                                sourceIdWithNoneActualAssignment.add(tmsWorklog.getSourceId());
                            } else {
                                sourceIdWithActualAssignment.add(tmsWorklog.getSourceId());
                            }
                        }
                        target.remove(timeIdForRemove);
                    }
                    continue;
                }
            }

            source.put(getExternalId(worklog), worklog);
        }

        //fetch worklogs that should be deleted
        List<Worklog> worklogsShouldBeDeleted = fetchWorklogsBySourceIds(shouldBeDeletedSourceIds, accountUid);
        worklogsShouldBeDeleted.forEach(worklog -> {
            target.put(worklog.getId(), worklog);
        });

        //clear manually reported
        if (resynchronize && from != null && to != null) {
            Set<Long> assignmentIdsToCleanManuallyReported = new HashSet<>();

            for (Worklog i : source.values()) {
                assignmentIdsToCleanManuallyReported.add(i.getAssignmentUid());
            }

            for (Worklog i : target.values()) {//everywhere activities from this source where reported
                assignmentIdsToCleanManuallyReported.add(i.getAssignmentUid());
            }

            for (Long i : assignmentIdsToCleanManuallyReported) {
                timeJdbcTemplate.query("select * from worklog where assignment_uid = ? " +
                        "and timesheet_date between ? and ? and source_id is null ", (rs) -> {
                    Worklog entity = readWorklog(rs);
                    entity.setSourceId("illegal-" + entity.getSourceId());
                    target.put(entity.getId(), entity);
                }, i, from, to);
            }
        }

        if (resynchronize) {
            List<String> keysForFetchStatus = target.values().stream().map(Worklog::getSourceId).collect(Collectors.toList());
            if (!keysForFetchStatus.isEmpty()) {
                unsuccessfullySyncedSourceIds = worklogExternalTmsMessageDao.fetchUnsuccessfullySyncedSourceIds(keysForFetchStatus, accountId);
            }
        }

        if (!resynchronize) {
            worklogExternalTmsMessageDao.actualizeAssignmentChanged(true, sourceIdWithNoneActualAssignment, accountId);
            worklogExternalTmsMessageDao.actualizeAssignmentChanged(false, sourceIdWithActualAssignment, accountId);
        }

        Set<Long> allKeys = new HashSet<>();
        allKeys.addAll(source.keySet());
        allKeys.addAll(target.keySet());

        // guarantees to perform deletions first
        ArrayList<Long> allKeysToSort = new ArrayList<>(allKeys);
        allKeysToSort.sort(Comparator.comparing(k -> source.get(k) == null ? null : k, Comparator.nullsFirst(Comparator.naturalOrder())));

        for (Long i : allKeysToSort) {
            collect(i, source.get(i));
        }

        flush();

        logger.info("end to process user {}", userPmcId);
    }

    private static Map<String, TmsWorklog> prepareWorklogsMap(List<TmsWorklog> externalWorklogs) {
        Map<String, TmsWorklog> result = new HashMap<>();
        for (TmsWorklog i : externalWorklogs) {
            TmsWorklog clone = new TmsWorklog();

            clone.setAccountId(i.getAccountId());
            clone.setSourceId(i.getSourceId());
            clone.setComment(i.getComment());
            clone.setDay(i.getDay());
            clone.setHours(i.getHours());
            clone.setUserSourceId(i.getUserSourceId());
            clone.setReportingItemId(i.getReportingItemId());

            if (clone.getHours() != null && (clone.getHours() <= 0 || clone.getHours().intValue() > 99)) {
                clone.setHours(null);
            }

            result.put(i.getSourceId(), clone);
        }

        return result;
    }

    private static Worklog readWorklog(ResultSet rs) throws SQLException {
        Worklog timeWorklog = new Worklog();
        timeWorklog.setId(rs.getLong("id"));
        timeWorklog.setSourceId(rs.getString("source_id"));
        timeWorklog.setEmployeeId(rs.getLong("employee_uid"));
        timeWorklog.setComment(rs.getString("activity_name"));
        timeWorklog.setDay(rs.getObject("timesheet_date", LocalDate.class));
        timeWorklog.setHours(rs.getDouble("duration"));
        timeWorklog.setAssignmentUid(rs.getLong("assignment_uid"));
        timeWorklog.setEscapePmcProjectId(rs.getLong("project_uid_projection"));

        return timeWorklog;
    }

    private Optional<String> checkExternalWorklog(TmsWorklog externalWorklog) {
        String exceptionMessage = null;
        if (externalWorklog.getHours() == null) {//hours are fetched from cache table and guaranteed to be not negative or null
            exceptionMessage = "Hours in a correct format should be specified";
        }

//        if(externalWorklog.getDay() == null) {//todo enable when wrong day format will be supported
//            exceptionMessage = "Day should be specified";
//        }
        return Optional.ofNullable(exceptionMessage);
    }

    private Optional<String> checkAssignmentCandidate(TmsWorklog externalWorklog, Set<Long> timePackageCandidates,
                                                      List<Long> assignmentCandidates) {
        String exceptionMessage = null;
        if (assignmentCandidates.size() != 1) {
            if (timePackageCandidates.isEmpty()) {
                boolean knownReportingItem = timeJdbcTemplate
                        .queryForObject("select exists (select 1 from tms_reporting_item where account_id = ? and source_id = ?)",
                                Boolean.class, accountId, externalWorklog.getReportingItemId());
                if (knownReportingItem) {
                    exceptionMessage = "The mapping between issue and Time package wasn't found";
                } else {
                    exceptionMessage = "The worklog cannot be processed due to inability to get the appropriate issue. Please try again later.";
                }

            } else {
                if (assignmentCandidates.isEmpty()) {
                    exceptionMessage = "The employee is not included in the mentioned package for a date";
                } else {
                    exceptionMessage = "There are 2+ more positions worklog can be assigned to";
                }
            }
        }

        return Optional.ofNullable(exceptionMessage);
    }

    private List<Worklog> fetchWorklogsBySourceIds(List<String> sourceIds, String sourceSystemId) {
        return JdbcUtil.fetchPartitioned(sourceIds,
                (keys) -> new NamedParameterJdbcTemplate(timeJdbcTemplate).query("select * from worklog " +
                                " where source_system_id = :sourceSystemId " +
                                "and source_id in (:sourceIds)",
                        Map.of(
                                "sourceSystemId", sourceSystemId,
                                "sourceIds", keys
                        ),
                        (rs, i) -> readWorklog(rs)
                ),
                JdbcUtil.FETCH_SIZE_10_000);
    }

    private static String calcHash(Worklog worklog, String reportingItemId) {
        return calcHash(worklog.getHours(), worklog.getComment(), worklog.getDay(), worklog.getEmployeeId(), reportingItemId);
    }

    private static String calcHash(Object... objects) {
        StringBuilder sb = new StringBuilder();
        for (Object i : objects) {
            if (i != null) {
                sb.append(Hashing.murmur3_128().hashUnencodedChars(i.toString()).toString());
            }
            sb.append(" ");
        }

        return Hashing.murmur3_128().hashUnencodedChars(sb.toString()).toString();
    }

    private static List<ReportingItemMappingDto> getTasksForSync(JdbcTemplate timeJdbcTemplate, long accountId) {
        return timeJdbcTemplate.query(
                "select time_package_id, reporting_item_id, m.start_date, m.end_date, " +
                        " p.start_date as package_start_date, p.end_date as package_end_date " +
                        "   from tms_reporting_item_mapping m " +
                        "   join time_package p on p.uid = m.time_package_id " +
                        "   where account_id = ?",
                (resultSet, i) -> convertSyncTask(resultSet), accountId);
    }

    private static ReportingItemMappingDto convertSyncTask(ResultSet rs) throws java.sql.SQLException {
        ReportingItemMappingDto entity = new ReportingItemMappingDto();
        LocalDate mappingStartDate = rs.getObject("start_date", LocalDate.class);
        LocalDate packageStartDate = rs.getObject("package_start_date", LocalDate.class);
        entity.setStartDate(DateUtil.maxOfNullable(mappingStartDate, packageStartDate));
        LocalDate packageEndDate = rs.getObject("package_end_date", LocalDate.class);
        LocalDate mappingEndDate = rs.getObject("end_date", LocalDate.class);
        entity.setEndDate(DateUtil.minOfNullable(mappingEndDate, packageEndDate));
        entity.setReportingItemId(rs.getString("reporting_item_id"));
        entity.setTimePackageId(rs.getObject("time_package_id", Long.class));
        return entity;
    }

    public static class ReportingItemMappingDto {
        private LocalDate startDate;
        private LocalDate endDate;
        private String reportingItemId;
        private Long timePackageId;

        public LocalDate getStartDate() {
            return startDate;
        }

        public void setStartDate(LocalDate startDate) {
            this.startDate = startDate;
        }

        public LocalDate getEndDate() {
            return endDate;
        }

        public void setEndDate(LocalDate endDate) {
            this.endDate = endDate;
        }

        public String getReportingItemId() {
            return reportingItemId;
        }

        public void setReportingItemId(String reportingItemId) {
            this.reportingItemId = reportingItemId;
        }

        public Long getTimePackageId() {
            return timePackageId;
        }

        public void setTimePackageId(Long timePackageId) {
            this.timePackageId = timePackageId;
        }
    }

    private static String getExceptionMessage(String prefix, Exception e) {
        String errorMessage = prefix + e.getMessage();
        if (RestClientResponseException.class.isAssignableFrom(e.getClass())) {
            String body = ((RestClientResponseException) e).getResponseBodyAsString();
            try {
                HashMap map = new ObjectMapper().readValue(body, HashMap.class);
                errorMessage = "Time reporting was failed. Reason: " + map.get("message").toString();
            } catch (Exception e2) {
                logger.warn(" can not parse time response " + body, e2);
            }
        }

        return errorMessage;
    }

    @Override
    protected Long getExternalId(Worklog entity) {
        return entity.getId();
    }

    @Override
    protected List<Worklog> fetchFromTarget(Set<Long> externalIds) {
        List<Worklog> result = new ArrayList<>();
        for (Long i : externalIds) {
            Worklog worklog = target.get(i);
            if (worklog != null) {
                result.add(worklog);
            }
        }

        return result;
    }

    @Override
    protected void insertIntoTarget(List<Worklog> entities) {
        for (Worklog i : entities) {
            try {
                time.addWorklog(i.getAssignmentUid(), i.getEmployeeId(), i.getComment(), i.getHours(), i.getDay(),
                        accountUid, i.getSourceId());
                worklogExternalTmsMessageDao.saveExternalMessage(i, accountId, true, null,
                        calcHash(i, worklogSyncIdToReportingItemIdMap.get(i.getSourceId())));
            } catch (Exception e) {
                String errorMessage = getExceptionMessage("Time reporting was failed. Reason: ", e);
                worklogExternalTmsMessageDao.saveExternalMessage(i, accountId, false, errorMessage, null);
                logger.warn(errorMessage, e);
            }
        }
    }

    @Override
    protected int[] updateTarget(List<Worklog> entities) {
        for (Worklog i : entities) {
            try {
                time.updateWorklogForcibly(i.getEmployeeId(), i.getAssignmentUid(), i.getComment(), i.getHours(),
                        i.getDay(), i.getId(), accountUid, i.getSourceId());
                worklogExternalTmsMessageDao.saveExternalMessage(i, accountId, true, null,
                        calcHash(i, worklogSyncIdToReportingItemIdMap.get(i.getSourceId())));
            } catch (Exception e) {
                String errorMessage = getExceptionMessage("Time reporting was failed. Reason: ", e);
                worklogExternalTmsMessageDao.saveExternalMessage(i, accountId, false, errorMessage, null);
                logger.warn(errorMessage, e);
            }
        }

        return new int[0];
    }

    @Override
    protected void deleteFromTarget(Long id) {
        Worklog worklogToLog = target.get(id);
        try {
            time.deleteWorklog(id);
            worklogExternalTmsMessageDao.saveExternalMessage(worklogToLog, accountId, true, null, null);
        } catch (Exception e) {
            String errorMessage = getExceptionMessage("Time removal was failed. Reason: ", e);
            worklogExternalTmsMessageDao.saveExternalMessage(worklogToLog, accountId, false, errorMessage, null);
            logger.warn(errorMessage, e);
        }
    }

    @Override
    protected void same(Worklog sourceObject) {
        if (!resynchronize) {
            //however all fields are the same. That means last hash is wrong. that may happen if user
            //changed in jira and time worklog in the same way (and assignment still matches).
            //then we consider that as success and change hash to correct
            //otherwise worklog will be tried to get synced (and then ignored) every sync run which affects performance
            worklogExternalTmsMessageDao.updateLastHash(calcHash(sourceObject, worklogSyncIdToReportingItemIdMap.get(sourceObject.getSourceId())),
                    accountId, sourceObject.getSourceId());//todo what if worklog_external_tms_message absent? need to leave log
            //externalWorklogDao.saveExternalMessage(sourceObject, accountId, true, null);//todo what message. null is ok?
        } else {
            //we can't use logic from sync mode, because we don't check sync status in resync
            //mode and we will have request to database for every successfully synced worklog
            if (unsuccessfullySyncedSourceIds.contains(sourceObject.getSourceId())) {
                worklogExternalTmsMessageDao.saveExternalMessage(sourceObject, accountId, true, null,
                        calcHash(sourceObject, worklogSyncIdToReportingItemIdMap.get(sourceObject.getSourceId())));
            }
        }
    }

    public static class Worklog {
        private Long id;
        private String sourceId;
        private String comment;
        private LocalDate day;
        private Double hours;
        private Long employeeId;
        private Long assignmentUid;
        private Long escapePmcProjectId;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getSourceId() {
            return sourceId;
        }

        public void setSourceId(String sourceId) {
            this.sourceId = sourceId;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public LocalDate getDay() {
            return day;
        }

        public void setDay(LocalDate day) {
            this.day = day;
        }

        public Double getHours() {
            return hours;
        }

        public void setHours(Double hours) {
            this.hours = hours;
        }

        public Long getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(Long employeeId) {
            this.employeeId = employeeId;
        }

        public Long getAssignmentUid() {
            return assignmentUid;
        }

        public void setAssignmentUid(Long assignmentUid) {
            this.assignmentUid = assignmentUid;
        }

        public Long getEscapePmcProjectId() {
            return escapePmcProjectId;
        }

        public void setEscapePmcProjectId(Long escapePmcProjectId) {
            this.escapePmcProjectId = escapePmcProjectId;
        }

        @Override
        public boolean equals(Object o) {
            return EqualsBuilder.reflectionEquals(this, o, false);
        }

        @Override
        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode(this, false);
        }

    }

    public static class ShortExternalMessage {
        private String sourceId;
        private String lastHash;
        private Boolean assignmentChanged;

        public String getSourceId() {
            return sourceId;
        }

        public void setSourceId(String sourceId) {
            this.sourceId = sourceId;
        }

        public String getLastHash() {
            return lastHash;
        }

        public void setLastHash(String lastHash) {
            this.lastHash = lastHash;
        }

        public Boolean getAssignmentChanged() {
            return assignmentChanged;
        }

        public void setAssignmentChanged(Boolean assignmentChanged) {
            this.assignmentChanged = assignmentChanged;
        }
    }
}
