package com.bsuir.time.core.dao;

import com.bsuir.time.core.dto.TmsReportingItemDto;
import com.bsuir.time.core.util.JdbcUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class TmsReportingItemDao {

    @Autowired
    @Qualifier("timeJdbcTemplate")
    private JdbcTemplate timeJdbcTemplate;

    public boolean existsReportingItem(long accountId, String sourceId){
        return timeJdbcTemplate.queryForObject("select exists (select 1 from tms_reporting_item where " +
                        "account_id = ? and source_id = ?)",
                Boolean.class, accountId, sourceId);
    }

    public List<String> fetchSourceIdsByAccountId(long accountId) {
        return timeJdbcTemplate.queryForList("select source_id from tms_reporting_item where account_id = ?",
                String.class, accountId);
    }

    public List<TmsReportingItemDto> fetchByAccountIdAndParentSourceId(Long accountId, String parentSourceId) {
        return timeJdbcTemplate.query("select * from tms_reporting_item where parent_ids_projection @> ? and account_id = ?",
                pStmt -> {
                    pStmt.setArray(1, JdbcUtil.getArray(pStmt, List.of(parentSourceId)));
                    pStmt.setObject(2, accountId);
                },
                (resultSet, i) -> convertToTmsReportingItemDto(resultSet));
    }

    public List<TmsReportingItemDto> fetchByAccountId(long accountId) {
        return timeJdbcTemplate.query("select * from tms_reporting_item where account_id = ?",
                (resultSet, i) -> convertToTmsReportingItemDto(resultSet), accountId);
    }

    private TmsReportingItemDto convertToTmsReportingItemDto(ResultSet resultSet) throws SQLException {
        TmsReportingItemDto entity = new TmsReportingItemDto();
        entity.setSourceId(resultSet.getString("source_id"));
        entity.setAccountId(resultSet.getLong("account_id"));
        entity.setName(resultSet.getString("name"));
        entity.setParentSourceId(resultSet.getString("parent_source_id"));
        entity.setParentIdsProjection(JdbcUtil.getNullableListOfStringsFor("parent_ids_projection", resultSet));
        return entity;
    }
}
