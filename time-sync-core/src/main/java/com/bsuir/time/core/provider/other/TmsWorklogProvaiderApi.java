package com.bsuir.time.core.provider.other;

import com.bsuir.time.core.dto.TmsWorklog;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TmsWorklogProvaiderApi implements TmsWorklogProvider {

    private Set<String> userSyncIds;
    private Long accountId;
    private LocalDate from;
    private LocalDate to;
    private List<TmsWorklog> worklogs;

    public TmsWorklogProvaiderApi(Long accountId, LocalDate from, LocalDate to, List<TmsWorklog> worklogs) {
        List<TmsWorklog> filteredWorklogs = worklogs.stream()
                .filter(worklog -> worklog.getAccountId().equals(accountId))
                .filter(worklog -> !(worklog.getDay().isBefore(from) || worklog.getDay().isAfter(to)))
                .collect(Collectors.toList());
        this.userSyncIds = filteredWorklogs.stream()
                .map(TmsWorklog::getUserSourceId)
                .collect(Collectors.toSet());
        this.accountId = accountId;
        this.from = from;
        this.to = to;
        this.worklogs = filteredWorklogs;
    }

    @Override
    public Long getAccountId() {
        return accountId;
    }

    @Override
    public Set<String> getUserSourceIds() {
        return userSyncIds;
    }

    @Override
    public List<TmsWorklog> getWorklogs(String userSyncId) {
        return worklogs.stream()
                .filter(worklog -> worklog.getUserSourceId().equals(userSyncId))
                .collect(Collectors.toList());
    }

    @Override
    public LocalDate getFrom() {
        return from;
    }

    @Override
    public LocalDate getTo() {
        return to;
    }
}
