package com.bsuir.time.core.service.other;

import com.bsuir.time.core.dto.TmsWorklog;
import com.bsuir.time.core.util.SyncServiceAbstract3;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TmsWorklogSyncService extends SyncServiceAbstract3<TmsWorklog, String> {

    private static final String SQL_UPSERT =
            "insert into tms_worklog (source_id, comment, timesheet_date, hours, " +
                    "reporting_item_id, " +
                    "sync_user_id, account_id, changed, deleted) " +
                    "values (:sourceId, :comment, :day, :hours, :reportingItemId, " +
                    ":userSourceId, :accountId, :changed, :deleted) " +
                    "on conflict (source_id, account_id) do " +
                    "update set source_id = :sourceId, comment = :comment, timesheet_date = :day, hours = :hours, " +
                    "reporting_item_id = :reportingItemId, " +
                    "sync_user_id = :userSourceId, account_id = :accountId, changed = :changed, deleted = :deleted";

    private final JdbcTemplate timeJdbcTemplate;
    private final long accountId;
    private final String syncUserId;
    private Map<String, TmsWorklog> targetCache = new HashMap<>();

    public TmsWorklogSyncService(JdbcTemplate timeJdbcTemplate, String syncUserId, long accountId) {
        this.timeJdbcTemplate = timeJdbcTemplate;
        this.accountId = accountId;
        this.syncUserId = syncUserId;
    }

    public void sync(Map<String, TmsWorklog> externalWorklogMap, LocalDate from, LocalDate to) {
        try {
            init();

            Map<String, TmsWorklog> sourceCache = new HashMap<>();
            for (Map.Entry<String, TmsWorklog> externalWorklogEntry : externalWorklogMap.entrySet()) {
                sourceCache.put(externalWorklogEntry.getKey(), externalWorklogEntry.getValue());
            }

            if (from != null && to != null) {
                for (TmsWorklog targetWorklog : fetch(accountId, syncUserId, from, to)) {
                    targetCache.put(getExternalId(targetWorklog), targetWorklog);
                }
            }

            for (String id : sourceCache.keySet()) {
                if (!targetCache.containsKey(id)) {
                    TmsWorklog externalWorklog = fetchById(id);
                    if (externalWorklog != null) {
                        targetCache.put(id, externalWorklog);
                    }
                }
            }

            Set<String> keys = new HashSet<>();
            keys.addAll(sourceCache.keySet());
            keys.addAll(targetCache.keySet());

            for (String key : keys) {
                collect(key, sourceCache.get(key));
            }
            flush();
        } finally {
            targetCache.clear();
        }
    }

    @Override
    protected String getExternalId(TmsWorklog entity) {
        return entity.getSourceId();
    }

    @Override
    protected List<TmsWorklog> fetchFromTarget(Set<String> externalIds) {
        List<TmsWorklog> result = new ArrayList<>();
        for (String externalId : externalIds) {
            TmsWorklog entity = targetCache.get(externalId);
            if (entity != null) {
                result.add(entity);
            }
        }
        return result;
    }

    @Override
    protected void insertIntoTarget(List<TmsWorklog> entities) {
        saveOrUpdate(entities, accountId);
    }

    @Override
    protected int[] updateTarget(List<TmsWorklog> entities) {
        saveOrUpdate(entities, accountId);
        return new int[0];
    }

    @Override
    protected void deleteFromTarget(String id) {
        timeJdbcTemplate.update(
                "update tms_worklog set deleted = true, changed = ? where account_id = ? and source_id = ?",
                Instant.now().atOffset(ZoneOffset.UTC), accountId, id);
    }

    @Override
    protected void log() {
        // no logs
    }


    private void saveOrUpdate(List<TmsWorklog> worklogs, long accountId) {
        SqlParameterSource[] batchArgs = new SqlParameterSource[worklogs.size()];
        for (int i = 0; i < worklogs.size(); i++) {
            TmsWorklog worklog = worklogs.get(i);
            Map<String, Object> params = new HashMap<>();
            params.put("accountId", accountId);
            params.put("sourceId", worklog.getSourceId());
            params.put("comment", worklog.getComment());
            params.put("day", worklog.getDay());
            params.put("hours", worklog.getHours());
            params.put("reportingItemId", worklog.getReportingItemId());
            params.put("userSourceId", worklog.getUserSourceId());
            params.put("changed", Instant.now().atOffset(ZoneOffset.UTC));
            params.put("deleted", false);

            batchArgs[i] = new MapSqlParameterSource(params);
        }
        new NamedParameterJdbcTemplate(timeJdbcTemplate).batchUpdate(SQL_UPSERT, batchArgs);
    }

    private List<TmsWorklog> fetch(long accountId, String syncUserId, LocalDate from, LocalDate to) {
        return timeJdbcTemplate.query("select * from tms_worklog " +
                        "where account_id = ? " +
                        "and sync_user_id = ? " +
                        "and timesheet_date between ? and ? " +
                        "and deleted = false",
                (rs, rowNum) -> convert(rs),
                accountId, syncUserId, from, to);
    }

    private TmsWorklog fetchById(String externalWorklogId) {
        try {
            return timeJdbcTemplate.queryForObject("select * " +
                    "from tms_worklog " +
                    "where account_id = ? and source_id = ? and deleted = false", (rs, rowNum) -> convert(rs), accountId, externalWorklogId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private void convert(TmsWorklog entity, ResultSet rs) throws SQLException {
        entity.setSourceId(rs.getString("source_id"));
        entity.setComment(rs.getString("comment"));
        entity.setDay(rs.getObject("timesheet_date", LocalDate.class));
        entity.setHours(rs.getDouble("hours"));
        entity.setReportingItemId(rs.getString("reporting_item_id"));
        entity.setAccountId(rs.getLong("account_id"));
        entity.setUserSourceId(rs.getString("sync_user_id"));
    }

    private TmsWorklog convert(ResultSet rs) throws SQLException {
        TmsWorklog result = new TmsWorklog();
        convert(result, rs);
        return result;
    }


}
