package com.bsuir.time.core.dao;

import com.bsuir.time.core.dto.TmsReportingItemMapping;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface TmsReportingItemMappingDao extends Repository<TmsReportingItemMapping, Void> {
    @Modifying
    @Query("delete from tms_reporting_item_mapping where account_id = :accountId and reporting_item_id = :sourceId")
    void deleteMapping(@Param("accountId") Long accountId, @Param("sourceId") String sourceId);

    @Modifying
    @Query("insert into tms_reporting_item_mapping values (:accountId, :reportingItemId, :timePackageId, :startDate, :endDate)")
    void insertMapping(@Param("accountId") Long accountId, @Param("reportingItemId") String reportingItemId,
                       @Param("timePackageId") Long timePackageId,@Param("startDate") LocalDate startDate,
                       @Param("endDate") LocalDate endDate);

    @Query("select * from tms_reporting_item_mapping where account_id = :accountId")
    List<TmsReportingItemMapping> findByAccountId(@Param("accountId") Long accountId);
}
