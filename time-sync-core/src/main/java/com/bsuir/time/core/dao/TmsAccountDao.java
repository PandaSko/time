package com.bsuir.time.core.dao;

import com.bsuir.time.core.dto.TmsAccount;

public interface TmsAccountDao extends CustomCrudRepository<TmsAccount, Long> {
}
