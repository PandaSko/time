package com.bsuir.time.core.dto;

import java.time.LocalDate;

public class TmsReportingItemMapping {
    private Long accountId;
    private String reportingItemId;
    private Long timePackageId;
    private LocalDate startDate;
    private LocalDate endDate;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getReportingItemId() {
        return reportingItemId;
    }

    public void setReportingItemId(String reportingItemId) {
        this.reportingItemId = reportingItemId;
    }

    public Long getTimePackageId() {
        return timePackageId;
    }

    public void setTimePackageId(Long timePackageId) {
        this.timePackageId = timePackageId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
