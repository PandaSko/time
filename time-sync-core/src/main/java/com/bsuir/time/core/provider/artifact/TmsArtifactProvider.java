package com.bsuir.time.core.provider.artifact;

import java.util.Map;

public interface TmsArtifactProvider<TKey, T> {

    long getAccountId();

    Map<TKey, T> getSourceMap();
}
