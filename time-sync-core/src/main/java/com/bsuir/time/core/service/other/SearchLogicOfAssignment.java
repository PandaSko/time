package com.bsuir.time.core.service.other;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

public class SearchLogicOfAssignment {

    private final JdbcTemplate timeJdbcTemplate;

    public SearchLogicOfAssignment(JdbcTemplate timeJdbcTemplate) {
        this.timeJdbcTemplate = timeJdbcTemplate;
    }

    public List<Long> findMatchingAssignments(Set<Long> timePackageCandidates, Long pmcUserId, LocalDate day) {
        if (timePackageCandidates.isEmpty()) {
            return Collections.emptyList();
        }
        List<Long> candidates = new ArrayList<>();

        new NamedParameterJdbcTemplate(timeJdbcTemplate).query("select " +
                        "a.uid, " +
                        "a.start_date a_start_date, " +
                        "a.end_date a_end_date, " +
                        "p.start_date pack_start_date, " +
                        "p.end_date pack_end_date, " +
                        "pos.start_date pos_start_date, " +
                        "pos.end_date pos_end_date " +
                        "from assignment a " +
                        "join time_package p on p.uid = a.time_package_uid " +
                        "join position pos on pos.uid = a.position_uid " +
                        "where p.uid in (:packageIds) " +
                        "and a.employee_uid = :pmcUserId",
                Map.of(
                        "packageIds", timePackageCandidates,
                        "pmcUserId", pmcUserId
                ),
                rs -> {
                    Long assignmentUid = rs.getLong("uid");
                    LocalDate packStartDate = rs.getObject("pack_start_date", LocalDate.class);
                    LocalDate packEndDate = rs.getObject("pack_end_date", LocalDate.class);
                    LocalDate posStartDate = rs.getObject("pos_start_date", LocalDate.class);
                    LocalDate posEndDate = rs.getObject("pos_end_date", LocalDate.class);
                    LocalDate aStartDate = rs.getObject("a_start_date", LocalDate.class);
                    LocalDate aEndDate = rs.getObject("a_end_date", LocalDate.class);

                    LocalDate start = maxOfNotNull(aStartDate, packStartDate, posStartDate, day);
                    LocalDate end = minOfNotNull(aEndDate, packEndDate, posEndDate, day);
                    if (day.isBefore(start) || day.isAfter(end)) {
                        return;
                    }
                    candidates.add(assignmentUid);
                });

        return candidates;
    }

    private static LocalDate minOfNotNull(LocalDate... values) {
        return Stream.of(values).filter(Objects::nonNull).min(LocalDate::compareTo)
                .orElseThrow(() -> new IllegalArgumentException("At least some date should be not null"));
    }

    private static LocalDate maxOfNotNull(LocalDate... values) {
        return Stream.of(values).filter(Objects::nonNull).max(LocalDate::compareTo)
                .orElseThrow(() -> new IllegalArgumentException("At least some date should be not null"));
    }

    private static List<LocalDatePeriod> getNotNullableSetOfLocalDatesFor(String fieldName, ResultSet rs) throws SQLException {
        List<LocalDatePeriod> result = new ArrayList<>();
        LocalDatePeriod defaultLocalDatePeriod = new LocalDatePeriod();
        defaultLocalDatePeriod.setStartDate(null);
        defaultLocalDatePeriod.setEndDate(null);

        Array array = rs.getArray(fieldName);
        if (rs.wasNull()) {
            result.add(defaultLocalDatePeriod);
            return result;
        }

        java.sql.Date[] arr = new java.sql.Date[]{};
        if (array != null) {
            arr = (java.sql.Date[]) array.getArray();
        }

        if (arr.length % 2 != 0) {
            throw new RuntimeException("Periods should have even number of dates");
        }

        if (arr.length == 0) {
            result.add(defaultLocalDatePeriod);
            return result;
        }

        for (int i = 0; i < arr.length; i += 2) {
            LocalDatePeriod localDatePeriod = new LocalDatePeriod();
            localDatePeriod.setStartDate(arr[i] != null ? arr[i].toLocalDate() : null);
            localDatePeriod.setEndDate(arr[i + 1] != null ? arr[i + 1].toLocalDate() : null);
            if (localDatePeriod.getStartDate() != null && localDatePeriod.getEndDate() != null && localDatePeriod.getStartDate().isAfter(localDatePeriod.getEndDate())) {
                throw new RuntimeException("Start date is after end date");
            }
            result.add(localDatePeriod);
        }

        return result;
    }

    private static class LocalDatePeriod {
        private LocalDate startDate;
        private LocalDate endDate;

        public LocalDate getStartDate() {
            return startDate;
        }

        public void setStartDate(LocalDate startDate) {
            this.startDate = startDate;
        }

        public LocalDate getEndDate() {
            return endDate;
        }

        public void setEndDate(LocalDate endDate) {
            this.endDate = endDate;
        }
    }
}
