package com.bsuir.time.core.util;

import com.google.common.collect.Lists;
import org.springframework.util.CollectionUtils;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class JdbcUtil {

    public static final int FETCH_SIZE_500 = 500;
    public static final int FETCH_SIZE_1000 = 1000;
    public static final int FETCH_SIZE_10_000 = 10_000;

    public static Array getArray(PreparedStatement pStmt, Collection<String> elements) throws SQLException {
        return pStmt.getConnection().createArrayOf("text", elements.toArray());
    }

    public static List<String> getNullableListOfStringsFor(String fieldName, ResultSet rs) throws SQLException {
        Array array = rs.getArray(fieldName);
        if (rs.wasNull()) {
            return null;
        }
        String[] arr = new String[]{};
        if (array != null) {
            arr = (String[]) array.getArray();
        }
        return Arrays.stream(arr).collect(Collectors.toList());
    }

    public static List<String> getListOfStringsFor(String fieldName, ResultSet rs) throws SQLException {
        Array array = rs.getArray(fieldName);
        if (rs.wasNull()) {
            return new ArrayList<>();
        }
        String[] arr = new String[]{};
        if (array != null) {
            arr = (String[]) array.getArray();
        }
        return Arrays.stream(arr).collect(Collectors.toList());
    }

    public static <T, E> List<E> fetchPartitioned(Collection<T> params, Function<Collection<T>, Collection<E>> action, int fetchSize) {
        if (CollectionUtils.isEmpty(params)) {
            return Collections.emptyList();
        }
        return Lists.partition(new ArrayList<>(params), fetchSize).stream().flatMap(partition -> action.apply(partition).stream()).collect(Collectors.toList());
    }

    public static <T> void executePartitioned(Collection<T> params, Consumer<Collection<T>> action, int fetchSize) {
        if (CollectionUtils.isEmpty(params)) {
            return;
        }
        Lists.partition(new ArrayList<>(params), fetchSize).forEach(action);
    }
}
