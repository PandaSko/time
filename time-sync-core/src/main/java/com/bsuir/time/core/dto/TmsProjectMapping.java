package com.bsuir.time.core.dto;

public class TmsProjectMapping {
    private Long accountId;
    private String reportingItemId;
    private Long projectUid;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getReportingItemId() {
        return reportingItemId;
    }

    public void setReportingItemId(String reportingItemId) {
        this.reportingItemId = reportingItemId;
    }

    public Long getProjectUid() {
        return projectUid;
    }

    public void setProjectUid(Long projectUid) {
        this.projectUid = projectUid;
    }
}
