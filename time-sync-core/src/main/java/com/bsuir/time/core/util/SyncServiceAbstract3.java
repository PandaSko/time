package com.bsuir.time.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.sql.BatchUpdateException;
import java.sql.SQLException;
import java.util.*;

abstract public class SyncServiceAbstract3<T, TExKey> {

    public static final String CHANGER_SYNCHRONIZATION_SERVICE = "synchronization service";
    protected static final int BATCH_SIZE = 1000;//should not exceed PG batch in clause size, insert/update/delete possible batch size

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private long collectedCounter;
    private long createdCounter;
    private long modifiedCounter;
    private long deletedCounter;

    private Map<TExKey, T> sourceMap = new HashMap<>(BATCH_SIZE);

    private static volatile boolean shutdownRequested;

    @PreDestroy
    public void predestroy() {
        shutdownRequested = true;
    }

    protected void init() {
        this.collectedCounter = 0;
        this.createdCounter = 0;
        this.modifiedCounter = 0;
        this.deletedCounter = 0;
    }

    protected void collect(TExKey id, T sourceObject) {//null to delete in target
        if (shutdownRequested) {
            throw new ShutdownRequest();
        }
        collectedCounter++;
        sourceMap.put(id, sourceObject);
        if (sourceMap.size() >= BATCH_SIZE) {
            flush();
        }
    }

    protected void flush() {
        try {
            Set<TExKey> toDelete = new HashSet<>();
            List<T> toInsert = new ArrayList<>();
            List<T> toUpdate = new ArrayList<>();

            if (!sourceMap.isEmpty()) {
                Map<TExKey, T> targetMap = new HashMap<>();//empty map works for Algorithm.ALPHA
                for (T i : fetchFromTarget(sourceMap.keySet())) {
                    targetMap.put(getExternalId(i), i);
                }

                for (TExKey externalId : sourceMap.keySet()) {
                    T sourceObject = sourceMap.get(externalId);
                    T targetObject = targetMap.get(externalId);
                    if (skip(sourceObject, targetObject)) {
                        continue;
                    } else if (sourceObject == null && targetObject == null) {
                        //do nothing: object is deleted on both sides
                    } else if (sourceObject == null && targetObject != null) {
                        toDelete.add(externalId);
                    } else if (sourceObject != null && targetObject == null) {
                        toInsert.add(sourceObject);
                    } else if (!targetObject.equals(sourceObject)) {
                        toUpdate.add(sourceObject);
                    } else {
                        same(sourceObject);
                    }
                }
            }

            for (TExKey i : toDelete) {
                deleteFromTarget(i);
            }

            if (!toUpdate.isEmpty()) {
                updateTarget(toUpdate);
            }

            if (!toInsert.isEmpty()) {
                insertIntoTarget(toInsert);
            }

            createdCounter += toInsert.size();
            modifiedCounter += toUpdate.size();
            deletedCounter += toDelete.size();

            log();
        } catch (RuntimeException e) {
            if (e.getCause() != null && e.getCause() instanceof BatchUpdateException) {
                SQLException sqlException = ((BatchUpdateException) e.getCause()).getNextException();
                //set very first message as primary one
                if (sqlException != null && sqlException.getMessage() != null) {
                    sqlException.getMessage();
                    throw new RuntimeException(sqlException.getMessage(), e);
                }
            }
            throw e;
        } finally {
            sourceMap.clear();
        }
    }

    protected boolean skip(T sourceObject, T targetObject) {
        return false;
    }

    protected abstract TExKey getExternalId(T entity);

    protected abstract List<T> fetchFromTarget(Set<TExKey> externalIds);

    protected abstract void insertIntoTarget(List<T> entities);

    protected abstract int[] updateTarget(List<T> entities);

    protected abstract void deleteFromTarget(TExKey id);

    protected void same(T entitity) {
        //do nothing by default
    }

    protected void log() {
        logger.info("collected:{}; created:{}; modified:{}; deleted:{};", collectedCounter, createdCounter, modifiedCounter, deletedCounter);
    }

    public static class ShutdownRequest extends RuntimeException {
    }
}
