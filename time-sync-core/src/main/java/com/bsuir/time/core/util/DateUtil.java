package com.bsuir.time.core.util;

import java.time.LocalDate;
import java.util.Objects;
import java.util.stream.Stream;

public class DateUtil {
    public static LocalDate minOfNullable(LocalDate... values) {
        return Stream.of(values).filter(Objects::nonNull).min(LocalDate::compareTo)
                .orElse(null);
    }

    public static LocalDate maxOfNullable(LocalDate... values) {
        return Stream.of(values).filter(Objects::nonNull).max(LocalDate::compareTo)
                .orElse(null);
    }
}
