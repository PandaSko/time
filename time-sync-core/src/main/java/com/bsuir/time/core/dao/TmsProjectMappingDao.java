package com.bsuir.time.core.dao;

import com.bsuir.time.core.dto.TmsProjectMapping;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TmsProjectMappingDao extends Repository<TmsProjectMapping, Void> {
    @Modifying
    @Query("delete from tms_project_mapping where account_id = :accountId and reporting_item_id = :sourceId")
    void deleteMapping(@Param("accountId") Long accountId, @Param("sourceId") String sourceId);

    @Modifying
    @Query("insert into tms_project_mapping values (:accountId, :reportingItemId, :projectId)")
    void insertMapping(@Param("accountId") Long accountId, @Param("reportingItemId") String reportingItemId,
                       @Param("projectId") Long projectId);

    @Query("select * from tms_project_mapping where account_id = :accountId")
    List<TmsProjectMapping> findByAccountId(@Param("accountId") Long accountId);

    @Query("select * from tms_project_mapping where project_uid = :projectId")
    List<TmsProjectMapping> findByProjectUid(@Param("projectId") Long projectUid);
}
