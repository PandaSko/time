package com.bsuir.time.core.service.other;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class SearchLogicOfEscapeProject {

    public static Long find(long accountId, Collection<Long> timePackageIds, Collection<Long> assignmentCandidates, JdbcTemplate timeJdbcTemplate) {

        List<Long> projectUids;
        //try assignments
        if (!assignmentCandidates.isEmpty()) {
            projectUids = new NamedParameterJdbcTemplate(timeJdbcTemplate).queryForList("select distinct project_uid " +
                    "from assignment where uid in (:assignmentUids) order by project_uid", Map.of("assignmentUids", assignmentCandidates), Long.class);
            if (!projectUids.isEmpty()) {
                return new TreeSet<>(projectUids).first();
            }
        }

        //try time packages
        if (!CollectionUtils.isEmpty(timePackageIds)) {
            projectUids = new NamedParameterJdbcTemplate(timeJdbcTemplate).queryForList("select distinct uid from time_package tp " +
                            "join project p on tp.project_uid = p.uid where tp.id in (:ids) order by uid",
                    Map.of("ids", timePackageIds),
                    Long.class);

            if (!projectUids.isEmpty()) {
                return new TreeSet<>(projectUids).first();
            }
        }

        projectUids = getAssociatedUpsaProjectIds(accountId, timeJdbcTemplate);
        if (!projectUids.isEmpty()) {
            return new TreeSet<>(projectUids).first();
        }

        return null;
    }

    private static List<Long> getAssociatedUpsaProjectIds(long accountId, JdbcTemplate timeJdbcTemplate) {

        return timeJdbcTemplate.queryForList(
                "select project_uid from tms_project_mapping where account_id = ?", Long.class, accountId);

    }

}
