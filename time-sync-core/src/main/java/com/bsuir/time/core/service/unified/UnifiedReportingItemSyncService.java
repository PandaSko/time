package com.bsuir.time.core.service.unified;

import com.bsuir.time.core.dto.TmsArtifactExternalId;
import com.bsuir.time.core.dto.TmsReportingItemDto;
import com.bsuir.time.core.provider.artifact.TmsArtifactProvider;
import com.bsuir.time.core.util.JdbcUtil;
import com.bsuir.time.core.util.SyncServiceAbstract3;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.CollectionUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class UnifiedReportingItemSyncService extends SyncServiceAbstract3<TmsReportingItemDto, TmsArtifactExternalId> {
    private JdbcTemplate timeJdbcTemplate;

    public UnifiedReportingItemSyncService(JdbcTemplate timeJdbcTemplate) {
        this.timeJdbcTemplate = timeJdbcTemplate;
    }

    public void syncWith(TmsArtifactProvider<String, TmsReportingItemDto> provider) {
        init();

        Map<String, TmsReportingItemDto> collectMap = provider.getSourceMap();
        long accountId = provider.getAccountId();
        fillParents(collectMap);

        for (String key : collectMap.keySet()) {
            collect(convertToExternalId(key, accountId), collectMap.get(key));
        }

        flush();
    }

    @Override
    protected TmsArtifactExternalId getExternalId(TmsReportingItemDto entity) {
        TmsArtifactExternalId externalId = new TmsArtifactExternalId();
        externalId.setAccountId(entity.getAccountId());
        externalId.setSourceId(entity.getSourceId());
        return externalId;
    }

    @Override
    protected List<TmsReportingItemDto> fetchFromTarget(Set<TmsArtifactExternalId> externalIds) {
        if (CollectionUtils.isEmpty(externalIds)) {
            return new ArrayList<>();
        }

        Long accountId = externalIds.stream().findFirst().get().getAccountId();
        List<String> sourceIds = externalIds.stream().map(TmsArtifactExternalId::getSourceId).distinct().collect(Collectors.toList());
        return new NamedParameterJdbcTemplate(timeJdbcTemplate).query(
                "select * from tms_reporting_item where account_id = :accountId and source_id in (:sourceIds)",
                Map.of(
                        "accountId", accountId,
                        "sourceIds", sourceIds
                ),
                (resultSet, i) -> convertToTmsReportingItemDto(resultSet));
    }

    @Override
    protected void insertIntoTarget(List<TmsReportingItemDto> entities) {
        timeJdbcTemplate.batchUpdate("insert into tms_reporting_item " +
                        "(account_id, source_id, name, parent_source_id, parent_ids_projection) values (?,?,?,?,?)",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pStmt, int i) throws SQLException {
                        TmsReportingItemDto entity = entities.get(i);
                        pStmt.setLong(1, entity.getAccountId());
                        pStmt.setString(2, entity.getSourceId());
                        pStmt.setString(3, entity.getName());
                        pStmt.setString(4, entity.getParentSourceId());
                        pStmt.setArray(5, JdbcUtil.getArray(pStmt, entity.getParentIdsProjection()));
                    }

                    @Override
                    public int getBatchSize() {
                        return entities.size();
                    }
                });
    }

    @Override
    protected int[] updateTarget(List<TmsReportingItemDto> entities) {
        return timeJdbcTemplate.batchUpdate("update tms_reporting_item set name = ?, " +
                        "parent_source_id = ?, parent_ids_projection = ? " +
                        "where account_id = ? and source_id = ?",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pStmt, int i) throws SQLException {
                        TmsReportingItemDto entity = entities.get(i);
                        pStmt.setString(1, entity.getName());
                        pStmt.setString(2, entity.getParentSourceId());
                        pStmt.setArray(3, JdbcUtil.getArray(pStmt, entity.getParentIdsProjection()));
                        pStmt.setLong(4, entity.getAccountId());
                        pStmt.setString(5, entity.getSourceId());
                    }

                    @Override
                    public int getBatchSize() {
                        return entities.size();
                    }
                });
    }

    @Override
    protected void deleteFromTarget(TmsArtifactExternalId externalId) {
        timeJdbcTemplate.update("delete from tms_reporting_item where account_id = ? and source_id = ?",
                externalId.getAccountId(), externalId.getSourceId());
    }

    private TmsReportingItemDto convertToTmsReportingItemDto(ResultSet resultSet) throws SQLException {
        TmsReportingItemDto entity = new TmsReportingItemDto();
        entity.setSourceId(resultSet.getString("source_id"));
        entity.setAccountId(resultSet.getLong("account_id"));
        entity.setName(resultSet.getString("name"));
        entity.setParentSourceId(resultSet.getString("parent_source_id"));
        entity.setParentIdsProjection(JdbcUtil.getNullableListOfStringsFor("parent_ids_projection", resultSet));
        return entity;
    }

    private TmsArtifactExternalId convertToExternalId(String key, long accountId) {
        TmsArtifactExternalId externalId = new TmsArtifactExternalId();
        externalId.setAccountId(accountId);
        externalId.setSourceId(key);
        return externalId;
    }

    private void fillParents(Map<String, TmsReportingItemDto> collectMap) {
        for (TmsReportingItemDto entity : collectMap.values()) {
            if (entity == null) {
                continue;
            }

            //parents should be sorted
            List<String> parents = new ArrayList<>();

            TmsReportingItemDto child = entity;
            while (child != null && child.getParentSourceId() != null) {
                if (collectMap.containsKey(child.getParentSourceId())) {
                    parents.add(child.getParentSourceId());
                }
                child = collectMap.get(child.getParentSourceId());
            }
            entity.setParentIdsProjection(parents);
        }
    }
}
