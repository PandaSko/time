package com.bsuir.time.core.provider.other;

import com.bsuir.time.core.dto.TmsWorklog;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface TmsWorklogProvider {
    Long getAccountId();

    Set<String> getUserSourceIds();

    List<TmsWorklog> getWorklogs(String userSyncId);

    default LocalDate getFrom(){
        return null;
    }

    default LocalDate getTo() {
        return null;
    }
}
