package com.bsuir.time.core.provider.artifact;

import com.bsuir.time.core.dto.TmsUserDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TmsUserProviderApi implements TmsArtifactProvider<String, TmsUserDto> {
    private long accountId;
    private Map<String, TmsUserDto> sourceMap;

    public TmsUserProviderApi(long accountId, List<TmsUserDto> sources, List<String> targetKeys) {
        this.accountId = accountId;
        this.sourceMap = new HashMap<>();

        targetKeys.forEach(key -> sourceMap.put(key, null));
        sources.forEach(user -> sourceMap.put(user.getSourceId(), user));
    }

    @Override
    public long getAccountId() {
        return accountId;
    }

    @Override
    public Map<String, TmsUserDto> getSourceMap() {
        return sourceMap;
    }
}
