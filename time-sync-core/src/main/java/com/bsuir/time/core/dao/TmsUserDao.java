package com.bsuir.time.core.dao;

import com.bsuir.time.core.dto.TmsUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TmsUserDao {

    @Autowired
    @Qualifier("timeJdbcTemplate")
    private JdbcTemplate timeJdbcTemplate;

    public boolean existsUser(long accountId, String sourceId){
        return timeJdbcTemplate.queryForObject("select exists (select 1 from tms_user where " +
                        "account_id = ? and source_id = ?)",
                Boolean.class, accountId, sourceId);
    }

    public List<String> fetchSourceIdsByAccountId(long accountId) {
        return timeJdbcTemplate.queryForList("select source_id from tms_user where account_id = ?", String.class, accountId);
    }

    public List<TmsUserDto> fetchByAccountId(long accountId) {
        return timeJdbcTemplate.query(
                "select * from tms_user where account_id = ?",
                (resultSet, i) -> {
                    TmsUserDto user = new TmsUserDto();
                    user.setName(resultSet.getString("name"));
                    user.setSourceId(resultSet.getString("source_id"));
                    user.setAccountId(resultSet.getObject("account_id", Long.class));
                    return user;
                }, accountId);
    }
}
