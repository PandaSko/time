package com.bsuir.time.core.dao;

import com.bsuir.time.core.dto.TmsUserMapping;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TmsUserMappingDao extends Repository<TmsUserMapping, Void> {

    @Modifying
    @Query("insert into tms_user_mapping values (:accountId, :sourceId, :employeeUid)")
    void createMapping(@Param("accountId") Long accountId, @Param("sourceId") String sourceId,
                              @Param("employeeUid") Long employeeUid);

    @Modifying
    @Query("delete from tms_user_mapping where account_id = :accountId and source_id = :sourceId")
    void deleteMapping(@Param("accountId") Long accountId, @Param("sourceId") String sourceId);

    @Query("select * from tms_user_mapping where account_id = :accountId")
    List<TmsUserMapping> findByAccountId(@Param("accountId") Long accountId);

    @Query("select employee_uid from tms_user_mapping where account_id = :accountId and source_id = :userSourceId")
    Long getMappedUserUid(@Param("accountId") Long accountId, @Param("userSourceId") String userSourceId);
}
