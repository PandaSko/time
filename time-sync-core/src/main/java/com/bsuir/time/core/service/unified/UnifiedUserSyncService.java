package com.bsuir.time.core.service.unified;

import com.bsuir.time.core.dto.TmsArtifactExternalId;
import com.bsuir.time.core.dto.TmsUserDto;
import com.bsuir.time.core.provider.artifact.TmsArtifactProvider;
import com.bsuir.time.core.util.SyncServiceAbstract3;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.CollectionUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class UnifiedUserSyncService extends SyncServiceAbstract3<TmsUserDto, TmsArtifactExternalId> {

    private JdbcTemplate timeJdbcTemplate;

    public UnifiedUserSyncService(JdbcTemplate timeJdbcTemplate) {
        this.timeJdbcTemplate = timeJdbcTemplate;
    }

    public void syncWith(TmsArtifactProvider<String, TmsUserDto> provider) {
        init();

        Map<String, TmsUserDto> collectMap = provider.getSourceMap();
        long accountId = provider.getAccountId();

        for (String key : collectMap.keySet()) {
            collect(convertToExternalId(key, accountId), collectMap.get(key));
        }

        flush();
    }

    @Override
    protected TmsArtifactExternalId getExternalId(TmsUserDto entity) {
        TmsArtifactExternalId externalId = new TmsArtifactExternalId();
        externalId.setAccountId(entity.getAccountId());
        externalId.setSourceId(entity.getSourceId());
        return externalId;
    }

    @Override
    protected List<TmsUserDto> fetchFromTarget(Set<TmsArtifactExternalId> externalIds) {
        if (CollectionUtils.isEmpty(externalIds)) {
            return new ArrayList<>();
        }

        long accountId = externalIds.stream().findFirst().get().getAccountId();
        List<String> sourceIds = externalIds.stream().map(TmsArtifactExternalId::getSourceId).distinct().collect(Collectors.toList());
        return new NamedParameterJdbcTemplate(timeJdbcTemplate).query(
                "select * from tms_user where account_id = :accountId and source_id in (:sourceIds)",
                Map.of(
                        "accountId", accountId,
                        "sourceIds", sourceIds
                ),
                (resultSet, i) -> convertToTmsUserDto(resultSet));
    }

    @Override
    protected void insertIntoTarget(List<TmsUserDto> entities) {
        timeJdbcTemplate.batchUpdate("insert into tms_user (account_id, source_id, name) values (?,?,?)",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pStmt, int i) throws SQLException {
                        TmsUserDto entity = entities.get(i);
                        pStmt.setLong(1, entity.getAccountId());
                        pStmt.setString(2, entity.getSourceId());
                        pStmt.setString(3, entity.getName());
                    }

                    @Override
                    public int getBatchSize() {
                        return entities.size();
                    }
                });
    }

    @Override
    protected int[] updateTarget(List<TmsUserDto> entities) {
        return timeJdbcTemplate.batchUpdate("update tms_user set name = ? where account_id = ? and source_id = ?",
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement pStmt, int i) throws SQLException {
                        TmsUserDto entity = entities.get(i);
                        pStmt.setString(1, entity.getName());
                        pStmt.setLong(2, entity.getAccountId());
                        pStmt.setString(3, entity.getSourceId());
                    }

                    @Override
                    public int getBatchSize() {
                        return entities.size();
                    }
                });
    }

    @Override
    protected void deleteFromTarget(TmsArtifactExternalId key) {
        timeJdbcTemplate.update("delete from tms_user where account_id = ? and source_id = ?", key.getAccountId(), key.getSourceId());
    }

    private TmsArtifactExternalId convertToExternalId(String key, long accountId) {
        TmsArtifactExternalId externalId = new TmsArtifactExternalId();
        externalId.setAccountId(accountId);
        externalId.setSourceId(key);
        return externalId;
    }

    private TmsUserDto convertToTmsUserDto(ResultSet resultSet) throws SQLException {
        TmsUserDto userDto = new TmsUserDto();
        userDto.setAccountId(resultSet.getObject("account_id", Long.class));
        userDto.setName(resultSet.getString("name"));
        userDto.setSourceId(resultSet.getString("source_id"));
        return userDto;
    }
}
