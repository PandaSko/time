package com.bsuir.time.core.provider.artifact;

import com.bsuir.time.core.dto.TmsReportingItemDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TmsReportingItemProviderApi implements TmsArtifactProvider<String, TmsReportingItemDto> {

    private long accountId;
    private Map<String, TmsReportingItemDto> sourceMap;

    public TmsReportingItemProviderApi(long accountId, List<TmsReportingItemDto> sources, List<String> targetIds) {
        this.accountId = accountId;
        sourceMap = new HashMap<>();

        targetIds.forEach(sourceId -> sourceMap.put(sourceId, null));
        sources.forEach(source -> sourceMap.put(source.getSourceId(), source));
    }

    @Override
    public long getAccountId() {
        return accountId;
    }

    @Override
    public Map<String, TmsReportingItemDto> getSourceMap() {
        return sourceMap;
    }
}
