package com.bsuir.time.core.service.other;

import java.time.LocalDate;

public interface TimeFacade {

     Long addWorklog(Long taskId, Long pmcUserId, String activityName, double duration, LocalDate date, String accountUid,
                    String sourceId);

    Long updateWorklogForcibly(Long pmcUserId, Long pmcTaskId, String activityName, double duration, LocalDate date,
                               long oldActivityId, String accountUid, String sourceId);

    void deleteWorklog(long id);
}
