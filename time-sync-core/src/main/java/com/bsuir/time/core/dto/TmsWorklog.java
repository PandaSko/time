package com.bsuir.time.core.dto;

import java.time.LocalDate;

public class TmsWorklog {
    private Long accountId;
    private String sourceId;
    private String comment;
    private LocalDate day;
    private Double hours;
    private String userSourceId;
    private String reportingItemId;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public Double getHours() {
        return hours;
    }

    public void setHours(Double hours) {
        this.hours = hours;
    }

    public String getUserSourceId() {
        return userSourceId;
    }

    public void setUserSourceId(String userSourceId) {
        this.userSourceId = userSourceId;
    }

    public String getReportingItemId() {
        return reportingItemId;
    }

    public void setReportingItemId(String reportingItemId) {
        this.reportingItemId = reportingItemId;
    }
}
