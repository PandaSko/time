package com.bsuir.time.core.dao;

import com.bsuir.time.core.service.unified.UnifiedWorklogSyncService;
import com.bsuir.time.core.util.JdbcUtil;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.bsuir.time.core.util.JdbcUtil.FETCH_SIZE_10_000;

public class WorklogExternalTmsMessageDao {

    private JdbcTemplate timeJdbcTemplate;
    private NamedParameterJdbcTemplate namedTimeJdbcTemplate;

    public WorklogExternalTmsMessageDao(JdbcTemplate timeJdbcTemplate) {
        this.timeJdbcTemplate = timeJdbcTemplate;
        namedTimeJdbcTemplate = new NamedParameterJdbcTemplate(timeJdbcTemplate);
    }

    public void actualizeAssignmentChanged(boolean actualValue, List<String> sourceIds, Long accountId) {
        if (CollectionUtils.isEmpty(sourceIds)) {
            return;
        }

        JdbcUtil.executePartitioned(sourceIds,
                (keys) -> namedTimeJdbcTemplate.update(
                        "update tms_worklog_message set assignment_changed = :actual where account_id = :accountId and source_id in (:sourceIds)",
                        Map.of(
                                "actual", actualValue,
                                "sourceIds", keys,
                                "accountId", accountId
                        )
                ),
                FETCH_SIZE_10_000);
    }

    public List<UnifiedWorklogSyncService.ShortExternalMessage> fetchShortExternalMessage(Set<String> sourceIds, Long accountId) {
        return JdbcUtil.fetchPartitioned(sourceIds, keys -> namedTimeJdbcTemplate.query(
                "select source_id, last_success_hash, assignment_changed from tms_worklog_message " +
                        "where account_id = :accountId and source_id in (:keys) and deleted = false",
                Map.of(
                        "accountId", accountId,
                        "keys", keys
                ),
                (resultSet, i) -> {
                    UnifiedWorklogSyncService.ShortExternalMessage result = new UnifiedWorklogSyncService.ShortExternalMessage();
                    result.setAssignmentChanged(resultSet.getBoolean("assignment_changed"));
                    result.setLastHash(resultSet.getString("last_success_hash"));
                    result.setSourceId(resultSet.getString("source_id"));
                    return result;
                }), FETCH_SIZE_10_000);
    }

    public List<String> fetchUnsuccessfullySyncedSourceIds(List<String> keysForFetchStatus, Long accountId) {
        return JdbcUtil.fetchPartitioned(keysForFetchStatus, keys -> namedTimeJdbcTemplate.queryForList(
                "select source_id from tms_worklog_message " +
                        "where account_id = :accountId and source_id in (:keys) and success = false and deleted = false",
                Map.of(
                        "accountId", accountId,
                        "keys", keys),
                String.class), FETCH_SIZE_10_000);
    }

    public void saveExternalMessage(UnifiedWorklogSyncService.Worklog worklog, long accountId, boolean success, String message, String hash) {
        Map<String, Object> params = new HashMap<>();
        String syncId = worklog.getSourceId();
        if (worklog.getSourceId().contains("illegal")) {
            accountId = 0;//zero means time system
            syncId = worklog.getSourceId().split("-")[1];
        }

        params.put("accountId", accountId);
        params.put("sourceId", syncId);
        params.put("success", success);
        params.put("message", message);
        params.put("changed", Instant.now().atOffset(ZoneOffset.UTC));
        params.put("projectUidProjection", worklog.getEscapePmcProjectId());
        params.put("dayProjection", worklog.getDay());
        params.put("deleted", false);//todo remove this field?
        params.put("lastSuccessHash", success ? hash : null);
        params.put("assignmentChanged", success && hash != null ? false : null);//deleted have null hash

        String query =
                "insert into tms_worklog_message (account_id, source_id, success, message, changed, " +
                "   project_uid_projection, day_projection, last_success_hash, deleted, assignment_changed) " +
                "   values (:accountId, :sourceId, :success, :message, :changed, :projectUidProjection, :dayProjection, :lastSuccessHash, :deleted, :assignmentChanged) " +
                "on conflict (account_id, source_id) do " +
                "update set success = :success, message = :message, changed = :changed, project_uid_projection = :projectUidProjection," +
                "   day_projection = :dayProjection, deleted = :deleted";

        if (success) {
            query += ", assignment_changed = :assignmentChanged, last_success_hash = :lastSuccessHash";
        }

        namedTimeJdbcTemplate.update(query, params);
    }

    public void updateLastHash(String hash, Long accountId, String syncId) {
        timeJdbcTemplate.update("update tms_worklog_message set last_success_hash = ? where account_id = ? and source_id = ?",
                hash, accountId, syncId);
    }
}
