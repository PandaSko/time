package com.bsuir.time.core.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

public class TmsReportingItemDto {

    private Long accountId;
    private String sourceId;
    private String name;
    private String parentSourceId;
    private List<String> parentIdsProjection = new ArrayList<>();

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentSourceId() {
        return parentSourceId;
    }

    public void setParentSourceId(String parentSourceId) {
        this.parentSourceId = parentSourceId;
    }

    public List<String> getParentIdsProjection() {
        return parentIdsProjection;
    }

    public void setParentIdsProjection(List<String> parentIdsProjection) {
        this.parentIdsProjection = parentIdsProjection;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o, false);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, false);
    }
}
