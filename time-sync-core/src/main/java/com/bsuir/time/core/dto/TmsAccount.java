package com.bsuir.time.core.dto;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;

public class TmsAccount {
    @Id
    private Long id;
    private String uid;
    private String type;
    private String name;
    private Boolean preferResynchronization;
    @LastModifiedDate
    private LocalDateTime changed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPreferResynchronization() {
        return preferResynchronization;
    }

    public void setPreferResynchronization(Boolean preferResynchronization) {
        this.preferResynchronization = preferResynchronization;
    }
}
