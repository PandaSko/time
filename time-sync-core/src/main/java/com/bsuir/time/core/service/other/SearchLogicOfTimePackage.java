package com.bsuir.time.core.service.other;


import com.bsuir.time.core.service.unified.UnifiedWorklogSyncService.ReportingItemMappingDto;
import com.bsuir.time.core.util.JdbcUtil;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SearchLogicOfTimePackage {

    private static final LocalDate START_DATE = LocalDate.of(1900, 1, 1);
    private static final LocalDate END_DATE = LocalDate.of(2900, 1, 1);

    public static Set<Long> findMatchingTimePackages(long accountId, String reportingItemId, LocalDate day, List<ReportingItemMappingDto> syncTasks,
                                                     JdbcTemplate timeJdbcTemplate) {
        Set<Long> result = new HashSet<>();

        Boolean isExcelAccount = timeJdbcTemplate.queryForObject(
                "select exists (select 1 from tms_account where id = ? and type = 'Excel')", Boolean.class, accountId);

        if (isExcelAccount != null && isExcelAccount) {
            try {
                Long specifiedTimePackageId = Long.valueOf(reportingItemId);
                Boolean packageExists = timeJdbcTemplate.queryForObject(
                        "select exists (select 1 from time_package where id = ? and deleted = false)", Boolean.class, specifiedTimePackageId);
                if (packageExists != null && packageExists) {
                    result.add(specifiedTimePackageId);
                }
            } catch (NumberFormatException e) {
                //ignore
            }

            return result;
        }

        if (reportingItemId != null) {
            Boolean knownReportingItem = timeJdbcTemplate.queryForObject(
                    "select exists (select 1 from tms_reporting_item where account_id = ? and source_id = ?)", Boolean.class, accountId, reportingItemId);

            if (knownReportingItem != null && !knownReportingItem) {
                return result;
            }

            List<ReportingItemMappingDto> syncTasksNoIgnored = new ArrayList<>();
            syncTasksNoIgnored.addAll(syncTasks);

            List<ReportingItemMappingDto> tasksFilteredByDates = filterTasksByDates(day, syncTasksNoIgnored);

            List<ReportingItemMappingDto> tasks = new ArrayList<>();

            List<String> reportingItems = new ArrayList<>();
            reportingItems.add(reportingItemId);
            timeJdbcTemplate.query(
                    "select parent_ids_projection from tms_reporting_item where account_id = ? and source_id = ?",
                    rs -> {
                        reportingItems.addAll(JdbcUtil.getListOfStringsFor("parent_ids_projection", rs));
                    },
                    accountId, reportingItemId);

            for (String reportingItem : reportingItems) {
                for (ReportingItemMappingDto i : tasksFilteredByDates) {
                    if (reportingItem.equals(i.getReportingItemId())) {
                        tasks.add(i);
                    }
                }

                if (!tasks.isEmpty()) {
                    break;
                }
            }
            result.addAll(tasks.stream().map(ReportingItemMappingDto::getTimePackageId).collect(Collectors.toSet()));
        }

        return result;
    }

    private static List<ReportingItemMappingDto> filterTasksByDates(LocalDate day, List<ReportingItemMappingDto> syncTasks) {
        List<ReportingItemMappingDto> result = new ArrayList<>();
        for (ReportingItemMappingDto i : syncTasks) {
            LocalDate startDate = i.getStartDate();
            if (startDate == null) {
                startDate = START_DATE;
            }

            LocalDate endDate = i.getEndDate();

            if (endDate == null) {
                endDate = END_DATE;
            }

            if (day.isBefore(startDate) || day.isAfter(endDate)) {
                continue;
            }
            result.add(i);
        }
        return result;
    }
}
